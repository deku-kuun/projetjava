package INTERFACEclasses;



import Controller.controller;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class addTypeWeaponInterface extends Application
{
	private controller ctrl;
	private String catWeaponName ;
	private Stage stage ;
	
	public addTypeWeaponInterface(controller ctrl)
	{
		this.ctrl = ctrl;
	}

	public addTypeWeaponInterface(controller ctrl, String catWeaponName)
	{
		this(ctrl) ;
		this.catWeaponName = catWeaponName ;
	}

	@Override
	public void start(Stage primaryStage) 
	{

		stage = primaryStage;
		stage.setTitle("New type weapon");

		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setVgap(10);
		grid.setHgap(10);

		Text title = new Text("Add a new type of weapon");
		title.setFont(Font.font("Tahoma", FontWeight.LIGHT, 25));
		grid.add(title,1,0);

		Label typeWeaponName = new Label("type weapon's name");
		grid.add(typeWeaponName,0,1);

		TextField typeWeaponNameText  = new TextField();
		typeWeaponNameText.setPromptText("type weapon's name");
		grid.add(typeWeaponNameText,1,1);

		Label typeWeaponDescription = new Label("type Weapon Description");
		grid.add(typeWeaponDescription,0,2);

		TextField typeWeaponDescriptionText= new TextField();
		typeWeaponDescriptionText.setPromptText("type Weapon Description");
		grid.add(typeWeaponDescriptionText,1,2);

		Label typeWeaponRange = new Label("type Weapon Range");
		grid.add(typeWeaponRange,0,3);

		TextField typeWeaponRangeText  = new TextField();
		typeWeaponRangeText.setPromptText("type Weapon Range");
		grid.add(typeWeaponRangeText,1,3);

		Label typeWeaponWeight = new Label("type Weapon Weight");
		grid.add(typeWeaponWeight,0,4);

		TextField typeWeaponWeightText  = new TextField();
		typeWeaponWeightText.setPromptText("type Weapon Weight");
		grid.add(typeWeaponWeightText,1,4);

		Label typeWeaponPic = new Label("type Weapon Pic");
		grid.add(typeWeaponPic,0,5);

		TextField typeWeaponPicText  = new TextField();
		typeWeaponPicText.setPromptText("type Weapon Pic");
		grid.add(typeWeaponPicText,1,5);

		Label selectCatWeaponLabel = new Label("select Cat Weapon");
		grid.add(selectCatWeaponLabel,0,6);
		
		Button selectCatWeaponButton = new Button("Select");
		grid.add(selectCatWeaponButton,1,6);
		selectCatWeaponButton.setOnAction(selectCatWeaponEvent->
		{
			ctrl.lancerSelectCatWeaponForTypeWeapon(stage) ;
		});

		Label catWeapon = new Label(catWeaponName) ;
		grid.add(catWeapon,2,6);
		
		Button addTypeWeaponInDB = new Button("Add this type of weapon");
		addTypeWeaponInDB.setOnAction(addTypeWeaponEvent->
		{
			controller.addTypeWeapon( typeWeaponNameText.getText(),  typeWeaponDescriptionText.getText(),  
					typeWeaponRangeText.getText(), typeWeaponWeightText.getText(), 
					typeWeaponPicText.getText(), catWeapon.getText());
		});
		grid.add(addTypeWeaponInDB,1,9);

		Button launchAccueilButton =new Button("Accueil");
		grid.add(launchAccueilButton,4,0);
		launchAccueilButton.setOnAction(launchAccueilEvent->
		{
			ctrl.lancerAccueil(primaryStage);
		}
				);

		Scene scene = new Scene(grid,1000,700);
		stage.setScene(scene);
		stage.show();
	}	
}
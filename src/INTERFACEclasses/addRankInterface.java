package INTERFACEclasses;

import Controller.controller;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class addRankInterface extends Application 
{
	private controller ctrl;
	/**
	 * Constructor of controller
	 * @param ctrl
	 */
	public addRankInterface(controller ctrl){
		this.ctrl = ctrl;
	}
	
	@Override
	public void start(Stage primaryStage) {

		Stage Window = primaryStage;
		Window.setTitle("New rank");

		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setVgap(10);
		grid.setHgap(10);

		Text title = new Text("Create a new rank");
		title.setFont(Font.font("Tahoma", FontWeight.LIGHT, 25));
		grid.add(title,1,0);

		Label RankCodeLabel = new Label("Rank code");
		grid.add(RankCodeLabel,0,1);

		TextField RankCodeText = new TextField();
		grid.add(RankCodeText,1,1);

		Label RankNameLabel = new Label("Rank name");
		grid.add(RankNameLabel,0,2);

		TextField addRankNameText = new TextField();
		grid.add(addRankNameText,1,2);

		Button createRankButton = new Button("Create");
		grid.add(createRankButton,1,4);
		createRankButton.setOnAction(e->
		{
			// ctrl.addRank(RankCodeText.getText().trim(), addRankNameText.getText().trim()) ;
		});

		Button launchAccueilButton =new Button("Accueil");
		grid.add(launchAccueilButton,4,0);
		launchAccueilButton.setOnAction(launchAccueilEvent->
		{
			ctrl.lancerAccueil(primaryStage);
		}
		);

		Scene scene = new Scene(grid,1000,700);
		Window.setScene(scene);
		Window.show();
	}

	/**
	 * main method, launch stages
	 * @param args
	 */
	public static void main(String[] args) {
		launch(args);
	}

}
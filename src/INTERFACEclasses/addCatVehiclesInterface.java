package INTERFACEclasses;

import Controller.controller;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class addCatVehiclesInterface extends Application 
{
	Stage Window;
	private controller ctrl;

	/**
	 * constructor of the controller
	 * @param ctrl
	 */
	public addCatVehiclesInterface(controller ctrl){
		this.ctrl = ctrl;
	}

	/**
	 * main method launch the frame
	 * @param args
	 */
	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage)
	{
		Window = primaryStage;
		Window.setTitle("New vehicle category");
		//Grid permite to dispose our text label textfield on a grid
		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setVgap(10);
		grid.setHgap(10);

		Text title = new Text("Add a new vehicle category");
		title.setFont(Font.font("Tahoma", FontWeight.LIGHT, 25));
		grid.add(title,1,0);

		Label catVehicleNameLabel = new Label("Category name");
		grid.add(catVehicleNameLabel,0,1);

		TextField catVehicleNameText = new TextField();
		grid.add(catVehicleNameText,1,1);

		Button addCatVehicleButton = new Button("Add this category");
		grid.add(addCatVehicleButton,1,2);
		addCatVehicleButton.setOnAction(addCatVehicleEvent -> 
		{
			controller.addCatVehicle(catVehicleNameText.getText().trim());
		});	

		Button launchAccueilButton =new Button("Accueil");
		grid.add(launchAccueilButton,4,0);
		launchAccueilButton.setOnAction(lauchAccueilEvent-> 
		{
			ctrl.lancerAccueil(primaryStage); 
		} );

		Scene scene = new Scene(grid,1000,700);
		Window.setScene(scene);
		Window.show();
	}
}
package INTERFACEclasses;


import java.sql.SQLException;

import Controller.controller;
import DBclasses.countryDB;
import MODELclasses.country;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.Callback;

public class selectCountryForDistrictInterface extends Application {
	//table view will contains all the country 
	private TableView<country> table ;
	private controller ctrl;
	private Stage stage ;
	/**
	 * constructor of the controller
	 * @param ctrl
	 */
	public selectCountryForDistrictInterface(controller ctrl){
		this.ctrl = ctrl;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void start(Stage primaryStage) throws ClassNotFoundException, SQLException 
	{
		stage = primaryStage ;
		GridPane grid = new GridPane() ;
		table = new TableView<country>();
		grid.add(table, 0, 1);

		table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

		TableColumn<country, Integer> countryId //
		= new TableColumn<country, Integer>("Id");

		TableColumn<country, Integer> countryCode//
		= new TableColumn<country, Integer>("Code");

		TableColumn<country, String> countryName//
		= new TableColumn<country, String>("Name");

		TableColumn<country, String> registrationDate //
		= new TableColumn<country, String>("Registration Date");

		addBoutonToTable();

		// ==== typeVehicleId ===
		countryId.setCellValueFactory(new PropertyValueFactory<>("countryId"));
		countryId.setMinWidth(80);

		// ==== typeVehicleName ===
		countryCode.setCellValueFactory(new PropertyValueFactory<>("countryCode"));
		countryCode.setMinWidth(100);

		// ==== typeVehicleDescription ===
		countryName.setCellValueFactory(new PropertyValueFactory<>("countryName"));
		countryName.setMinWidth(255);

		// ==== typeVehicleMaxSpeed ===
		registrationDate.setCellValueFactory(new PropertyValueFactory<>("registrationDate"));
		registrationDate.setMinWidth(30);



		ObservableList<country> list = FXCollections.observableArrayList(countryDB.getCountryList());
		table.setItems(list);

		table.getColumns().addAll(countryId, countryCode, countryName, registrationDate);

		stage.setTitle("add country for mission");
		Scene scene = new Scene(grid, 1000, 700);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * method to add Button to Table 
	 */
	private void addBoutonToTable() {
		//create column who take information of model class called coutry 
		TableColumn<country, Void> colBtn = new TableColumn<country, Void>("select country ");
		colBtn.setMinWidth(60);
		Callback<TableColumn<country, Void>, TableCell<country, Void>> cellFactory = new Callback<TableColumn<country, Void>, TableCell<country, Void>>() {
			@Override
			public TableCell<country, Void> call(final TableColumn<country, Void> param) {
				final TableCell<country, Void> cell = new TableCell<country, Void>() {

					private final Button btn = new Button("Select");
					{
						btn.setOnAction((ActionEvent event) -> {
							country data = getTableView().getItems().get(getIndex());
							ctrl.selectCountryForDistrict(stage, data.getCountryName());
						});
					}
					@Override
					public void updateItem(Void item, boolean empty) {
						super.updateItem(item, empty);
						if (empty) {
							setGraphic(null);
						} else {
							setGraphic(btn);
						}
					}
				}; return cell;
			}
		};
		colBtn.setCellFactory(cellFactory);
		table.getColumns().add(colBtn);
	}
	/**
	 * main method, launch stage
	 * @param args
	 */
	public static void main(String[] args) {
		launch(args);
	}
}
package INTERFACEclasses;


import java.sql.SQLException;
import Controller.controller;
import DBclasses.weaponDB;
import MODELclasses.weapon;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class showWeaponInterface extends Application
{
	private TableView<weapon> table ;
	private controller ctrl;
	private Stage stage;
	
	
	/**
	 * constructor of the controller
	 * @param ctrl
	 */
	public showWeaponInterface(controller ctrl){
		this.ctrl = ctrl;
	}

	
	
	@SuppressWarnings("unchecked")
	@Override
	public void start(Stage primaryStage) throws ClassNotFoundException, SQLException 
	{
		this.stage = primaryStage ;
		GridPane grid = new GridPane() ;
		table = new TableView <weapon>();
		grid.add(table, 0, 1);

		table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

		TableColumn<weapon, Integer> weaponNameCol//
		= new TableColumn<weapon, Integer>("serial number");

		TableColumn<weapon, String> typeWeaponCol //
		= new TableColumn<weapon, String>("weapon model");

		TableColumn<weapon, String> weaponRegistrationDateCol//
		= new TableColumn<weapon, String>("registration date");


		// ==== weaponSN ===
		weaponNameCol.setCellValueFactory(new PropertyValueFactory<>("weaponSN"));
		weaponNameCol.setMinWidth(100);

		// ==== registrationDate ===
		weaponRegistrationDateCol.setCellValueFactory(new PropertyValueFactory<>("registrationDate"));
		weaponRegistrationDateCol.setMinWidth(100);

		// ==== typeWeaponName ===
		typeWeaponCol.setCellValueFactory(new PropertyValueFactory<>("typeWeaponName"));
		typeWeaponCol.setMinWidth(100);

		ObservableList<weapon> list = FXCollections.observableArrayList(weaponDB.getWeaponList());
		table.setItems(list);
		table.getColumns().addAll(weaponNameCol, weaponRegistrationDateCol, typeWeaponCol) ;


		Button launchAccueilButton =new Button("Accueil");
		grid.add(launchAccueilButton,0,0);
		launchAccueilButton.setOnAction(launchAccueilEvent->
		{
			ctrl.lancerAccueil(primaryStage);
		}
				);

		stage.setTitle("select weapon");
		Scene scene = new Scene(grid, 1000, 700);
		stage.setScene(scene);
		stage.show();

	}
	
}
package INTERFACEclasses;

import Controller.controller;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class addDistrictInterface extends Application 
{
	private controller ctrl;	
	private String countryName = "" ;
	/**
	 * constructor of the controller
	 * @param ctrl
	 */
	public addDistrictInterface(controller ctrl){
		this.ctrl = ctrl;
	}
	
	public addDistrictInterface(controller ctrl, String countryName)
	{
		this.ctrl = ctrl;
		this.countryName = countryName ;
	}

	/**
	 * main method and launch the frame
	 * @param args
	 */
	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {

		Stage Window = primaryStage;
		Window.setTitle("new district");

		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setVgap(10);
		grid.setHgap(10);

		Text title = new Text("Add a new district");
		title.setFont(Font.font("Tahoma", FontWeight.LIGHT, 25));
		grid.add(title,1,0);

		Label districtCodeLabel = new Label("district code");
		grid.add(districtCodeLabel,0,1);

		TextField districtCodeText = new TextField();
		grid.add(districtCodeText,1,1);

		Label districtNameLabel = new Label("District name");
		grid.add(districtNameLabel,0,2);

		TextField cityNameText = new TextField();
		grid.add(cityNameText,1,2);

		Label countryCodeLabel = new Label("Country code");
		grid.add(countryCodeLabel,0,3);

		Button selectCountryButton = new Button("Select from existing countries");
		grid.add(selectCountryButton,1,3);
		selectCountryButton.setOnAction(selectCountryEvent->
		{
			ctrl.lancerSelectCountryForDistrict(primaryStage);
		}
		);
		
		Label country = new Label(countryName);
		grid.add(country,2,3);
		
		Button addDistrict = new Button("Add this district");
		grid.add(addDistrict,1,4);
		addDistrict.setOnAction(addDistrictEvent -> 
		{
			controller.addDistrict(districtCodeText.getText().trim(), cityNameText.getText().trim(), country.getText().trim());
		});	

		Button launchAccueilButton =new Button("Accueil");
		grid.add(launchAccueilButton,4,0);
		launchAccueilButton.setOnAction(launchAccueilEvent->
		{
			ctrl.lancerAccueil(primaryStage);
		}
		);

		Scene scene = new Scene(grid,1000,700);
		Window.setScene(scene);
		Window.show();
	}
}
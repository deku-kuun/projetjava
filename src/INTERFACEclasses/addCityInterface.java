package INTERFACEclasses;

import Controller.controller;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class addCityInterface extends Application 
{
	private controller ctrl;
	private String districtName = "";
	/**
	 * constructor of the controller
	 * @param ctrl
	 */
	public addCityInterface(controller ctrl){
		this.ctrl = ctrl;
	}
	
	public addCityInterface(controller ctrl, String districtName)
	{
		this(ctrl) ;
		this.districtName = districtName ;
	}
	/**
	 * main method and launch the frame
	 * @param args
	 */
	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) 
	{
		Stage Window = primaryStage;
		Window.setTitle("New city");

		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setVgap(10);
		grid.setHgap(10);

		Text title = new Text("Add a new city");
		title.setFont(Font.font("Tahoma", FontWeight.LIGHT, 25));
		grid.add(title,1,0);

		Label postCodeCityLabel = new Label("City post code");
		grid.add(postCodeCityLabel,0,1);

		TextField postCodeCityText = new TextField();
		grid.add(postCodeCityText,1,1);

		Label cityNameLabel = new Label("City name");
		grid.add(cityNameLabel,0,2);

		TextField cityNameText = new TextField();
		grid.add(cityNameText,1,2);

		Label districtCodeLabel = new Label("District name");
		grid.add(districtCodeLabel,0,3);

		Button selectDistrictCodeButton= new Button("Select from existing districts");
		grid.add(selectDistrictCodeButton,1,3);
		selectDistrictCodeButton.setOnAction(selectDistrictCodeEvent-> 
		{
			ctrl.lancerSelectDistrictForCity(primaryStage);
		}
		);
		
		Label district = new Label(districtName);
		grid.add(district,2,3);

		Button addCityButton = new Button("Add this city");
		grid.add(addCityButton,1,4);
		addCityButton.setOnAction(addCityEvent -> 
		{
			controller.addCity(postCodeCityText.getText().trim(),cityNameText.getText().trim(), district.getText().trim());
		});	

		Button launchAccueilButton = new Button("Accueil");
		grid.add(launchAccueilButton,4,0);
		launchAccueilButton.setOnAction(launchAccueilEvent -> 
		{
			ctrl.lancerAccueil(primaryStage);
		});	

		Scene scene = new Scene(grid,1000,700);
		Window.setScene(scene);
		Window.show();
	}
}
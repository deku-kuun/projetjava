package INTERFACEclasses;


import java.sql.SQLException;

import Controller.controller;
import DBclasses.typeVehicleDB;
import MODELclasses.typeVehicle;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class showTypeVehicleInterface extends Application {
	
	public static void main(String[] args) {
		launch(args);
	}
	
	private TableView<typeVehicle> table ;
	private controller ctrl;
	/**
	 * constructor of the controller
	 * @param ctrl
	 */
	public showTypeVehicleInterface(controller ctrl){
		this.ctrl = ctrl;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void start(Stage stage) throws ClassNotFoundException, SQLException {
		GridPane grid = new GridPane() ;
		table = new TableView<typeVehicle>();
		grid.add(table, 0, 1);

		table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

		TableColumn<typeVehicle, Integer> typeVehicleIdCol //
		= new TableColumn<typeVehicle, Integer>("Id");

		TableColumn<typeVehicle, Integer> typeVehicleNameCol//
		= new TableColumn<typeVehicle, Integer>("Name");

		TableColumn<typeVehicle, String> TypeVehicleDescriptionCol//
		= new TableColumn<typeVehicle, String>("Description");

		TableColumn<typeVehicle, String> TypeVehicleMaxSpeedCol //
		= new TableColumn<typeVehicle, String>("Maximum speed");

		TableColumn<typeVehicle, String> TypeVehiclePlaceCol //
		= new TableColumn<typeVehicle, String>("capacity");

		TableColumn<typeVehicle, String> typeVehiclePicCol //
		= new TableColumn<typeVehicle, String>("Picture");

		TableColumn<typeVehicle, String> registrationDateCol //
		= new TableColumn<typeVehicle, String>("registrationDate");




		// ==== typeVehicleId ===
		typeVehicleIdCol.setCellValueFactory(new PropertyValueFactory<>("typeVehicleId"));
		typeVehicleIdCol.setMinWidth(80);

		// ==== typeVehicleName ===
		typeVehicleNameCol.setCellValueFactory(new PropertyValueFactory<>("typeVehicleName"));
		typeVehicleNameCol.setMinWidth(100);

		// ==== typeVehicleDescription ===
		TypeVehicleDescriptionCol.setCellValueFactory(new PropertyValueFactory<>("typeVehicleDescription"));
		TypeVehicleDescriptionCol.setMinWidth(255);

		// ==== typeVehicleMaxSpeed ===
		TypeVehicleMaxSpeedCol.setCellValueFactory(new PropertyValueFactory<>("typeVehicleMaxSpeed"));
		TypeVehicleMaxSpeedCol.setMinWidth(30);

		// ==== typeVehiclePlaces ===
		TypeVehiclePlaceCol.setCellValueFactory(new PropertyValueFactory<>("typeVehiclePlaces"));
		TypeVehiclePlaceCol.setMinWidth(30);

		// ==== typeVehiclePic ===
		typeVehiclePicCol.setCellValueFactory(new PropertyValueFactory<>("typeVehiclePic"));
		typeVehiclePicCol.setMinWidth(50);

		// ==== registrationDate  ===
		registrationDateCol.setCellValueFactory(new PropertyValueFactory<>("registrationDate"));
		registrationDateCol.setMinWidth(100);

		ObservableList<typeVehicle> list = FXCollections.observableArrayList(typeVehicleDB.getTypeVehicleList());
		table.setItems(list);

		table.getColumns().addAll(typeVehicleIdCol, typeVehicleNameCol, TypeVehicleDescriptionCol, TypeVehicleMaxSpeedCol,TypeVehiclePlaceCol, typeVehiclePicCol ,registrationDateCol);


		Button accueilButton = new Button("Accueil") ;
		grid.add(accueilButton, 0, 0);



		accueilButton.setOnAction(new EventHandler<ActionEvent>() {

			public void handle (ActionEvent event) {
				try {
					ctrl.lancerAccueil(stage);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
				);

		stage.setTitle("show type vehicle");

		Scene scene = new Scene(grid, 1000, 700);
		stage.setScene(scene);
		stage.show();

	}

	
}

package INTERFACEclasses;


import java.sql.SQLException;
import java.util.ArrayList;

import Controller.controller;
import DBclasses.cityDB;
import MODELclasses.city;
import MODELclasses.member;
import MODELclasses.vehicle;
import MODELclasses.weapon;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.Callback;

public class selectCityForMissionInterface extends Application {
	private TableView<city> table ;
	private controller ctrl;
	private Stage stage;

	private String cityName = "" ;
	private ArrayList <member> members ;
	private ArrayList <vehicle> vehicles  ;
	private ArrayList <weapon> weapons  ;

	public selectCityForMissionInterface(controller ctrl)
	{
		this.ctrl = ctrl;
		members = new ArrayList <member> () ;
		vehicles = new ArrayList <vehicle> () ; 
		weapons = new ArrayList <weapon> () ;
	}

	public selectCityForMissionInterface(controller ctrl, String cityName,
			ArrayList <member> members, ArrayList <vehicle> vehicles,
			ArrayList <weapon> weapons)
	{
		this(ctrl) ;
		this.cityName = cityName;
		this.members = members ;
		this.vehicles = vehicles ;
		this.weapons = weapons ;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void start(Stage stage) throws ClassNotFoundException, SQLException {

		this.stage = stage;
		GridPane grid = new GridPane() ;
		table = new TableView<city>();
		//table.set

		grid.add(table, 0, 1);
		TableColumn<city, Integer> cityId//
		= new TableColumn<city, Integer>("Id");


		TableColumn<city, String> CityPostCode //
		= new TableColumn<city, String>("post code");


		TableColumn<city, String> cityName//
		= new TableColumn<city, String>("name");

		TableColumn<city, Integer> registrationDate//
		= new TableColumn<city, Integer>("registration date");


		TableColumn<city, String> DistrictName //
		= new TableColumn<city, String>("District");

		addButtonToTable();


		// ==== typeSoldierId (TEXT FIELD) ===
		cityId.setCellValueFactory(new PropertyValueFactory<>("cityId"));
		cityId.setMinWidth(200);

		// ==== typeSoldierName (TEXT FIELD) ===
		CityPostCode.setCellValueFactory(new PropertyValueFactory<>("CityPostCode"));
		CityPostCode.setMinWidth(200);

		// ==== creationDate (TEXT FIELD) ===
		cityName.setCellValueFactory(new PropertyValueFactory<>("cityName"));
		cityName.setMinWidth(200);

		// ==== typeSoldierName (TEXT FIELD) ===
		registrationDate.setCellValueFactory(new PropertyValueFactory<>("registrationDate"));
		registrationDate.setMinWidth(200);

		// ==== creationDate (TEXT FIELD) ===
		DistrictName.setCellValueFactory(new PropertyValueFactory<>("DistrictName"));
		DistrictName.setMinWidth(200);

		ObservableList<city>list =  FXCollections.observableArrayList(cityDB.getCityList());
		table.setItems(list);

		table.getColumns().addAll(cityId, CityPostCode, cityName, registrationDate,DistrictName);

		stage.setTitle("add City for mission ");
		Scene scene = new Scene(grid, 1000, 700);
		stage.setScene(scene);
		stage.show();
	}

	private void addButtonToTable() {
		TableColumn<city, Void> colBtn = new TableColumn<city, Void>("select city");

		Callback<TableColumn<city, Void>, TableCell<city, Void>> cellFactory = new Callback<TableColumn<city, Void>, TableCell<city, Void>>() {
			@Override
			public TableCell<city, Void> call(final TableColumn<city, Void> param) {
				final TableCell<city, Void> cell = new TableCell<city, Void>() {
					private final Button btn = new Button("Select");
					{
						btn.setOnAction((ActionEvent event) -> 
						{
							city data = getTableView().getItems().get(getIndex());
							cityName = data.getCityName() ;
							ctrl.selectCityForMission(stage, cityName, members, vehicles, weapons);
						});
					}
					@Override
					public void updateItem(Void item, boolean empty) {
						super.updateItem(item, empty);
						if (empty) {
							setGraphic(null);
						} else {
							setGraphic(btn);
						}
					}
				};
				return cell;
			}
		};

		colBtn.setCellFactory(cellFactory);
		table.getColumns().add(colBtn);
	}
}

package INTERFACEclasses;


import java.sql.SQLException;
import java.util.ArrayList;

import Controller.controller;
import DBclasses.vehicleDB;
import MODELclasses.member;
import MODELclasses.vehicle;
import MODELclasses.weapon;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.Callback;

public class selectVehicleForMissionInterface extends Application
{
	private TableView<vehicle> table ;
	private controller ctrl;
	private Stage stage;
	
	private String cityName ;
	private ArrayList <member> members ;
	private ArrayList <vehicle> vehicles  ;
	private ArrayList <weapon> weapons  ;
	/**
	 * constructor of the controller
	 * @param ctrl
	 */
	public selectVehicleForMissionInterface(controller ctrl){
		this.ctrl = ctrl;
		members = new ArrayList <member> () ;
		vehicles = new ArrayList <vehicle> () ; 
		weapons = new ArrayList <weapon> () ;
	}
	
	public selectVehicleForMissionInterface(controller ctrl, String cityName,
			ArrayList <member> members, ArrayList <vehicle> vehicles,
			ArrayList <weapon> weapons)
	{
		this(ctrl) ;
		this.cityName = cityName;
		this.members = members ;
		this.vehicles = vehicles ;
		this.weapons = weapons ;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void start(Stage primaryStage) throws ClassNotFoundException, SQLException 
	{
		this.stage = primaryStage ;
		GridPane grid = new GridPane() ;
		table = new TableView<vehicle>();
		grid.add(table, 0, 1);

		table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

		TableColumn<vehicle, Integer> vehicleNameCol
		= new TableColumn<vehicle, Integer>("serial number");

		TableColumn<vehicle, String> typeVehicleCol 
		= new TableColumn<vehicle, String>("vehicle model");

		TableColumn<vehicle, String> vehicleRegistrationDateCol
		= new TableColumn<vehicle, String>("registration date");

		addButtonToTable();

		// ==== weaponSN ===
		vehicleNameCol.setCellValueFactory(new PropertyValueFactory<>("vehicleLP"));
		vehicleNameCol.setMinWidth(50);

		// ==== registrationDate ===
		vehicleRegistrationDateCol.setCellValueFactory(new PropertyValueFactory<>("registrationDate"));
		vehicleRegistrationDateCol.setMinWidth(100);

		// ==== typeWeaponName ===
		typeVehicleCol.setCellValueFactory(new PropertyValueFactory<>("typeVehicleName"));
		typeVehicleCol.setMinWidth(100);



		ObservableList<vehicle> list = FXCollections.observableArrayList(vehicleDB.getVehiclesList());
		table.setItems(list);
		table.getColumns().addAll(vehicleNameCol, vehicleRegistrationDateCol, typeVehicleCol) ;

		stage.setTitle("select vehicle");
		Scene scene = new Scene(grid, 1000, 700);
		stage.setScene(scene);
		stage.show();
	}
	private void addButtonToTable() {
		//create column who take information of model class called vehicle
		TableColumn<vehicle, Void> colBtn = new TableColumn<vehicle, Void>("select");

		Callback<TableColumn<vehicle, Void>, TableCell<vehicle, Void>> cellFactory = new Callback<TableColumn<vehicle, Void>, TableCell<vehicle, Void>>() {
			@Override
			public TableCell<vehicle, Void> call(final TableColumn<vehicle, Void> param) {
				final TableCell<vehicle, Void> cell = new TableCell<vehicle, Void>() {

					private final Button btn = new Button("Select");

					{
						btn.setOnAction((ActionEvent event) -> {
							vehicle data = getTableView().getItems().get(getIndex());
							if(!vehicles.contains(data))
								vehicles.add(data) ;
							ctrl.selectVehicleForMission(stage, cityName, members, vehicles, weapons);
						});
					}
					@Override
					public void updateItem(Void item, boolean empty) {
						super.updateItem(item, empty);
						if (empty) {
							setGraphic(null);
						} else {
							setGraphic(btn);
						}
					}
				};
				return cell;
			}
		};

		colBtn.setCellFactory(cellFactory);

		table.getColumns().add(colBtn);
	}
}
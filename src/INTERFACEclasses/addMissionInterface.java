package INTERFACEclasses;

import java.util.ArrayList;

import Controller.controller;
import MODELclasses.member;
import MODELclasses.vehicle;
import MODELclasses.weapon;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class addMissionInterface extends Application {


	private controller ctrl;
	private String cityName = "";
	private ArrayList <member> members ;
	private ArrayList <vehicle> vehicles  ;
	private ArrayList <weapon> weapons  ;

	/**
	 * Constructor of controller
	 * @param ctrl
	 */
	public addMissionInterface(controller ctrl)
	{
		this.ctrl = ctrl;
		members = new ArrayList <member> () ;
		vehicles = new ArrayList <vehicle> () ; 
		weapons = new ArrayList <weapon> () ;
	}

	public addMissionInterface(controller ctrl, String cityName,
			ArrayList <member> members, ArrayList <vehicle> vehicles,
			ArrayList <weapon> weapons)
	{
		this(ctrl) ;
		this.cityName = cityName;
		this.members = members ;
		this.vehicles = vehicles ;
		this.weapons = weapons ;
	}

	@Override
	public void start(Stage primaryStage) {

		Stage Window = primaryStage;
		Window.setTitle("Add Mission in DB");

		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setVgap(10);
		grid.setHgap(10);

		Text addMission = new Text("Add Mission in DB");
		addMission.setFont(Font.font("Tahoma", FontWeight.LIGHT, 25));
		grid.add(addMission,1,0);

		Label addMissionName = new Label("Mission's name : ");
		grid.add(addMissionName,0,1);

		TextField addMissionNameText = new TextField();
		grid.add(addMissionNameText,1,1);

		Label addMissionDescription = new Label("Mission's Description :");
		grid.add(addMissionDescription,0,2);

		TextField addMissionDescriptionText = new TextField();
		grid.add(addMissionDescriptionText,1,2);

		Label addMissionCity = new Label("Mission's city : ");
		grid.add(addMissionCity,0,3);

		Button selectCityForMission = new Button("Select");
		grid.add(selectCityForMission,1,3);

		Label city = new Label(cityName);
		grid.add(city,2,4);

		selectCityForMission.setOnAction(selectCityEvent -> {
			ctrl.lancerSelectCityForMission(primaryStage, cityName, members, vehicles, weapons);
		}
				);

		Label addMissionWeapon = new Label("Mission's weapon : ");
		grid.add(addMissionWeapon,0,4);

		Button selectWeaponForMissionButton = new Button("Select");
		grid.add(selectWeaponForMissionButton,1,4);
		selectWeaponForMissionButton.setOnAction(selectWeaponEvent->
		{
			ctrl.lancerWeaponForMission(primaryStage, cityName, members, vehicles, weapons);
		}
				);

		Label weaponsLabel = new Label(displayWeaponList(weapons));
		grid.add(weaponsLabel,2,4);

		Label addMissionMemberLabel = new Label("Mission's member : ");
		grid.add(addMissionMemberLabel,0,5);

		Button selectMemberForMissionButton = new Button("Select");
		grid.add(selectMemberForMissionButton,1,5);
		selectMemberForMissionButton.setOnAction(selectMemberEvent->
		{
			ctrl.lancerMemberForMission(primaryStage, cityName, members, vehicles, weapons);		
		}
				);

		Label membersLabel = new Label(displayMemberList(members));
		grid.add(membersLabel,2,5);

		Label addMissionVehicleLabel = new Label("Mission's vehicles : ");
		grid.add(addMissionVehicleLabel,0,6);

		Button selectVehicleForMissionButton = new Button("Select from existing vehicles");
		grid.add(selectVehicleForMissionButton,1,6);
		selectVehicleForMissionButton.setOnAction(selectVehicleEvent->
		{
			ctrl.lancerVehicleForMission(primaryStage, cityName, members, vehicles, weapons);
		}
				);

		Label vehiclesLabel = new Label(displayVehicleList(vehicles));
		grid.add(vehiclesLabel,2,6);

		Button addMissionButton = new Button("Add this mission");
		grid.add(addMissionButton,1,7);
		addMissionButton.setOnAction(addMissionEvent->
		{
			controller.addMission(addMissionNameText.getText(), addMissionDescriptionText.getText() 
					,cityName, members, weapons, vehicles, ctrl, Window) ;
		}
				);

		Button launchAccueilButton =new Button("Accueil");
		grid.add(launchAccueilButton,4,0);
		launchAccueilButton.setOnAction(launchAccueilEvent->
		{
			ctrl.lancerAccueil(primaryStage);
		}
				);

		Scene scene = new Scene(grid,1000,700);
		Window.setScene(scene);
		Window.show();
	}

	public static String displayMemberList(ArrayList <member> list)
	{
		String text = ""; 
		if(list.isEmpty())
			return text ;
		else
		{
			for(member m : list)
				text += (m.getMilitaryId()+", ") ;
			return text.substring(0,text.length()-2).trim();
		}
	}	

	public static String displayVehicleList(ArrayList <vehicle> list)
	{
		String text = ""; 
		if(list.isEmpty())
			return text ;
		else
		{
			for(vehicle v : list)
				text += (v.getVehicleLP()+", ") ;
			return text.substring(0,text.length()-2).trim();
		}
	}

	public static String displayWeaponList(ArrayList <weapon> list)
	{
		String text = ""; 
		if(list.isEmpty())
			return text ;
		else
		{
			for(weapon w : list)
				text += (w.getWeaponSN()+", ") ;
			return text.substring(0,text.length()-2).trim();
		}
	}
}
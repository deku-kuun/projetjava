package INTERFACEclasses;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

public class AlertInterface 
{
	private final Alert error = new Alert(AlertType.ERROR);
	private final Alert information = new Alert(AlertType.INFORMATION);
	public AlertInterface(Exception e)
	{
		showError(e) ;
	}
	
	public AlertInterface(String message)
	{
		showSucces(message) ;
	}
	
	public void showError(Exception e) {
		error.setTitle("Pay Attention");
		error.setHeaderText("Error!");
		error.setContentText(e.getMessage());
		error.showAndWait();
	}
	
	public void showSucces(String message)
	{
		information.setTitle("Done");
		information.setHeaderText("Succes action !");
		information.setContentText(message) ;
		information.showAndWait();
	}
}

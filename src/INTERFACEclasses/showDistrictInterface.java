package INTERFACEclasses;


import java.sql.SQLException;

import Controller.controller;
import DBclasses.districtDB;
import MODELclasses.district;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class showDistrictInterface extends Application {
	private TableView<district> table ;

	private controller ctrl;
	/**
	 * constructor of the controller
	 * @param ctrl
	 */
	public showDistrictInterface(controller ctrl){
		this.ctrl = ctrl;
	}

	public showDistrictInterface() 
	{
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public void start(Stage stage) throws ClassNotFoundException, SQLException {
		GridPane grid = new GridPane() ;
		table = new TableView<district>();
		
		grid.add(table, 0, 1);

		table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

		TableColumn<district, Integer> districtId
		= new TableColumn<district, Integer>("Id");


		TableColumn<district, String> districtCode 
		= new TableColumn<district, String>("International Code");


		TableColumn<district, String> districtName
		= new TableColumn<district, String>("Name");

		TableColumn<district, String> registrationDate 
		= new TableColumn<district, String>("registration Date");


		TableColumn<district, String> countryName
		= new TableColumn<district, String>("Country");

	


		// ==== districtId ===
		districtId.setCellValueFactory(new PropertyValueFactory<>("districtId"));
		districtId.setMinWidth(50);
		
		// ==== districtCode ===
		districtCode.setCellValueFactory(new PropertyValueFactory<>("districtCode"));
		districtCode.setMinWidth(50);

		// ==== districtName ===
		districtName.setCellValueFactory(new PropertyValueFactory<>("districtName"));
		districtName.setMinWidth(50);

		// ==== registrationDate ===
		registrationDate.setCellValueFactory(new PropertyValueFactory<>("registrationDate"));
		registrationDate.setMinWidth(80);

		// ==== countryName  ===
		countryName.setCellValueFactory(new PropertyValueFactory<>("countryName"));
		countryName.setMinWidth(50);

		ObservableList<district> list =  FXCollections.observableArrayList(districtDB.getDistrictList());
		table.setItems(list);

		table.getColumns().addAll(districtId, districtCode,districtName, registrationDate,countryName);
		Button accueilButton = new Button("Accueil") ;
		grid.add(accueilButton, 0, 0);
		
		accueilButton.setOnAction(new EventHandler<ActionEvent>() {

			public void handle (ActionEvent event) {
				try {
					ctrl.lancerAccueil(stage);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
				);
		
		
		
		
		stage.setTitle("show district");

		Scene scene = new Scene(grid, 1000, 700);
		stage.setScene(scene);
		stage.show();


	}
	

	public static void main(String[] args) {
		launch(args);
	}

}

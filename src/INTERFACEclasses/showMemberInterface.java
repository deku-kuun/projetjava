package INTERFACEclasses;


import java.sql.SQLException;

import Controller.controller;
import DBclasses.memberDB;
import MODELclasses.member;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class showMemberInterface extends Application {
	private TableView<member> table ;
	private controller ctrl;
	private Stage stage;

	public showMemberInterface(controller ctrl){
		this.ctrl = ctrl;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void start(Stage stage) throws ClassNotFoundException, SQLException {

		this.stage = stage;

		GridPane grid = new GridPane() ;

		table = new TableView<member>();
		TableColumn<member, Integer> MilitaryId//
		= new TableColumn<member, Integer>("MilitaryId");

		TableColumn<member, String> Email //
		= new TableColumn<member, String>("Email");

		TableColumn<member, Integer> Surname //
		= new TableColumn<member, Integer>("Surname");

		TableColumn<member, Integer> Firstname//
		= new TableColumn<member, Integer>("Firstname");

		TableColumn<member, String> picName//
		= new TableColumn<member, String>("picName");

		TableColumn<member, String> registrationDate //
		= new TableColumn<member, String>("Registration Date");

		TableColumn<member, String> rankName //
		= new TableColumn<member, String>("rankName ");


		// ==== MilitaryId ===
		MilitaryId.setCellValueFactory(new PropertyValueFactory<>("MilitaryId"));
		MilitaryId.setMinWidth(100);

		// ==== Email ===
		Email.setCellValueFactory(new PropertyValueFactory<>("Email"));
		Email.setMinWidth(80);

		// ==== Surname ===
		Surname.setCellValueFactory(new PropertyValueFactory<>("Surname"));
		Surname.setMinWidth(100);

		// ==== Firstname ===
		Firstname.setCellValueFactory(new PropertyValueFactory<>("Firstname"));
		Firstname.setMinWidth(50);

		// ==== picName ===
		picName.setCellValueFactory(new PropertyValueFactory<>("picName"));
		picName.setMinWidth(30);

		// ==== registrationDate ===
		registrationDate.setCellValueFactory(new PropertyValueFactory<>("registrationDate"));
		registrationDate.setMinWidth(30);

		// ==== rankName ===
		rankName.setCellValueFactory(new PropertyValueFactory<>("rankName"));
		rankName.setMinWidth(30);



		ObservableList<member> list = FXCollections.observableArrayList(memberDB.getMemberList());
		table.setItems(list);

		table.getColumns().addAll(MilitaryId,Email, Surname,Firstname, picName, registrationDate, rankName);
		grid.add(table, 0, 1);

		Button accueilButton = new Button("Accueil") ;
		grid.add(accueilButton, 0, 0);

		accueilButton.setOnAction(new EventHandler<ActionEvent>() {

			public void handle (ActionEvent event) {
				try {
					ctrl.lancerAccueil(stage);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
				);

		stage.setTitle("show city ");

		Scene scene = new Scene(grid, 1000, 700);
		stage.setScene(scene);
		stage.show();


	}


	public static void main(String[] args) {
		launch(args);
	}

}

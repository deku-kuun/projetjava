package INTERFACEclasses;



import Controller.controller;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class addTypeVehicleInterface extends Application
{
	private controller ctrl;
	private Stage stage ;
	private String catVehicleName ;

	public addTypeVehicleInterface(controller ctrl)
	{
		this.ctrl = ctrl;
	}

	public addTypeVehicleInterface(controller ctrl, String catVehicleName)
	{
		this(ctrl) ;
		this.catVehicleName = catVehicleName ;
	}

	@Override
	public void start(Stage primaryStage) 
	{
		stage = primaryStage;
		stage.setTitle("New type vehicle");

		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setVgap(10);
		grid.setHgap(10);

		Text title = new Text("Add a new type of vehicle");
		title.setFont(Font.font("Tahoma", FontWeight.LIGHT, 25));
		grid.add(title,1,0);

		Label typeVehicleName = new Label(" type Vehicle Name ");
		grid.add(typeVehicleName,0,1);

		TextField typeVehicleNameText  = new TextField();
		typeVehicleNameText.setPromptText("type Vehicle Name");
		grid.add(typeVehicleNameText,1,1);

		Label typeVehicleDescription = new Label("type Vehicle Description");
		grid.add(typeVehicleDescription,0,2);

		TextField typeVehicleDescriptionText= new TextField();
		typeVehicleDescriptionText.setPromptText("type Vehicle Description");
		grid.add(typeVehicleDescriptionText,1,2);

		Label typeVehicleMaxSpeed = new Label("type Vehicle Max Speed");
		grid.add(typeVehicleMaxSpeed,0,3);

		TextField typeVehicleMaxSpeedText  = new TextField();
		typeVehicleMaxSpeedText.setPromptText("type Vehicle Max Speed");
		grid.add(typeVehicleMaxSpeedText,1,3);

		Label typeVehiclePlaces = new Label("type Vehicle Places");
		grid.add(typeVehiclePlaces,0,4);

		TextField typeVehiclePlacesText  = new TextField();
		typeVehiclePlacesText.setPromptText("type Vehicle Places");
		grid.add(typeVehiclePlacesText,1,4);

		Label typeVehiclePic = new Label("type Vehicle Pic");
		grid.add(typeVehiclePic,0,5);

		TextField typeVehiclePicText  = new TextField();
		typeVehiclePicText.setPromptText("type Vehicle Pic");
		grid.add(typeVehiclePicText,1,5);

		Label selectCatVehicle = new Label("select Cat Vehicle");
		grid.add(selectCatVehicle,0,6);

		Button selectCatVehicleButton = new Button("Select");
		grid.add(selectCatVehicleButton,1,6);
		selectCatVehicleButton.setOnAction(selectCatVehicleEvent->
		{
			ctrl.lancerSelectCatVehicleForTypeVehicle(primaryStage);
		});

		Label catVehicle = new Label(catVehicleName) ;
		grid.add(catVehicle,2,6);

		Button addTypeWeaponInDB = new Button("Add this type of vehicle");
		addTypeWeaponInDB.setOnAction(e->
		{
			controller.addTypeWeapon( typeVehicleNameText.getText(),  typeVehicleDescriptionText.getText(), 
					typeVehicleMaxSpeedText.getText(),  
					typeVehiclePlacesText.getText(),  typeVehiclePicText.getText(), catVehicle.getText());
		});
		grid.add(addTypeWeaponInDB,1,9);

		Button launchAccueilButton =new Button("Accueil");
		grid.add(launchAccueilButton,4,0);
		launchAccueilButton.setOnAction(launchAccueilEvent->
		{
			ctrl.lancerAccueil(primaryStage);
		}
				);

		Scene scene = new Scene(grid,1000,700);
		stage.setScene(scene);
		stage.show();
	}	
}
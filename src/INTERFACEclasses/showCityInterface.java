package INTERFACEclasses;


import java.sql.SQLException;

import Controller.controller;
import DBclasses.cityDB;
import MODELclasses.city;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class showCityInterface extends Application {
	private TableView<city> table ;
	private controller ctrl;
	private Stage stage;

	public showCityInterface(controller ctrl){
		this.ctrl = ctrl;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void start(Stage stage) throws ClassNotFoundException, SQLException {

		this.stage = stage;
		GridPane grid = new GridPane() ;
		table = new TableView<city>();
		//table.set

		grid.add(table, 0, 1);
		TableColumn<city, Integer> cityId//
		= new TableColumn<city, Integer>("cityId");


		TableColumn<city, String> CityPostCode //
		= new TableColumn<city, String>("CityPostCode");


		TableColumn<city, String> cityName//
		= new TableColumn<city, String>("cityName");

		TableColumn<city, Integer> registrationDate//
		= new TableColumn<city, Integer>("registrationDate");


		TableColumn<city, String> DistrictName //
		= new TableColumn<city, String>("DistrictName");



		// ==== typeSoldierId (TEXT FIELD) ===
		cityId.setCellValueFactory(new PropertyValueFactory<>("cityId"));
		cityId.setMinWidth(200);

		// ==== typeSoldierName (TEXT FIELD) ===
		CityPostCode.setCellValueFactory(new PropertyValueFactory<>("CityPostCode"));
		CityPostCode.setMinWidth(200);

		// ==== creationDate (TEXT FIELD) ===
		cityName.setCellValueFactory(new PropertyValueFactory<>("cityName"));
		cityName.setMinWidth(200);

		// ==== typeSoldierName (TEXT FIELD) ===
		registrationDate.setCellValueFactory(new PropertyValueFactory<>("registrationDate"));
		registrationDate.setMinWidth(200);

		// ==== creationDate (TEXT FIELD) ===
		DistrictName.setCellValueFactory(new PropertyValueFactory<>("DistrictName"));
		DistrictName.setMinWidth(200);

		ObservableList<city>list =  FXCollections.observableArrayList(cityDB.getCityList());
		table.setItems(list);

		table.getColumns().addAll(cityId, CityPostCode, cityName, registrationDate,DistrictName);

		
		Button accueilButton = new Button("Accueil") ;
		grid.add(accueilButton, 0, 0);
		
		accueilButton.setOnAction(new EventHandler<ActionEvent>() {

			public void handle (ActionEvent event) {
				try {
					ctrl.lancerAccueil(stage);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
				);
		
		stage.setTitle("show city ");

		Scene scene = new Scene(grid, 1000, 700);
		stage.setScene(scene);
		stage.show();


	}


	public static void main(String[] args) {
		launch(args);
	}

}

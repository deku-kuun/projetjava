package INTERFACEclasses;

import Controller.controller;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class connectionInterface extends Application {

	public static String militaryId ;
	private controller ctrl;
	private Stage stage ;
	/**
	 * Constructor of the controller
	 * @param ctrl
	 */
	public connectionInterface(controller ctrl){
		this.ctrl = ctrl;
	}

	@Override
	public void start(Stage primaryStage) {
		stage = primaryStage;
		stage.setTitle("Login");

		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setVgap(10);
		grid.setHgap(10);

		Text connexion = new Text("Login");
		connexion.setFont(Font.font("Tahoma", FontWeight.LIGHT, 35));
		grid.add(connexion,1,0);


		Label militaryIdLabel = new Label("Military Id");
		grid.add(militaryIdLabel,0,1);

		TextField militaryIdText = new TextField();
		militaryIdText.setPromptText("Military id");
		grid.add(militaryIdText,1,1);

		Label passwordLabel = new Label("Password");
		grid.add(passwordLabel,0,2);

		PasswordField passwordText= new PasswordField();
		passwordText.setPromptText("password");
		grid.add(passwordText,1,2);

		Button loginButton = new Button("Login");
		grid.add(loginButton,1,3);


		loginButton.setOnAction(loginEvent -> 
		{
			//TestConnection member
			ctrl.testConnection(stage, militaryIdText.getText(), passwordText.getText()) ;
			militaryId = militaryIdText.getText() ;
		}
		) ;

		Scene scene = new Scene(grid,1000,700);
		stage.setScene(scene);
		stage.show();
	}
}
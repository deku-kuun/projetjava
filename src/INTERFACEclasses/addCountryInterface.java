package INTERFACEclasses;

import Controller.controller;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class addCountryInterface extends Application 
{
	private controller ctrl;
	/**
	 * Constructor of the controller for this class
	 * @param ctrl
	 */
	public addCountryInterface(controller ctrl){
		this.ctrl = ctrl;
	}
	
	/**
	 * main method and launch the frame
	 * @param args
	 */
	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {

		Stage Window = primaryStage;
		Window.setTitle("New country");

		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setVgap(10);
		grid.setHgap(10);

		Text title = new Text("Add a new country");
		title.setFont(Font.font("Tahoma", FontWeight.LIGHT, 25));
		grid.add(title,1,0);

		Label countryCodeLabel = new Label("Country international code");
		grid.add(countryCodeLabel,0,1);

		TextField countryCodeText = new TextField();
		grid.add(countryCodeText,1,1);

		Label countryNameLabel = new Label("Country name");
		grid.add(countryNameLabel,0,2);

		TextField countryNameText = new TextField();
		grid.add(countryNameText,1,2);

		Button addCountryButton = new Button("Add this country");
		grid.add(addCountryButton,1,4);
		
		addCountryButton.setOnAction(addCountryEvent-> 
		{
			controller.addCountry(countryCodeText.getText().trim(),countryNameText.getText().trim());
		});	
	
		Button launchAccueilButton =new Button("Accueil");
		grid.add(launchAccueilButton,4,0);
		launchAccueilButton.setOnAction(launchAccueilEvent->
		{
			ctrl.lancerAccueil(primaryStage);
		}
		);

		Scene scene = new Scene(grid,1000,700);
		Window.setScene(scene);
		Window.show();
	}

	

}
package INTERFACEclasses;

import Controller.controller;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class addWeaponInterface extends Application {
	
	private controller ctrl;
	private String typeWeaponName = "";
	private Stage stage;

	/**
	 * Constructor of the controller
	 * @param ctrl
	 */
	public addWeaponInterface(controller ctrl){
		this.ctrl = ctrl;
	}

	public addWeaponInterface(controller ctrl, String typeWeaponName)
	{
		this(ctrl) ;
		this.typeWeaponName = typeWeaponName ;
	}

	@Override
	public void start(Stage primaryStage) {

		stage = primaryStage;
		stage.setTitle("New weapon");

		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setVgap(10);
		grid.setHgap(10);

		Text addCatVehicle = new Text("Add a new weapon");
		addCatVehicle.setFont(Font.font("Tahoma", FontWeight.LIGHT, 25));
		grid.add(addCatVehicle,1,0);

		Label weaponSN = new Label("Weapon serial number");
		grid.add(weaponSN,0,1);

		TextField weaponSNText = new TextField();
		grid.add(weaponSNText,1,1);

		Label typeWeaponLabel = new Label("weapon model");
		grid.add(typeWeaponLabel,0,2);

		Button selectTypeWeapon = new Button("Select from existing types");
		grid.add(selectTypeWeapon,1,2);

		selectTypeWeapon.setOnAction(selectTypeWeaponEvent -> 
		{
			ctrl.lancerSelectTypeWeaponForWeapon(primaryStage);
		}
				);

		Label typeWeapon = new Label(typeWeaponName);
		grid.add(typeWeapon,2,2);

		Button addWeaponinDB = new Button("Add this weapon");
		grid.add(addWeaponinDB,1,3);

		Button launchAccueilButton =new Button("Accueil");
		grid.add(launchAccueilButton,4,0);
		launchAccueilButton.setOnAction(launchAccueilEvent->
		{
			ctrl.lancerAccueil(primaryStage);
		}
				);

		Scene scene = new Scene(grid,1000,700);
		stage.setScene(scene);
		stage.show();
	}
}
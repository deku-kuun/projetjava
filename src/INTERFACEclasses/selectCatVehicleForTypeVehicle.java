package INTERFACEclasses;


import java.sql.SQLException;

import Controller.controller;
import DBclasses.catVehicleDB;
import MODELclasses.catVehicle;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.Callback;

public class selectCatVehicleForTypeVehicle extends Application {
	private controller ctrl;
	private TableView<catVehicle> table ;
	private Stage stage ;
	public selectCatVehicleForTypeVehicle(controller ctrl)
	{
		this.ctrl = ctrl;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void start(Stage primaryStage) throws ClassNotFoundException, SQLException 
	{
		stage = primaryStage ;
		table  = new TableView<catVehicle>();
		GridPane grid = new GridPane();		
		grid.add(table, 0, 1);

		TableColumn<catVehicle, Integer> catVehicleId//
		= new TableColumn<catVehicle, Integer>("catVehicleId");


		TableColumn<catVehicle, String> catVehicleName //
		= new TableColumn<catVehicle, String>("catVehicleName");


		TableColumn<catVehicle, String> registrationDate//
		= new TableColumn<catVehicle, String>("registrationDate");

		addButtonToTable();

		// ==== catVehicleId (TEXT FIELD) ===
		catVehicleId.setCellValueFactory(new PropertyValueFactory<>("catVehicleId"));
		catVehicleId.setMinWidth(200);

		// ==== catVehicleName (TEXT FIELD) ===
		catVehicleName.setCellValueFactory(new PropertyValueFactory<>("catVehicleName"));
		catVehicleName.setMinWidth(200);

		// ==== registrationDate (TEXT FIELD) ===
		registrationDate.setCellValueFactory(new PropertyValueFactory<>("registrationDate"));
		registrationDate.setMinWidth(200);


		ObservableList <catVehicle> list =  FXCollections.observableArrayList(catVehicleDB.getCatVehicleList());
		table.setItems(list);
		table.getColumns().addAll(catVehicleId, catVehicleName, registrationDate);

		Button accueilButton = new Button("Accueil") ;
		grid.add(accueilButton, 0, 0);

		accueilButton.setOnAction(eventAcc->
		{
			try 
			{
				ctrl.lancerAccueil(stage);
			} 
			catch (Exception e) 
			{		
				new AlertInterface(e) ;
			}
		} );

		stage.setTitle("show cat vehicle");
		Scene scene = new Scene(grid, 1000, 700);
		stage.setScene(scene);
		stage.show();

	}
	private void addButtonToTable() {
		TableColumn<catVehicle, Void> colBtn = new TableColumn<catVehicle, Void>("select Cat Vehicle");

		Callback<TableColumn<catVehicle, Void>, TableCell<catVehicle, Void>> cellFactory = new Callback<TableColumn<catVehicle, Void>, TableCell<catVehicle, Void>>() {
			@Override
			public TableCell<catVehicle, Void> call(final TableColumn<catVehicle, Void> param) {
				final TableCell<catVehicle, Void> cell = new TableCell<catVehicle, Void>() {
					private final Button btn = new Button("Select");
					{
						btn.setOnAction((ActionEvent event) -> 
						{
							catVehicle data = getTableView().getItems().get(getIndex());
							ctrl.selectCatVehicleForTypeVehicle(stage, data.getCatVehicleName());
						});
					}
					@Override
					public void updateItem(Void item, boolean empty) {
						super.updateItem(item, empty);
						if (empty) {
							setGraphic(null);
						} else {
							setGraphic(btn);
						}
					}
				};
				return cell;
			}
		};

		colBtn.setCellFactory(cellFactory);
		table.getColumns().add(colBtn);
	}

	public static void main(String[] args) {
		launch(args);
	}

}

package INTERFACEclasses;

import Controller.controller;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class addCatWeaponInterface extends Application 
{
	private controller ctrl;
	/**
	 * constructor of the controller
	 * @param ctrl
	 */
	public addCatWeaponInterface(controller ctrl){
		this.ctrl = ctrl;
	}
	/**
	 * main method, she launch the window
	 * @param args
	 */
	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) 
	{
		Stage Window = primaryStage;
		Window.setTitle("New weapon category");

		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setVgap(10);
		grid.setHgap(10);

		Text title = new Text("Add a new weapon category");
		title.setFont(Font.font("Tahoma", FontWeight.LIGHT, 25));
		grid.add(title,1,0);

		Label catWeaponNameLabel = new Label("Category name");
		grid.add(catWeaponNameLabel,0,1);

		TextField catWeaponNameText = new TextField();
		grid.add(catWeaponNameText,1,1);

		Button addCatWeaponButton = new Button("Add this category");
		grid.add(addCatWeaponButton,1,2);

		addCatWeaponButton.setOnAction(addCatWeaponEvent -> 
		{
			controller.addCatWeapon(catWeaponNameText.getText().trim());
		});	

		Button launchAccueilButton =new Button("Accueil");
		grid.add(launchAccueilButton,4,0);
		launchAccueilButton.setOnAction(launchAccueilEvent-> 
		{
			ctrl.lancerAccueil(primaryStage);
		} );

		Scene scene = new Scene(grid,1000,700);
		Window.setScene(scene);
		Window.show();
	}
}
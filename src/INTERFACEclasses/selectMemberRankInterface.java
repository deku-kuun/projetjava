package INTERFACEclasses;


import java.sql.SQLException;

import Controller.controller;
import DBclasses.rankDB;
import MODELclasses.rank;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.Callback;

public class selectMemberRankInterface extends Application 
{
	private TableView <rank> table ;
	private controller ctrl;
	private Stage stage ;
	public selectMemberRankInterface(controller ctrl)
	{
		this.ctrl = ctrl;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void start(Stage primaryStage) throws ClassNotFoundException, SQLException
	{
		this.stage = primaryStage ;
		GridPane grid = new GridPane() ;
		table = new TableView<rank>();
		//table.set
		grid.add(table, 0, 1);

		TableColumn<rank, Integer> rankId//
		= new TableColumn<rank, Integer>("rankId");


		TableColumn<rank, String> rankCode //
		= new TableColumn<rank, String>("rankCode");


		TableColumn<rank, String> rankName//
		= new TableColumn<rank, String>("rankName");

		TableColumn<rank, String> creationDate//
		= new TableColumn<rank, String>("creationDate");

		addButtonToTable();

		// ==== typeSoldierId (TEXT FIELD) ===
		rankId.setCellValueFactory(new PropertyValueFactory<>("rankId"));
		rankId.setMinWidth(200);

		// ==== typeSoldierName (TEXT FIELD) ===
		rankCode.setCellValueFactory(new PropertyValueFactory<>("rankCode"));
		rankCode.setMinWidth(200);

		// ==== creationDate (TEXT FIELD) ===
		rankName.setCellValueFactory(new PropertyValueFactory<>("rankName"));
		rankName.setMinWidth(200);

		// ==== creationDate (TEXT FIELD) ===
		creationDate.setCellValueFactory(new PropertyValueFactory<>("creationDate"));
		creationDate.setMinWidth(200);

		ObservableList<rank> list =  FXCollections.observableArrayList(rankDB.getRankList());
		table.setItems(list);
		table.getColumns().addAll(rankId, rankCode, rankName, creationDate);

		Button accueilButton = new Button("Accueil") ;
		grid.add(accueilButton, 0, 0);
		accueilButton.setOnAction(accueilButtonEvent -> 
		{
			ctrl.lancerAccueil(stage);
		}
				);

		stage.setTitle("select member rank");
		Scene scene = new Scene(grid, 600, 500);
		stage.setScene(scene);
		stage.show();
	}

	private void addButtonToTable() {
		TableColumn<rank, Void> colBtn = new TableColumn<rank, Void>("select rank");
		colBtn.setMinWidth(70);
		Callback<TableColumn<rank, Void>, TableCell<rank, Void>> cellFactory = new Callback<TableColumn<rank, Void>, TableCell<rank, Void>>() {
			@Override
			public TableCell<rank, Void> call(final TableColumn<rank, Void> param) {
				final TableCell<rank, Void> cell = new TableCell<rank, Void>() {
					private final Button btn = new Button("Select");
					{
						btn.setOnAction(event -> {
							rank data = getTableView().getItems().get(getIndex());
							ctrl.selectMemberRank(stage, data.getRankName());
						});
					}
					@Override
					public void updateItem(Void item, boolean empty) {
						super.updateItem(item, empty);
						if (empty) {
							setGraphic(null);
						} else {
							setGraphic(btn);
						}
					}
				}; return cell;	
			}
		};

		colBtn.setCellFactory(cellFactory);

		table.getColumns().add(colBtn);
	}
	public static void main(String[] args) {
		launch(args);
	}
}
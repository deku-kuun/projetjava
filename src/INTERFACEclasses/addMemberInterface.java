package INTERFACEclasses;

import Controller.controller;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class addMemberInterface extends Application
{
	private controller ctrl;
	private Stage stage ;
	private String rankName = "";
	private String typeSoldierName = "" ;
	private String squadName = "" ;
	/**
	 * Constructor of the controller
	 * @param ctrl
	 */
	
	public addMemberInterface(controller ctrl){
		this.ctrl = ctrl;
	}

	public addMemberInterface(controller ctrl, String rankName) {
		this(ctrl) ;
		this.rankName = rankName ;
	}

	public addMemberInterface(controller ctrl, String rankName,
			String squadName, String typeSoldierName)
	{
		this(ctrl) ;
		this.rankName = rankName ;
		this.typeSoldierName = typeSoldierName ;
		this.squadName = squadName ;
	}

	@Override
	public void start(Stage primaryStage) 
	{
		stage = primaryStage;
		stage.setTitle("new member");

		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setVgap(10);
		grid.setHgap(10);

		Text Ajouter = new Text("Add a new member");
		Ajouter.setFont(Font.font("Tahoma", FontWeight.LIGHT, 25));
		grid.add(Ajouter,1,0);

		Label MilitaryId = new Label("Military id");
		grid.add(MilitaryId,0,1);

		TextField MilitaryIdText  = new TextField();
		MilitaryIdText.setPromptText("Military Id");
		grid.add(MilitaryIdText,1,1);

		Label password = new Label("Password");
		grid.add(password,0,2);

		Label passwordText= new Label();
		passwordText.setText("password");
		grid.add(passwordText,1,2);

		Label email = new Label("email");
		grid.add(email,0,3);

		TextField emailText  = new TextField();
		emailText.setPromptText("email");
		grid.add(emailText,1,3);

		Label surName = new Label("surName");
		grid.add(surName,0,4);

		TextField surNameText  = new TextField();
		surNameText.setPromptText("surName");
		grid.add(surNameText,1,4);

		Label firstName = new Label("firstName");
		grid.add(firstName,0,5);

		TextField firstNameText  = new TextField();
		firstNameText.setPromptText("firstName");
		grid.add(firstNameText,1,5);

		Label picName = new Label("picture path");
		grid.add(picName,0,6);

		TextField picNameText  = new TextField();
		picNameText.setPromptText("picName");
		grid.add(picNameText,1,6);

		Button launchAccueilButton =new Button("Accueil");
		grid.add(launchAccueilButton,4,0);
		launchAccueilButton.setOnAction(launchAccueilEvent->
		{
			ctrl.lancerAccueil(primaryStage);
		}
				);

		Button generateButton = new Button("generate");
		grid.add(generateButton,2,2);
		generateButton.setOnAction(generateButtonEvent -> 
		{
			passwordText.setText(controller.generatePassword());
		});

		Label MemberRank = new Label("Member rank");
		grid.add(MemberRank,0,7);

		Button selectMemberRank = new Button("Select from existing ranks");
		grid.add(selectMemberRank,1,7);
		selectMemberRank.setOnAction(selectMemberRankEvent->
		{
			ctrl.lancerSelectMemberRank(primaryStage);
		} 
				);

		Label rank = new Label(rankName);
		grid.add(rank,2,7);

		Label squad = new Label(squadName);
		Label typeSoldier = new Label(typeSoldierName);
		if(rankName.compareTo("Soldier")==0)
		{
			Label typeSoldierLabel = new Label("Soldier's type");
			grid.add(typeSoldierLabel,0,8);

			Button selectSoldierType = new Button("Select from existing types");
			grid.add(selectSoldierType,1,8);
			selectSoldierType.setOnAction(selectSoldierTypeEvent -> 
			{
				ctrl.lancerSelectTypeSoldier(primaryStage, squadName);
			}
					);

			grid.add(typeSoldier,2,8);

			Label soldierSquad = new Label("Soldier squad");
			grid.add(soldierSquad,0,9);

			Button selectSoldierSquad = new Button("Select from existing squads");
			grid.add(selectSoldierSquad,1,9);
			selectSoldierSquad.setOnAction(selectSoldierSquadEvent->
			{
				ctrl.lancerSelectSoldierSquad(primaryStage,typeSoldierName);
			}
					);
			grid.add(squad,2,9);
		}

		Button addMemberButton = new Button("Add this member");
		grid.add(addMemberButton,1,10);
		addMemberButton.setOnAction(addMemberButtonEvent -> 
		{
			if(rankName.compareTo("Soldier")==0)
			{
				controller.addSoldier(MilitaryIdText.getText().trim(), passwordText.getText(), emailText.getText().trim(), 
						firstNameText.getText().trim(), surNameText.getText().trim(), picNameText.getText().trim(), 
						squad.getText(), typeSoldier.getText()) ;
			}
			else
			{
				controller.addMember(MilitaryIdText.getText().trim(),passwordText.getText(), emailText.getText().trim(), 
						firstNameText.getText().trim(), surNameText.getText().trim(), picNameText.getText().trim(), 
						rank.getText());
			}

		});	
		Scene scene = new Scene(grid,1000,700);
		stage.setScene(scene);
		stage.show();
	}
}
package INTERFACEclasses;


import java.sql.SQLException;

import Controller.controller;
import DBclasses.typeWeaponDB;
import MODELclasses.typeWeapon;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class showTypeWeapon extends Application { //class showTypeWeapon


	private TableView<typeWeapon> table ;
	private controller ctrl;
	
	public showTypeWeapon(controller ctrl){
		this.ctrl = ctrl;
	}
	@SuppressWarnings("unchecked")
	@Override
	public void start(Stage stage) throws ClassNotFoundException, SQLException {
		GridPane grid = new GridPane() ;
		table = new TableView<typeWeapon>();
		grid.add(table, 0, 1);

		table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

		TableColumn<typeWeapon, Integer> typeWeaponIdCol //
		= new TableColumn<typeWeapon, Integer>("Id");

		TableColumn<typeWeapon, Integer> typeWeaponNameCol//
		= new TableColumn<typeWeapon, Integer>("Name");

		TableColumn<typeWeapon, String> TypeWeaponDescriptionCol//
		= new TableColumn<typeWeapon, String>("Description");

		TableColumn<typeWeapon, String> TypeWeaponRangeCol //
		= new TableColumn<typeWeapon, String>("Range");

		TableColumn<typeWeapon, String> TypeWeaponWeightCol //
		= new TableColumn<typeWeapon, String>("Weight");

		TableColumn<typeWeapon, String> typeWeaponPicCol //
		= new TableColumn<typeWeapon, String>("Picture");

		TableColumn<typeWeapon, String> registrationDateCol //
		= new TableColumn<typeWeapon, String>("registrationDate");

		TableColumn<typeWeapon, String> catWeaponCol //
		= new TableColumn<typeWeapon, String>("catWeapon");


		// ==== typeVehicleId ===
		typeWeaponIdCol.setCellValueFactory(new PropertyValueFactory<>("typeWeaponId"));
		typeWeaponIdCol.setMinWidth(80);

		// ==== typeVehicleName ===
		typeWeaponNameCol.setCellValueFactory(new PropertyValueFactory<>("typeWeaponName"));
		typeWeaponNameCol.setMinWidth(100);

		// ==== typeVehicleDescription ===
		TypeWeaponDescriptionCol.setCellValueFactory(new PropertyValueFactory<>("typeWeaponDescription"));
		TypeWeaponDescriptionCol.setMinWidth(255);

		// ==== typeVehicleMaxSpeed ===
		TypeWeaponRangeCol.setCellValueFactory(new PropertyValueFactory<>("typeWeaponRange"));
		TypeWeaponRangeCol.setMinWidth(30);

		// ==== typeVehiclePlaces ===
		TypeWeaponWeightCol.setCellValueFactory(new PropertyValueFactory<>("typeWeaponWeight"));
		TypeWeaponWeightCol.setMinWidth(30);

		// ==== typeVehiclePic ===
		typeWeaponPicCol.setCellValueFactory(new PropertyValueFactory<>("typeWeaponPic"));
		typeWeaponPicCol.setMinWidth(50);

		// ==== registrationDate  ===
		registrationDateCol.setCellValueFactory(new PropertyValueFactory<>("registrationDate"));
		registrationDateCol.setMinWidth(100);


		// ==== registrationDate  ===
		catWeaponCol.setCellValueFactory(new PropertyValueFactory<>("catWeaponName"));
		catWeaponCol.setMinWidth(100);

		ObservableList<typeWeapon> list = FXCollections.observableArrayList(typeWeaponDB.getTypeWeaponList());
		table.setItems(list);

		table.getColumns().addAll(typeWeaponIdCol, typeWeaponNameCol, TypeWeaponDescriptionCol, TypeWeaponRangeCol,TypeWeaponWeightCol, typeWeaponPicCol ,registrationDateCol,catWeaponCol);
	


		Button accueilButton = new Button("Accueil") ;
		grid.add(accueilButton, 0, 0);



		accueilButton.setOnAction(new EventHandler<ActionEvent>() {

			public void handle (ActionEvent event) {
				try {
					ctrl.lancerAccueil(stage);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
				);

		
		stage.setTitle("show type weapon");

		Scene scene = new Scene(grid,1000, 700);
		stage.setScene(scene);
		stage.show();

	}

	public static void main(String[] args) {
		launch(args);
	}

}




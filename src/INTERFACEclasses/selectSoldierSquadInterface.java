package INTERFACEclasses;


import java.sql.SQLException;

import Controller.controller;
import DBclasses.squadDB;
import MODELclasses.squad;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.Callback;

public class selectSoldierSquadInterface extends Application {
	private TableView<squad> table ;
	private controller ctrl;
	private Stage stage ;
	private String typeSoldierName = "" ;
	
	public selectSoldierSquadInterface(controller ctrl){
		this.ctrl = ctrl;
	}
	
	public selectSoldierSquadInterface(controller ctrl, String typeSoldierName)
	{
		this(ctrl) ;
		this.typeSoldierName = typeSoldierName ;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void start(Stage primaryStage) throws ClassNotFoundException, SQLException {
		stage = primaryStage ;
		GridPane grid = new GridPane() ;
		table = new TableView<squad>();
		grid.add(table, 0, 1);

		TableColumn<squad, Integer> squadId//
		= new TableColumn<squad, Integer>("squadId");


		TableColumn<squad, String> squadName //
		= new TableColumn<squad, String>("squadName");


		TableColumn<squad, String> creationDate//
		= new TableColumn<squad, String>("creationDate");

		addButtonToTable();

		// ==== typeSoldierId (TEXT FIELD) ===
		squadId.setCellValueFactory(new PropertyValueFactory<>("squadId"));
		squadId.setMinWidth(200);

		// ==== typeSoldierName (TEXT FIELD) ===
		squadName.setCellValueFactory(new PropertyValueFactory<>("squadName"));
		squadName.setMinWidth(200);

		// ==== creationDate (TEXT FIELD) ===
		creationDate.setCellValueFactory(new PropertyValueFactory<>("creationDate"));
		creationDate.setMinWidth(200);

		

		ObservableList<squad> list =  FXCollections.observableArrayList(squadDB.getSquadList());
		table.setItems(list);
		table.getColumns().addAll(squadId, squadName, creationDate);
		
		stage.setTitle("select soldier squad  ");
		Scene scene = new Scene(grid, 1000, 700);
		stage.setScene(scene);
		stage.show();
	}

	private void addButtonToTable() {
		TableColumn<squad, Void> colBtn = new TableColumn<squad, Void>("select soldier squad");
		colBtn.setMinWidth(70);
		Callback<TableColumn<squad, Void>, TableCell<squad, Void>> cellFactory = new Callback<TableColumn<squad, Void>, TableCell<squad, Void>>() {
			@Override
			public TableCell<squad, Void> call(final TableColumn<squad, Void> param) {
				final TableCell<squad, Void> cell = new TableCell<squad, Void>() {
					private final Button btn = new Button("Select");
					{
						btn.setOnAction(event -> {
							squad data = getTableView().getItems().get(getIndex());
							ctrl.selectTypeSoldierOrSquad(stage, "Soldier", data.getSquadName(), typeSoldierName);
						});
					}
					@Override
					public void updateItem(Void item, boolean empty) {
						super.updateItem(item, empty);
						if (empty) {
							setGraphic(null);
						} else {
							setGraphic(btn);
						}
					}
				};
				return cell;
			}
		};

		colBtn.setCellFactory(cellFactory);

		table.getColumns().add(colBtn);
	}

	public static void main(String[] args) {
		launch(args);
	}

}

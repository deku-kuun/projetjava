package INTERFACEclasses;


import java.sql.SQLException;

import Controller.controller;
import DBclasses.catWeaponDB;
import MODELclasses.catWeapon;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class showCatWeaponInterface extends Application {
	private TableView<catWeapon> table ;

	private controller ctrl;
	/**
	 * constructor of the controler
	 * @param ctrl
	 */
	public showCatWeaponInterface(controller ctrl){
		this.ctrl = ctrl;
	}
	@SuppressWarnings("unchecked")
	@Override
	public void start(Stage stage) throws ClassNotFoundException, SQLException {
		GridPane grid = new GridPane() ;
		table = new TableView<catWeapon>();
		//table.set
		grid.add(table, 0, 1);

		TableColumn<catWeapon, Integer> catWeaponId//
		= new TableColumn<catWeapon, Integer>("catWeaponId");


		TableColumn<catWeapon, String> catWeaponName //
		= new TableColumn<catWeapon, String>("catWeaponName");


		TableColumn<catWeapon, String> registrationDate//
		= new TableColumn<catWeapon, String>("registrationDate");



		// ==== catVehicleId (TEXT FIELD) ===
		catWeaponId.setCellValueFactory(new PropertyValueFactory<>("catWeaponId"));
		catWeaponId.setMinWidth(200);

		// ==== catVehicleName (TEXT FIELD) ===
		catWeaponName.setCellValueFactory(new PropertyValueFactory<>("catWeaponName"));
		catWeaponName.setMinWidth(200);

		// ==== registrationDate (TEXT FIELD) ===
		registrationDate.setCellValueFactory(new PropertyValueFactory<>("registrationDate"));
		registrationDate.setMinWidth(200);

		ObservableList<catWeapon> list =  FXCollections.observableArrayList(catWeaponDB.getCatWeaponList());
		table.setItems(list);

		table.getColumns().addAll(catWeaponId, catWeaponName, registrationDate);


		Button accueilButton = new Button("Accueil") ;
		grid.add(accueilButton, 0, 0);



		accueilButton.setOnAction(new EventHandler<ActionEvent>() {

			public void handle (ActionEvent event) {
				try {
					ctrl.lancerAccueil(stage);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
				);

		stage.setTitle("show cat weapon");

		Scene scene = new Scene(grid, 700, 500);
		stage.setScene(scene);
		stage.show();


	}

	

	public static void main(String[] args) {
		launch(args);
	}

}

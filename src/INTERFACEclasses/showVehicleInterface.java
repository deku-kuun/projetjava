package INTERFACEclasses;


import java.sql.SQLException;
import Controller.controller;
import DBclasses.vehicleDB;
import MODELclasses.vehicle;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class showVehicleInterface extends Application
{
	private TableView<vehicle> table ;
	private controller ctrl;
	private Stage stage;
	
	
	/**
	 * constructor of the controller
	 * @param ctrl
	 */
	public showVehicleInterface(controller ctrl){
		this.ctrl = ctrl;
		
	}
	
	

	@SuppressWarnings("unchecked")
	@Override
	public void start(Stage primaryStage) throws ClassNotFoundException, SQLException 
	{
		this.stage = primaryStage ;
		GridPane grid = new GridPane() ;
		table = new TableView<vehicle>();
		grid.add(table, 0, 1);

		table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

		TableColumn<vehicle, Integer> vehicleNameCol
		= new TableColumn<vehicle, Integer>("serial number");

		TableColumn<vehicle, String> typeVehicleCol 
		= new TableColumn<vehicle, String>("vehicle model");

		TableColumn<vehicle, String> vehicleRegistrationDateCol
		= new TableColumn<vehicle, String>("registration date");


		// ==== weaponSN ===
		vehicleNameCol.setCellValueFactory(new PropertyValueFactory<>("vehicleLP"));
		vehicleNameCol.setMinWidth(100);

		// ==== registrationDate ===
		vehicleRegistrationDateCol.setCellValueFactory(new PropertyValueFactory<>("registrationDate"));
		vehicleRegistrationDateCol.setMinWidth(100);

		// ==== typeWeaponName ===
		typeVehicleCol.setCellValueFactory(new PropertyValueFactory<>("typeVehicleName"));
		typeVehicleCol.setMinWidth(120);



		ObservableList<vehicle> list = FXCollections.observableArrayList(vehicleDB.getVehiclesList());
		table.setItems(list);
		table.getColumns().addAll(vehicleNameCol, vehicleRegistrationDateCol, typeVehicleCol) ;


		Button launchAccueilButton =new Button("Accueil");
		grid.add(launchAccueilButton,0,0);
		launchAccueilButton.setOnAction(launchAccueilEvent->
		{
			ctrl.lancerAccueil(primaryStage);
		}
				);

		stage.setTitle("select vehicle");
		Scene scene = new Scene(grid, 1000, 700);
		stage.setScene(scene);
		stage.show();
	}
	
}
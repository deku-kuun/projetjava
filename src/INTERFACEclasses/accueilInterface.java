package INTERFACEclasses;

import Controller.controller;
import MODELclasses.info;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.* ;
import javafx.scene.layout.* ;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;


public class accueilInterface extends Application {

	private controller ctrl;
	private VBox layout ;
	private TableView <info> table ;

	public accueilInterface(controller ctrl){
		this.ctrl = ctrl;
	}

	@Override
	public void start(Stage primaryStage) throws Exception {

		Stage window = primaryStage;
		window.setTitle("Accueil");

		// --------------------- Missions ------------------------------------
		Menu missionMenu = new Menu("Missions");

		MenuItem showMission = new MenuItem("missions");
		missionMenu.getItems().add(showMission);

		showMission.setOnAction(showMissionevent->
		{
			ctrl.lancerShowMission(primaryStage);
		}
				);

		MenuItem addMission = new MenuItem("new mission..");
		missionMenu.getItems().add(addMission);
		addMission.setOnAction(addMissionevent->
		{
			ctrl.lancerAddMission(primaryStage);
		}
				);

		// --------------------- Weapons ------------------------------------
		Menu weaponsMenu = new Menu("Weapons");

		MenuItem ShowWeapon = new MenuItem("weapons");
		weaponsMenu.getItems().add(ShowWeapon);
		ShowWeapon.setOnAction(showTypeWeaponEvent->
		{
			ctrl.lancerShowWeapon(primaryStage);
		}
				);

		MenuItem ShowTypeWeapon = new MenuItem("weapon models");
		weaponsMenu.getItems().add(ShowTypeWeapon);
		ShowTypeWeapon.setOnAction(showTypeWeaponEvent->
		{
			ctrl.lancerShowTypeWeapon(primaryStage);
		}
				);

		MenuItem ShowCatWeapon = new MenuItem("weapon categories");
		weaponsMenu.getItems().add(ShowCatWeapon);
		ShowCatWeapon.setOnAction(showCatWeaponEvent->
		{
			ctrl.lancerShowCatWeapon(primaryStage);
		}
				);

		//Item menu weapon
		MenuItem addWeapon = new MenuItem("new weapon..");
		weaponsMenu.getItems().add(addWeapon);
		addWeapon.setOnAction(addWeaponEvent-> 
		{
			ctrl.lancerAddWeapon(primaryStage);
		}
				);

		//Item menu weapon's type
		MenuItem addTypeWeapon = new MenuItem("new weapon model..");
		weaponsMenu.getItems().add(addTypeWeapon);
		addTypeWeapon.setOnAction(addCatWeaponEvent->
		{
			ctrl.lancerTypeWeapon(primaryStage);
		});

		//Item menu weapon's category
		MenuItem addCatWeapon = new MenuItem("new weapon category..");
		weaponsMenu.getItems().add(addCatWeapon);
		addCatWeapon.setOnAction(addCatWeaponEvent->
		{
			ctrl.lancerAddCatWeapon(primaryStage);
		});

		// --------------------- Vehicles ------------------------------------
		Menu vehiclesMenu = new Menu("Vehicles");

		MenuItem showVehicle = new MenuItem("vehicles");
		vehiclesMenu.getItems().add(showVehicle);
		showVehicle.setOnAction(showTypeVehicleEvent->
		{
			ctrl.lancerShowVehicle(primaryStage);
		});

		MenuItem showTypeVehicle = new MenuItem("vehicle models");
		vehiclesMenu.getItems().add(showTypeVehicle);
		showTypeVehicle.setOnAction(showTypeVehicleEvent->
		{
			ctrl.lancerShowTypeVehicle(primaryStage);
		});

		MenuItem showCatVehicle = new MenuItem("vehicle categories");
		vehiclesMenu.getItems().add(showCatVehicle);
		showCatVehicle.setOnAction(showCatVehicleEvent->
		{
			ctrl.lancerShowCatVehicle(primaryStage);
		});

		MenuItem addVehicle = new MenuItem("new vehicle..");
		vehiclesMenu.getItems().add(addVehicle);
		addVehicle.setOnAction(addVehicleEvent->
		{
			ctrl.lancerAddVehicle(primaryStage);
		});

		MenuItem addCatVehicle = new MenuItem("new vehicle category..");
		vehiclesMenu.getItems().add(addCatVehicle);
		addCatVehicle.setOnAction(addCatVehicleEvent->
		{
			ctrl.lancerAddCatVehicle(primaryStage);
		});

		//Item menu vehicle's model
		MenuItem addTypeVehicle = new MenuItem("new vehicle model");
		vehiclesMenu.getItems().add(addTypeVehicle);
		addTypeVehicle.setOnAction(addCatWeaponEvent->
		{
			ctrl.lancerTypeVehicle(primaryStage);
		});

		// --------------------- Geographic ------------------------------------
		Menu geographicMenu = new Menu("Geographic");

		MenuItem showCity = new MenuItem("cities");
		geographicMenu.getItems().add(showCity);
		showCity.setOnAction(showCityEvent->
		{
			ctrl.lancerShowCity(primaryStage);
		});

		MenuItem addCity = new MenuItem("new city..");
		geographicMenu.getItems().add(addCity);
		addCity.setOnAction(addCityEvent->
		{
			ctrl.lancerAddCity(primaryStage) ;
		}
				);

		MenuItem showDistrict = new MenuItem("districts");
		geographicMenu.getItems().add(showDistrict);
		showDistrict.setOnAction(showDistrictEvent->
		{
			ctrl.lancerShowDistrict(primaryStage);
		}
				);

		MenuItem addDistrict = new MenuItem("new district..");
		geographicMenu.getItems().add(addDistrict);
		addDistrict.setOnAction(addDistrictEvent->
		{
			ctrl.lancerAddDistrict(primaryStage);
		}
				);

		MenuItem showCountry = new MenuItem("countries");
		geographicMenu.getItems().add(showCountry);
		showCountry.setOnAction(addCityEvent->
		{
			ctrl.lancerShowCountry(primaryStage);
		}
				);

		MenuItem addCountry = new MenuItem("new country..");
		geographicMenu.getItems().add(addCountry);
		addCountry.setOnAction(addCountryEvent->
		{
			ctrl.lancerAddCountry(primaryStage);
		}
				);

		// --------------------- Members ------------------------------------
		Menu memberMenu = new Menu("Members");

		MenuItem showMember = new MenuItem("members");
		memberMenu.getItems().add(showMember);
		showMember.setOnAction(showMemberEvent->
		{
			ctrl.lancerShowMember(primaryStage);
		}
				);

		MenuItem addMember = new MenuItem("new member..");
		memberMenu.getItems().add(addMember);
		addMember.setOnAction(addMemberEvent->
		{
			ctrl.lancerAddMember(primaryStage) ;
		}
				);

		// main menu bar
		MenuBar menuBar = new MenuBar();
		menuBar.getMenus().addAll(missionMenu, weaponsMenu, vehiclesMenu, geographicMenu,memberMenu);

		layout = new VBox();
		layout.getChildren().add(menuBar) ;
		addInfos() ;
		Scene scene = new Scene(layout, 1000, 700);
		window.setScene(scene);
		window.show();
	}

	@SuppressWarnings("unchecked")
	private void addInfos()
	{		
		Text welcomeText = new Text("Welcome : " + ctrl.getNames());
		welcomeText.setFont(Font.font("Tahoma", FontWeight.LIGHT, 25));
		layout.getChildren().add(welcomeText) ;
		VBox.setMargin(welcomeText, new Insets(20,0,5,150));

		Text messageText = new Text("Your missions");
		messageText.setFont(Font.font("Tahoma", FontWeight.LIGHT, 15));
		layout.getChildren().add(messageText) ;
		VBox.setMargin(messageText, new Insets(20,0,5,465));

		if(ctrl.getInfos().size()!=0)
		{
			table = new TableViewWithVisibleRowCount <info> (ctrl.getInfos()) ;
		}
		else
		{
			table = new TableView <info>() ;
		}
		table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
		table.setPlaceholder(new Label("You don't have any missions yet"));

		TableColumn <info, Integer> missionId
		= new TableColumn <info, Integer>("Id");

		TableColumn <info, String> missionName
		= new TableColumn <info, String>("name");

		TableColumn <info, String> missionDescription
		= new TableColumn <info, String>("description");

		TableColumn <info, String> missionPlaces
		= new TableColumn <info, String>("number of members");

		TableColumn <info, String> missionStatus
		= new TableColumn <info, String>("status");

		TableColumn <info, String> creationDate
		= new TableColumn <info, String>("created on the");

		TableColumn <info, String> cityName
		= new TableColumn <info, String>("city");

		missionId.setCellValueFactory(new PropertyValueFactory<>("missionId"));
		missionId.setMinWidth(40);

		missionName.setCellValueFactory(new PropertyValueFactory<>("missionName"));
		missionName.setMinWidth(50);

		missionDescription.setCellValueFactory(new PropertyValueFactory<>("missionDescription"));
		missionDescription.setMinWidth(120);

		missionPlaces.setCellValueFactory(new PropertyValueFactory<>("missionPlaces"));
		missionPlaces.setMinWidth(130);

		missionStatus.setCellValueFactory(new PropertyValueFactory<>("missionStatus"));
		missionStatus.setMinWidth(20);

		creationDate.setCellValueFactory(new PropertyValueFactory<>("creationDate"));
		creationDate.setMinWidth(110);

		cityName.setCellValueFactory(new PropertyValueFactory<>("cityName"));
		cityName.setMinWidth(60);

		table.getColumns().addAll(missionId, missionName, missionDescription, missionPlaces, missionStatus,creationDate,cityName);
		layout.getChildren().add(table) ;
		VBox.setMargin(table, new Insets(15));
	}	
}
package INTERFACEclasses;


import java.sql.SQLException;

import Controller.controller;
import DBclasses.typeWeaponDB;
import MODELclasses.typeWeapon;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.Callback;

public class selectTypeWeaponForWeapon extends Application {
	
	private TableView<typeWeapon> table ;
	private controller ctrl;
	private Stage stage ;
	/**
	 * constructor of the controller
	 * @param ctrl
	 */
	public selectTypeWeaponForWeapon(controller ctrl){
		this.ctrl = ctrl;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void start(Stage primaryStage) throws ClassNotFoundException, SQLException {
		stage = primaryStage ;
		GridPane grid = new GridPane() ;
		table = new TableView<typeWeapon>();
		grid.add(table, 0, 1);

		table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

		TableColumn<typeWeapon, Integer> typeWeaponIdCol //
		= new TableColumn<typeWeapon, Integer>("Id");

		TableColumn<typeWeapon, Integer> typeWeaponNameCol//
		= new TableColumn<typeWeapon, Integer>("Name");

		TableColumn<typeWeapon, String> TypeWeaponDescriptionCol//
		= new TableColumn<typeWeapon, String>("Description");

		TableColumn<typeWeapon, String> TypeWeaponRangeCol //
		= new TableColumn<typeWeapon, String>("Range");

		TableColumn<typeWeapon, String> TypeWeaponWeightCol //
		= new TableColumn<typeWeapon, String>("Weight");

		TableColumn<typeWeapon, String> typeWeaponPicCol //
		= new TableColumn<typeWeapon, String>("Picture");

		TableColumn<typeWeapon, String> registrationDateCol //
		= new TableColumn<typeWeapon, String>("registrationDate");

		TableColumn<typeWeapon, String> catWeaponCol //
		= new TableColumn<typeWeapon, String>("catWeapon");

		addButtonToTable();
		
		// ==== typeVehicleId ===
		typeWeaponIdCol.setCellValueFactory(new PropertyValueFactory<>("typeWeaponId"));
		typeWeaponIdCol.setMinWidth(80);

		// ==== typeVehicleName ===
		typeWeaponNameCol.setCellValueFactory(new PropertyValueFactory<>("typeWeaponName"));
		typeWeaponNameCol.setMinWidth(100);

		// ==== typeVehicleDescription ===
		TypeWeaponDescriptionCol.setCellValueFactory(new PropertyValueFactory<>("typeWeaponDescription"));
		TypeWeaponDescriptionCol.setMinWidth(255);

		// ==== typeVehicleMaxSpeed ===
		TypeWeaponRangeCol.setCellValueFactory(new PropertyValueFactory<>("typeWeaponRange"));
		TypeWeaponRangeCol.setMinWidth(30);

		// ==== typeVehiclePlaces ===
		TypeWeaponWeightCol.setCellValueFactory(new PropertyValueFactory<>("typeWeaponWeight"));
		TypeWeaponWeightCol.setMinWidth(30);

		// ==== typeVehiclePic ===
		typeWeaponPicCol.setCellValueFactory(new PropertyValueFactory<>("typeWeaponPic"));
		typeWeaponPicCol.setMinWidth(50);

		// ==== registrationDate  ===
		registrationDateCol.setCellValueFactory(new PropertyValueFactory<>("registrationDate"));
		registrationDateCol.setMinWidth(100);
		

		// ==== registrationDate  ===
		catWeaponCol.setCellValueFactory(new PropertyValueFactory<>("catWeaponName"));
		catWeaponCol.setMinWidth(100);

		ObservableList<typeWeapon> list = FXCollections.observableArrayList(typeWeaponDB.getTypeWeaponList());
		table.setItems(list);

		table.getColumns().addAll(typeWeaponIdCol, typeWeaponNameCol, TypeWeaponDescriptionCol, TypeWeaponRangeCol,TypeWeaponWeightCol, typeWeaponPicCol ,registrationDateCol,catWeaponCol);
		
		stage.setTitle("select type weapon");
		Scene scene = new Scene(grid, 1000, 700);
		stage.setScene(scene);
		stage.show();

	}
	private void addButtonToTable() {
		//create column who take information of model class called typeWeapon
		TableColumn<typeWeapon, Void> colBtn = new TableColumn<typeWeapon, Void>("select Type Weapon");

		Callback<TableColumn<typeWeapon, Void>, TableCell<typeWeapon, Void>> cellFactory = new Callback<TableColumn<typeWeapon, Void>, TableCell<typeWeapon, Void>>() {
			@Override
			public TableCell<typeWeapon, Void> call(final TableColumn<typeWeapon, Void> param) {
				final TableCell<typeWeapon, Void> cell = new TableCell<typeWeapon, Void>() {

					private final Button btn = new Button("Select");

					{
						btn.setOnAction((ActionEvent event) -> {
							typeWeapon data = getTableView().getItems().get(getIndex());
							ctrl.selectTypeWeaponForWeapon(stage, data.getTypeWeaponName()) ;
						});
					}
					@Override
					public void updateItem(Void item, boolean empty) {
						super.updateItem(item, empty);
						if (empty) {
							setGraphic(null);
						} 
						else {
							setGraphic(btn);
						}
					}
				};
				return cell;
			}
		};

		colBtn.setCellFactory(cellFactory);

		table.getColumns().add(colBtn);
	}

	public static void main(String[] args) {
		launch(args);
	}
		
	}

	


	package INTERFACEclasses;


import java.sql.SQLException;

import Controller.controller;
import DBclasses.catVehicleDB;
import MODELclasses.catVehicle;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class showCatVehicleInterface extends Application {
	private controller ctrl;

	public showCatVehicleInterface(controller ctrl)
	{
		this.ctrl = ctrl;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void start(Stage stage) throws ClassNotFoundException, SQLException {
		
		TableView<catVehicle> table  = new TableView<catVehicle>();
		GridPane grid = new GridPane();		
		grid.add(table, 0, 1);

		TableColumn<catVehicle, Integer> catVehicleId//
		= new TableColumn<catVehicle, Integer>("catVehicleId");


		TableColumn<catVehicle, String> catVehicleName //
		= new TableColumn<catVehicle, String>("catVehicleName");


		TableColumn<catVehicle, String> registrationDate//
		= new TableColumn<catVehicle, String>("registrationDate");



		// ==== catVehicleId (TEXT FIELD) ===
		catVehicleId.setCellValueFactory(new PropertyValueFactory<>("catVehicleId"));
		catVehicleId.setMinWidth(200);

		// ==== catVehicleName (TEXT FIELD) ===
		catVehicleName.setCellValueFactory(new PropertyValueFactory<>("catVehicleName"));
		catVehicleName.setMinWidth(200);

		// ==== registrationDate (TEXT FIELD) ===
		registrationDate.setCellValueFactory(new PropertyValueFactory<>("registrationDate"));
		registrationDate.setMinWidth(200);

		
		ObservableList <catVehicle> list =  FXCollections.observableArrayList(catVehicleDB.getCatVehicleList());
		table.setItems(list);
		table.getColumns().addAll(catVehicleId, catVehicleName, registrationDate);

		


		Button accueilButton = new Button("Accueil") ;
		grid.add(accueilButton, 0, 0);

		accueilButton.setOnAction(eventAcc->
		{
			try 
			{
				ctrl.lancerAccueil(stage);
			} 
			catch (Exception e) 
			{		
				new AlertInterface(e) ;
			}
		} );

		stage.setTitle("show cat vehicle");
		Scene scene = new Scene(grid, 1000, 700);
		stage.setScene(scene);
		stage.show();
		
	}
	public static void main(String[] args) {
		launch(args);
	}

}

package INTERFACEclasses;


import java.sql.SQLException;

import Controller.controller;
import DBclasses.typeSoldierDB;
import MODELclasses.typeSoldier;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.Callback;

public class selectTypeSoldierForSoldierInterface extends Application {
	//Table view of typeSoldier
	private TableView<typeSoldier> table ;
	private controller ctrl ;
	private Stage stage ;
	private String squadName ;

	/**
	 * constructor of the controller
	 * @param ctrl
	 */
	public selectTypeSoldierForSoldierInterface(controller ctrl){
		this.ctrl = ctrl;
	}

	public selectTypeSoldierForSoldierInterface(controller ctrl, String squadName)
	{
		this(ctrl) ;
		this.squadName = squadName ;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void start(Stage primaryStage) throws ClassNotFoundException, SQLException 
	{
		stage = primaryStage ;
		GridPane grid = new GridPane() ;
		table = new TableView<typeSoldier>();
		//table.set
		grid.add(table, 0, 1);

		TableColumn<typeSoldier, Integer> typeSoldierId//
		= new TableColumn<typeSoldier, Integer>("typeSoldierId");

		TableColumn<typeSoldier, String> typeSoldierName //
		= new TableColumn<typeSoldier, String>("typeSoldier Name");

		TableColumn<typeSoldier, String> creationDate//
		= new TableColumn<typeSoldier, String>("creationDate");

		addButtonToTable();


		// ==== typeSoldierId (TEXT FIELD) ===
		typeSoldierName.setCellValueFactory(new PropertyValueFactory<>("typeSoldierId"));
		typeSoldierName.setMinWidth(200);

		// ==== typeSoldierName (TEXT FIELD) ===
		typeSoldierId.setCellValueFactory(new PropertyValueFactory<>("typeSoldierName"));
		typeSoldierId.setMinWidth(200);

		// ==== creationDate (TEXT FIELD) ===
		creationDate.setCellValueFactory(new PropertyValueFactory<>("creationDate"));
		creationDate.setMinWidth(200);

		ObservableList<typeSoldier> list =  FXCollections.observableArrayList(typeSoldierDB.getTypeSoldierList());
		table.setItems(list);
		table.getColumns().addAll(typeSoldierName, typeSoldierId, creationDate);

		stage.setTitle("select type soldier");
		Scene scene = new Scene(grid, 1000, 700);
		stage.setScene(scene);
		stage.show();
	}

	private void addButtonToTable() {
		//create column who take information of model class called typeSoldier
		TableColumn<typeSoldier, Void> colBtn = new TableColumn<typeSoldier, Void>("select type soldier");
		colBtn.setMinWidth(70);
		Callback<TableColumn<typeSoldier, Void>, TableCell<typeSoldier, Void>> cellFactory = new Callback<TableColumn<typeSoldier, Void>, TableCell<typeSoldier, Void>>() {
			@Override
			public TableCell<typeSoldier, Void> call(final TableColumn<typeSoldier, Void> param) {
				final TableCell<typeSoldier, Void> cell = new TableCell<typeSoldier, Void>() {

					private final Button btn = new Button("Select");

					{
						btn.setOnAction((ActionEvent event) -> {
							typeSoldier data = getTableView().getItems().get(getIndex());
							ctrl.selectTypeSoldierOrSquad(stage, "Soldier", squadName, data.getTypeSoldierName()) ;
						});
					}
					@Override
					public void updateItem(Void item, boolean empty) {
						super.updateItem(item, empty);
						if (empty) {
							setGraphic(null);
						} else {
							setGraphic(btn);
						}
					}
				};
				return cell;
			}
		};

		colBtn.setCellFactory(cellFactory);

		table.getColumns().add(colBtn);
	}




	public static void main(String[] args) {
		launch(args);
	}

}

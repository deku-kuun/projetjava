package INTERFACEclasses;


import java.sql.SQLException;

import Controller.controller;
import DBclasses.districtDB;
import MODELclasses.district;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.Callback;

public class selectDistrictForCityInterface extends Application {
	private TableView<district> table ;
	private controller ctrl ;
	private Stage stage ;
	
	public selectDistrictForCityInterface(controller ctrl)
	{
		this.ctrl = ctrl;
	}


	@SuppressWarnings("unchecked")
	@Override
	public void start(Stage primaryStage) throws ClassNotFoundException, SQLException {
		stage = primaryStage ;
		GridPane grid = new GridPane() ;
		table = new TableView<district>();

		grid.add(table, 0, 1);

		table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

		TableColumn<district, Integer> districtId
		= new TableColumn<district, Integer>("Id");


		TableColumn<district, String> districtCode 
		= new TableColumn<district, String>("International code");


		TableColumn<district, String> districtName
		= new TableColumn<district, String>("name");

		TableColumn<district, String> registrationDate 
		= new TableColumn<district, String>("registration date");


		TableColumn<district, String> countryName
		= new TableColumn<district, String>("country");

		addButtonToTable();

		// ==== districtId ===
		districtId.setCellValueFactory(new PropertyValueFactory<>("districtId"));
		districtId.setMinWidth(50);

		// ==== districtCode ===
		districtCode.setCellValueFactory(new PropertyValueFactory<>("districtCode"));
		districtCode.setMinWidth(120);

		// ==== districtName ===
		districtName.setCellValueFactory(new PropertyValueFactory<>("districtName"));
		districtName.setMinWidth(50);

		// ==== registrationDate ===
		registrationDate.setCellValueFactory(new PropertyValueFactory<>("registrationDate"));
		registrationDate.setMinWidth(130);

		// ==== countryName  ===
		countryName.setCellValueFactory(new PropertyValueFactory<>("countryName"));
		countryName.setMinWidth(50);

		ObservableList<district> list =  FXCollections.observableArrayList(districtDB.getDistrictList());
		table.setItems(list);

		table.getColumns().addAll(districtId, districtCode,districtName, registrationDate,countryName);

		stage.setTitle("Select district for city");
		Scene scene = new Scene(grid, 1000, 700);
		stage.setScene(scene);
		stage.show();
	}

	private void addButtonToTable() {
		TableColumn<district, Void> colBtn = new TableColumn<district, Void>("Select this district");
		colBtn.setMinWidth(100);
		Callback<TableColumn<district, Void>, TableCell<district, Void>> cellFactory = new Callback<TableColumn<district, Void>, TableCell<district, Void>>() {
			@Override
			public TableCell<district, Void> call(final TableColumn<district, Void> param) {
				final TableCell<district, Void> cell = new TableCell<district, Void>() {
					private final Button btn = new Button("Select"); 
					{
						btn.setMinWidth(70);
						btn.setOnAction(event -> {
							district data = getTableView().getItems().get(getIndex());
							ctrl.selectDistrictForCity(stage,data.getDistrictName());
						});
					}					
					@Override
					public void updateItem(Void item, boolean empty) {
						super.updateItem(item, empty);
						if (empty) 
						{
							setGraphic(null);
						} 
						else {
							setGraphic(btn);
						}
					}
				};
				cell.setAlignment(Pos.CENTER);
				return cell;
			}
		};
		colBtn.setCellFactory(cellFactory);
		table.getColumns().add(colBtn);
	}

	public static void main(String[] args) 
	{
		launch(args);
	}

}

package INTERFACEclasses;


import java.sql.SQLException;
import Controller.controller;
import DBclasses.catWeaponDB;
import MODELclasses.catWeapon;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.Callback;

public class selectCatWeaponForTypeWeapon extends Application {
	private TableView<catWeapon> table ;
	private controller ctrl;
	private Stage stage ;

	/**
	 * constructor of the controler
	 * @param ctrl
	 */
	public selectCatWeaponForTypeWeapon(controller ctrl){
		this.ctrl = ctrl;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void start(Stage primaryStage) throws ClassNotFoundException, SQLException {
		stage = primaryStage ;
		GridPane grid = new GridPane() ;
		table = new TableView<catWeapon>();
		grid.add(table, 0, 1);

		table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

		TableColumn<catWeapon, Integer> catWeaponId//
		= new TableColumn<catWeapon, Integer>("catWeaponId");

		TableColumn<catWeapon, String> catWeaponName //
		= new TableColumn<catWeapon, String>("catWeaponName");

		TableColumn<catWeapon, String> registrationDate//
		= new TableColumn<catWeapon, String>("registrationDate");

		addButtonToTable();

		// ==== catVehicleId (TEXT FIELD) ===
		catWeaponId.setCellValueFactory(new PropertyValueFactory<>("catWeaponId"));
		catWeaponId.setMinWidth(200);

		// ==== catVehicleName (TEXT FIELD) ===
		catWeaponName.setCellValueFactory(new PropertyValueFactory<>("catWeaponName"));
		catWeaponName.setMinWidth(200);

		// ==== registrationDate (TEXT FIELD) ===
		registrationDate.setCellValueFactory(new PropertyValueFactory<>("registrationDate"));
		registrationDate.setMinWidth(200);

		ObservableList<catWeapon> list =  FXCollections.observableArrayList(catWeaponDB.getCatWeaponList());
		table.setItems(list);
		table.getColumns().addAll(catWeaponId, catWeaponName, registrationDate);

		Button accueilButton = new Button("Accueil") ;
		grid.add(accueilButton, 0, 0);

		accueilButton.setOnAction(new EventHandler<ActionEvent>() {

			public void handle (ActionEvent event) {
				try {
					ctrl.lancerAccueil(stage);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
				);

		stage.setTitle("show cat weapon");

		Scene scene = new Scene(grid, 700, 500);
		stage.setScene(scene);
		stage.show();
	}

	private void addButtonToTable() {
		TableColumn<catWeapon, Void> colBtn = new TableColumn<catWeapon, Void>("select Cat Weapon");

		Callback<TableColumn<catWeapon, Void>, TableCell<catWeapon, Void>> cellFactory = new Callback<TableColumn<catWeapon, Void>, TableCell<catWeapon, Void>>() {
			@Override
			public TableCell<catWeapon, Void> call(final TableColumn<catWeapon, Void> param) {
				final TableCell<catWeapon, Void> cell = new TableCell<catWeapon, Void>() {

					private final Button btn = new Button("Select");

					{
						btn.setOnAction((ActionEvent event) -> {
							catWeapon data = getTableView().getItems().get(getIndex());
							ctrl.selectCatWeaponForTypeWeapon(stage, data.getCatWeaponName());
						});
					}
					@Override
					public void updateItem(Void item, boolean empty) {
						super.updateItem(item, empty);
						if (empty) {
							setGraphic(null);
						} else {
							setGraphic(btn);
						}
					}
				};
				return cell;
			}
		};

		colBtn.setCellFactory(cellFactory);
		table.getColumns().add(colBtn);
	}

	public static void main(String[] args) {
		launch(args);
	}
}

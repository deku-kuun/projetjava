package INTERFACEclasses;


import java.sql.SQLException;

import Controller.controller;
import DBclasses.countryDB;
import MODELclasses.country;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class showCountryInterface extends Application {


	//table view will contains all the country 
	private TableView<country> table ;
	
	private controller ctrl;
	/**
	 * constructor of the controller
	 * @param ctrl
	 */
	public showCountryInterface(controller ctrl){
		this.ctrl = ctrl;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void start(Stage stage) throws ClassNotFoundException, SQLException {
		GridPane grid = new GridPane() ;
		table = new TableView<country>();
		grid.add(table, 0, 1);

		table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

		TableColumn<country, Integer> countryId //
		= new TableColumn<country, Integer>("Id");

		TableColumn<country, Integer> countryCode//
		= new TableColumn<country, Integer>("Code");

		TableColumn<country, String> countryName//
		= new TableColumn<country, String>("Name");

		TableColumn<country, String> registrationDate //
		= new TableColumn<country, String>("Registration Date");

		

		// ==== typeVehicleId ===
		countryId.setCellValueFactory(new PropertyValueFactory<>("countryId"));
		countryId.setMinWidth(80);

		// ==== typeVehicleName ===
		countryCode.setCellValueFactory(new PropertyValueFactory<>("countryCode"));
		countryCode.setMinWidth(100);

		// ==== typeVehicleDescription ===
		countryName.setCellValueFactory(new PropertyValueFactory<>("countryName"));
		countryName.setMinWidth(255);

		// ==== typeVehicleMaxSpeed ===
		registrationDate.setCellValueFactory(new PropertyValueFactory<>("registrationDate"));
		registrationDate.setMinWidth(30);



		ObservableList<country> list = FXCollections.observableArrayList(countryDB.getCountryList());
		table.setItems(list);

		table.getColumns().addAll(countryId, countryCode, countryName, registrationDate);
		Button accueilButton = new Button("Accueil") ;
		grid.add(accueilButton, 0, 0);
		
		accueilButton.setOnAction(new EventHandler<ActionEvent>() {

			public void handle (ActionEvent event) {
				try {
					ctrl.lancerAccueil(stage);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
				);
		
		
		
		

		stage.setTitle("show country");

		Scene scene = new Scene(grid, 1000, 700);
		stage.setScene(scene);
		stage.show();

	}

	/**
	 * main method, launch stage
	 * @param args
	 */
	public static void main(String[] args) {
		launch(args);
	}
}




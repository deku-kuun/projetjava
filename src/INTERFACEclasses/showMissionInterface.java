package INTERFACEclasses;


import java.sql.SQLException;
import Controller.controller;
import DBclasses.missionDB;
import MODELclasses.mission;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.Callback;

public class showMissionInterface extends Application 
{
	private TableView<mission> table ;
	private controller ctrl;
	private Stage stage ;

	public showMissionInterface(controller ctrl)
	{
		this.ctrl = ctrl;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void start(Stage primaryStage) throws ClassNotFoundException, SQLException 
	{
		stage = primaryStage ;
		GridPane grid = new GridPane() ;
		table = new TableView<mission>();
		//table.set

		grid.add(table, 0, 1);
		TableColumn<mission, String> missionName//
		= new TableColumn<mission, String>("missionName");

		TableColumn<mission, String> missionDescription //
		= new TableColumn<mission, String>("missionDescription");

		TableColumn<mission, String> missionPlaces//
		= new TableColumn<mission, String>("missionPlaces");

		TableColumn<mission, String> missionStatus//
		= new TableColumn<mission, String>("missionStatus");

		TableColumn<mission, String> missionBeginning//
		= new TableColumn<mission, String>("missionBeginning");

		TableColumn<mission, String> missionEnding//
		= new TableColumn<mission, String>("missionEnding");

		TableColumn<mission, String> cityName //
		= new TableColumn<mission, String>("cityName");

		TableColumn<mission, String> militaryId//
		= new TableColumn<mission, String>("Creotor");

		TableColumn<mission, String> creationDate//
		= new TableColumn<mission, String>("creationDate");

		TableColumn<mission, String> missionMember //
		= new TableColumn<mission, String>("missionMember");

		TableColumn<mission, String> missionWeapon//
		= new TableColumn<mission, String>("missionWeapon");

		TableColumn<mission, String> missionVehicle//
		= new TableColumn<mission, String>("missionVehicle");


		// ==== missionName (TEXT FIELD) ===
		missionName.setCellValueFactory(new PropertyValueFactory<>("missionName"));
		missionName.setMinWidth(200);

		// ==== missionDmissionDescriptionescription (TEXT FIELD) ===
		missionDescription.setCellValueFactory(new PropertyValueFactory<>("missionDescription"));
		missionDescription.setMinWidth(200);

		// ==== missionPlaces (TEXT FIELD) ===
		missionPlaces.setCellValueFactory(new PropertyValueFactory<>("missionPlaces"));
		missionPlaces.setMinWidth(200);

		// ==== missionStatus (TEXT FIELD) ===
		missionStatus.setCellValueFactory(new PropertyValueFactory<>("missionStatus"));
		missionStatus.setMinWidth(200);

		// ==== missionBeginning (TEXT FIELD) ===
		missionBeginning.setCellValueFactory(new PropertyValueFactory<>("missionBeginning"));
		missionBeginning.setMinWidth(200);

		// ==== missionBeginning (TEXT FIELD) ===
		missionEnding.setCellValueFactory(new PropertyValueFactory<>("missionEnding"));
		missionEnding.setMinWidth(200);

		// ==== cityName (TEXT FIELD) ===
		cityName.setCellValueFactory(new PropertyValueFactory<>("missionCityLocation"));
		cityName.setMinWidth(200);

		// ==== militaryId (TEXT FIELD) ===
		militaryId.setCellValueFactory(new PropertyValueFactory<>("creatorMilitaryId"));
		militaryId.setMinWidth(200);

		// ==== militaryId (TEXT FIELD) ===
		creationDate.setCellValueFactory(new PropertyValueFactory<>("creationDate"));
		creationDate.setMinWidth(200);

		// ==== cityName (TEXT FIELD) ===
		missionMember.setCellValueFactory(new PropertyValueFactory<>("membersRequired_MilitaryIds"));
		missionMember.setMinWidth(200);

		// ==== militaryId (TEXT FIELD) ===
		missionVehicle.setCellValueFactory(new PropertyValueFactory<>("vehiclesRequired_LPs"));
		missionVehicle.setMinWidth(200);

		// ==== militaryId (TEXT FIELD) ===
		missionWeapon.setCellValueFactory(new PropertyValueFactory<>("weaponsRequired_SNs"));
		missionWeapon.setMinWidth(200);

		ObservableList<mission> list =  FXCollections.observableArrayList(missionDB.getMissionList());
		table.setItems(list);
		addStartButtonToTable();
		table.getColumns().addAll(missionName, missionDescription, missionPlaces, 
				missionStatus, missionBeginning, missionEnding,
				cityName, creationDate, militaryId, 
				missionMember,missionWeapon,missionVehicle);
		addEndButtonToTable();

		Button accueilButton = new Button("Accueil") ;
		grid.add(accueilButton, 0, 0);

		accueilButton.setOnAction(accueilButtonEvent-> 
		{
			ctrl.lancerAccueil(stage);
		}
				);

		stage.setTitle(" ");

		Scene scene = new Scene(grid, 1000, 700);
		stage.setScene(scene);
		stage.show();
	}

	private void addStartButtonToTable() {
		TableColumn<mission, Void> colBtn = new TableColumn<mission, Void>("");
		Callback<TableColumn<mission, Void>, TableCell<mission, Void>> cellFactory = new Callback<TableColumn<mission, Void>, TableCell<mission, Void>>() {
			@Override
			public TableCell<mission, Void> call(final TableColumn<mission, Void> param) {
				final TableCell<mission, Void> cell = new TableCell<mission, Void>() {
					private final Button btn = new Button("start mission");
					{
						btn.setOnAction((ActionEvent event) -> 
						{
							mission data = getTableView().getItems().get(getIndex());
							controller.startMission(data);
							ctrl.lancerShowMission(stage);
						});
					}
					@Override
					public void updateItem(Void item, boolean empty) {
						super.updateItem(item, empty);
						if (empty) {
							setGraphic(null);
						} else {
							setGraphic(btn);
						}
					}
				};
				return cell;
			}
		};	
		colBtn.setCellFactory(cellFactory);
		table.getColumns().add(colBtn);
	}

	private void addEndButtonToTable() {
		TableColumn<mission, Void> colBtn = new TableColumn<mission, Void>("");
		Callback<TableColumn<mission, Void>, TableCell<mission, Void>> cellFactory = new Callback<TableColumn<mission, Void>, TableCell<mission, Void>>() {
			@Override
			public TableCell<mission, Void> call(final TableColumn<mission, Void> param) {
				final TableCell<mission, Void> cell = new TableCell<mission, Void>() {
					private final Button btn = new Button("end mission");
					{
						btn.setOnAction((ActionEvent event) -> 
						{
							mission data = getTableView().getItems().get(getIndex());
							controller.endMission(data); 
							ctrl.lancerShowMission(stage);
						});
					}
					@Override
					public void updateItem(Void item, boolean empty) {
						super.updateItem(item, empty);
						if (empty) {
							setGraphic(null);
						} else {
							setGraphic(btn);
						}
					}
				};
				return cell;
			}
		};
		colBtn.setCellFactory(cellFactory);
		table.getColumns().add(colBtn);
	}
}
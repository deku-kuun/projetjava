package INTERFACEclasses;


import java.sql.SQLException;
import Controller.controller;
import DBclasses.typeVehicleDB;
import MODELclasses.typeVehicle;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.Callback;

public class selectTypeVehicleForVehicleInterface extends Application {
	//create tableview of typeVehicle
	private TableView<typeVehicle> table ;
	private controller ctrl;
	private Stage stage ;

	/**
	 * Constructor of controller
	 * @param ctrl
	 */
	public selectTypeVehicleForVehicleInterface(controller ctrl){
		this.ctrl = ctrl;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void start(Stage primaryStage) throws ClassNotFoundException, SQLException {
		this.stage = primaryStage ;
		GridPane grid = new GridPane() ;
		table = new TableView<typeVehicle>();
		grid.add(table, 0, 1);

		table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

		TableColumn<typeVehicle, Integer> typeVehicleIdCol 
		= new TableColumn<typeVehicle, Integer>("Id");

		TableColumn<typeVehicle, Integer> typeVehicleNameCol
		= new TableColumn<typeVehicle, Integer>("Name");

		TableColumn<typeVehicle, String> TypeVehicleDescriptionCol
		= new TableColumn<typeVehicle, String>("Description");

		TableColumn<typeVehicle, String> TypeVehicleMaxSpeedCol 
		= new TableColumn<typeVehicle, String>("Maximum speed");

		TableColumn<typeVehicle, String> TypeVehiclePlaceCol
		= new TableColumn<typeVehicle, String>("Capacity");

		TableColumn<typeVehicle, String> typeVehiclePicCol 
		= new TableColumn<typeVehicle, String>("Picture");

		TableColumn<typeVehicle, String> registrationDateCol 
		= new TableColumn<typeVehicle, String>("Registration date");

		TableColumn<typeVehicle, String> catVehicleCol 
		= new TableColumn<typeVehicle, String>("catVehicle");

		addButtonToTable();

		// ==== typeVehicleId ===
		typeVehicleIdCol.setCellValueFactory(new PropertyValueFactory<>("typeVehicleId"));
		typeVehicleIdCol.setMinWidth(30);

		// ==== typeVehicleName ===
		typeVehicleNameCol.setCellValueFactory(new PropertyValueFactory<>("typeVehicleName"));
		typeVehicleNameCol.setMinWidth(70);

		// ==== typeVehicleDescription ===
		TypeVehicleDescriptionCol.setCellValueFactory(new PropertyValueFactory<>("typeVehicleDescription"));
		TypeVehicleDescriptionCol.setMinWidth(70);

		// ==== typeVehicleMaxSpeed ===
		TypeVehicleMaxSpeedCol.setCellValueFactory(new PropertyValueFactory<>("typeVehicleMaxSpeed"));
		TypeVehicleMaxSpeedCol.setMinWidth(50);

		// ==== typeVehiclePlaces ===
		TypeVehiclePlaceCol.setCellValueFactory(new PropertyValueFactory<>("typeVehiclePlaces"));
		TypeVehiclePlaceCol.setMinWidth(20);

		// ==== typeVehiclePic ===
		typeVehiclePicCol.setCellValueFactory(new PropertyValueFactory<>("typeVehiclePic"));
		typeVehiclePicCol.setMinWidth(50);

		// ==== registrationDate  ===
		registrationDateCol.setCellValueFactory(new PropertyValueFactory<>("registrationDate"));
		registrationDateCol.setMinWidth(100);


		// ==== registrationDate  ===
		catVehicleCol.setCellValueFactory(new PropertyValueFactory<>("catVehicleName"));
		catVehicleCol.setMinWidth(100);

		ObservableList<typeVehicle> list = FXCollections.observableArrayList(typeVehicleDB.getTypeVehicleList());
		table.setItems(list);

		table.getColumns().addAll(typeVehicleIdCol, typeVehicleNameCol, TypeVehicleDescriptionCol, TypeVehicleMaxSpeedCol,TypeVehiclePlaceCol, typeVehiclePicCol ,registrationDateCol,catVehicleCol);

		stage.setTitle("select type vehicle");
		Scene scene = new Scene(grid, 1000, 700);
		stage.setScene(scene);
		stage.show();
	}

	private void addButtonToTable() 
	{
		TableColumn<typeVehicle, Void> colBtn = new TableColumn<typeVehicle, Void>("select type Vehicle");
		colBtn.setMinWidth(110);
		Callback<TableColumn<typeVehicle, Void>, TableCell<typeVehicle, Void>> cellFactory = new Callback<TableColumn<typeVehicle, Void>, TableCell<typeVehicle, Void>>() {
			@Override
			public TableCell<typeVehicle, Void> call(final TableColumn<typeVehicle, Void> param) {
				final TableCell<typeVehicle, Void> cell = new TableCell<typeVehicle, Void>() {

					private final Button btn = new Button("Select");

					{
						btn.setOnAction((ActionEvent event) -> {
							typeVehicle data = getTableView().getItems().get(getIndex());
							ctrl.selectTypeVehicleForVehicle(stage, data.getTypeVehicleName());
						});
					}
					@Override
					public void updateItem(Void item, boolean empty) {
						super.updateItem(item, empty);
						if (empty) {
							setGraphic(null);
						} else {
							setGraphic(btn);
						}
					}
				}; return cell;
			}
		};
		colBtn.setCellFactory(cellFactory);
		table.getColumns().add(colBtn);
	}

	public static void main(String[] args) {
		launch(args);
	}

}

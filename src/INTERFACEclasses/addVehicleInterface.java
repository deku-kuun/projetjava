package INTERFACEclasses;

import Controller.controller;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class addVehicleInterface extends Application {

	private String typeVehicleName = "";

	private controller ctrl;
	private Stage stage ;
	/**
	 * constructor of the controller
	 * @param ctrl
	 */
	public addVehicleInterface(controller ctrl){
		this.ctrl = ctrl;
	}

	public addVehicleInterface(controller ctrl, String typeVehicleName){
		this.ctrl = ctrl;
		this.typeVehicleName = typeVehicleName;
	}

	@Override
	public void start(Stage primaryStage) 
	{
		stage = primaryStage;
		stage.setTitle("New vehicle");

		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setVgap(10);
		grid.setHgap(10);

		Text title = new Text("Add a new vehicle");
		title.setFont(Font.font("Tahoma", FontWeight.LIGHT, 25));
		grid.add(title,1,0);

		Label addVehicleLPLabel = new Label("Licence Plate");
		grid.add(addVehicleLPLabel,0,1);

		TextField catVehicleLPText = new TextField();
		grid.add(catVehicleLPText,1,1);

		Label TypeVehicleIdLabel = new Label("vehicle model");
		grid.add(TypeVehicleIdLabel,0,2);

		Button selectTypeVehicle = new Button("Select from existing models");
		grid.add(selectTypeVehicle,1,2);
		selectTypeVehicle.setOnAction(selectTypeVehicleEvent->
		{
			ctrl.launchSelectTypeVehicleForVehicle(stage) ;
		}
				);

		Label typeVehicle = new Label(typeVehicleName);
		grid.add(typeVehicle,2,2);

		Button addVehicle = new Button("Add this vehicle");
		grid.add(addVehicle,1,3);
		addVehicle.setOnAction(addVehicleEvent->
		{	
			// ctrl.addVehicle() ;
		});

		Button launchAccueilButton =new Button("Accueil");
		grid.add(launchAccueilButton,4,0);
		launchAccueilButton.setOnAction(launchAccueilEvent->
		{
			ctrl.lancerAccueil(stage);
		}
				);

		Scene scene = new Scene(grid,1000,700);
		stage.setScene(scene);
		stage.show();
	}
}
package INTERFACEclasses;


import java.sql.SQLException;
import java.util.ArrayList;

import Controller.controller;
import DBclasses.weaponDB;
import MODELclasses.member;
import MODELclasses.vehicle;
import MODELclasses.weapon;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.Callback;

public class selectWeaponForMissionInterface extends Application
{
	private TableView<weapon> table ;
	private controller ctrl;
	private Stage stage;
	
	private String cityName ;
	private ArrayList <member> members ;
	private ArrayList <vehicle> vehicles  ;
	private ArrayList <weapon> weapons  ;
	
	/**
	 * constructor of the controller
	 * @param ctrl
	 */
	public selectWeaponForMissionInterface(controller ctrl){
		this.ctrl = ctrl;
	}

	public selectWeaponForMissionInterface(controller ctrl, String cityName,
			ArrayList <member> members, ArrayList <vehicle> vehicles,
			ArrayList <weapon> weapons)
	{
		this(ctrl) ;
		this.cityName = cityName;
		this.members = members ;
		this.vehicles = vehicles ;
		this.weapons = weapons ;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void start(Stage primaryStage) throws ClassNotFoundException, SQLException 
	{
		this.stage = primaryStage ;
		GridPane grid = new GridPane() ;
		table = new TableView <weapon>();
		grid.add(table, 0, 1);

		table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

		TableColumn<weapon, Integer> weaponNameCol//
		= new TableColumn<weapon, Integer>("serial number");

		TableColumn<weapon, String> typeWeaponCol //
		= new TableColumn<weapon, String>("weapon model");

		TableColumn<weapon, String> weaponRegistrationDateCol//
		= new TableColumn<weapon, String>("registration date");

		addButtonToTable();

		// ==== weaponSN ===
		weaponNameCol.setCellValueFactory(new PropertyValueFactory<>("weaponSN"));
		weaponNameCol.setMinWidth(50);

		// ==== registrationDate ===
		weaponRegistrationDateCol.setCellValueFactory(new PropertyValueFactory<>("registrationDate"));
		weaponRegistrationDateCol.setMinWidth(100);

		// ==== typeWeaponName ===
		typeWeaponCol.setCellValueFactory(new PropertyValueFactory<>("typeWeaponName"));
		typeWeaponCol.setMinWidth(100);

		ObservableList<weapon> list = FXCollections.observableArrayList(weaponDB.getWeaponList());
		table.setItems(list);
		table.getColumns().addAll(weaponNameCol, weaponRegistrationDateCol, typeWeaponCol) ;

		stage.setTitle("select weapon");
		Scene scene = new Scene(grid, 1000, 700);
		stage.setScene(scene);
		stage.show();

	}
	private void addButtonToTable() {
		//create column who take information of model class called typeWeapon
		TableColumn<weapon, Void> colBtn = new TableColumn<weapon, Void>("select");

		Callback<TableColumn<weapon, Void>, TableCell<weapon, Void>> cellFactory = new Callback<TableColumn<weapon, Void>, TableCell<weapon, Void>>() {
			@Override
			public TableCell<weapon, Void> call(final TableColumn<weapon, Void> param) {
				final TableCell<weapon, Void> cell = new TableCell<weapon, Void>() {
					private final Button btn = new Button("Select");
					{
						btn.setOnAction((ActionEvent event) -> 
						{
							weapon data = getTableView().getItems().get(getIndex());
							if(!weapons.contains(data))
								weapons.add(data);
							ctrl.selectWeaponForMission(stage, cityName, members, vehicles, weapons);
						});
					}
					@Override
					public void updateItem(Void item, boolean empty) {
						super.updateItem(item, empty);
						if (empty) {
							setGraphic(null);
						} else {
							setGraphic(btn);
						}
					}
				};
				return cell;
			}
		};

		colBtn.setCellFactory(cellFactory);

		table.getColumns().add(colBtn);
	}
}
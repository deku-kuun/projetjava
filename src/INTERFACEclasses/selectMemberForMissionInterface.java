package INTERFACEclasses;


import java.sql.SQLException;
import java.util.ArrayList;

import Controller.controller;
import DBclasses.memberDB;
import MODELclasses.member;
import MODELclasses.vehicle;
import MODELclasses.weapon;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.Callback;

public class selectMemberForMissionInterface extends Application {


	private TableView<member> table ;
	private controller ctrl;
	private Stage stage ;

	private String cityName = "";
	private ArrayList <member> members ;
	private ArrayList <vehicle> vehicles  ;
	private ArrayList <weapon> weapons  ;

	public selectMemberForMissionInterface(controller ctrl){
		this.ctrl = ctrl;
		members = new ArrayList <member> () ;
		vehicles = new ArrayList <vehicle> () ; 
		weapons = new ArrayList <weapon> () ;
	}

	public selectMemberForMissionInterface(controller ctrl, String cityName,
			ArrayList <member> members, ArrayList <vehicle> vehicles,
			ArrayList <weapon> weapons)
	{
		this(ctrl);
		this.cityName = cityName;
		this.members = members ;
		this.vehicles = vehicles ;
		this.weapons = weapons ;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void start(Stage primaryStage) throws ClassNotFoundException, SQLException {
		this.stage = primaryStage ;
		GridPane grid = new GridPane() ;
		table = new TableView<member>();
		grid.add(table, 0, 1);

		table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);


		TableColumn<member, Integer> MilitaryId//
		= new TableColumn<member, Integer>("MilitaryId");

		TableColumn<member, String> Email //
		= new TableColumn<member, String>("Email");

		TableColumn<member, Integer> Surname //
		= new TableColumn<member, Integer>("Surname");

		TableColumn<member, Integer> Firstname//
		= new TableColumn<member, Integer>("Firstname");

		TableColumn<member, String> picName//
		= new TableColumn<member, String>("picName");

		TableColumn<member, String> registrationDate //
		= new TableColumn<member, String>("Registration Date");

		TableColumn<member, String> rankName //
		= new TableColumn<member, String>("rankName ");


		addBoutonToTable();


		// ==== typeVehicleName ===
		MilitaryId.setCellValueFactory(new PropertyValueFactory<>("MilitaryId"));
		MilitaryId.setMinWidth(100);


		// ==== typeVehicleId ===
		Email.setCellValueFactory(new PropertyValueFactory<>("Email"));
		Email.setMinWidth(100);

		// ==== typeVehicleName ===
		Surname.setCellValueFactory(new PropertyValueFactory<>("Surname"));
		Surname.setMinWidth(100);

		// ==== typeVehicleDescription ===
		Firstname.setCellValueFactory(new PropertyValueFactory<>("Firstname"));
		Firstname.setMinWidth(100);

		// ==== typeVehicleMaxSpeed ===
		picName.setCellValueFactory(new PropertyValueFactory<>("picName"));
		picName.setMinWidth(100);

		// ==== typeVehicleMaxSpeed ===
		registrationDate.setCellValueFactory(new PropertyValueFactory<>("registrationDate"));
		registrationDate.setMinWidth(100);

		// ==== typeVehicleMaxSpeed ===
		rankName.setCellValueFactory(new PropertyValueFactory<>("rankName"));
		rankName.setMinWidth(100);



		ObservableList<member> list = FXCollections.observableArrayList(memberDB.getMemberList());
		table.setItems(list);

		table.getColumns().addAll( MilitaryId,Email,Surname,Firstname, picName, registrationDate, rankName);

		stage.setTitle("select member for mission");
		Scene scene = new Scene(grid, 1000, 700);
		stage.setScene(scene);
		stage.show();
	}

	private void addBoutonToTable() {

		TableColumn<member, Void> colBtn = new TableColumn<member, Void>("select member");

		colBtn.setMinWidth(100);


		Callback<TableColumn<member, Void>, TableCell<member, Void>> cellFactory = new Callback<TableColumn<member, Void>, TableCell<member, Void>>() {
			@Override
			public TableCell<member, Void> call(final TableColumn<member, Void> param) {
				final TableCell<member, Void> cell = new TableCell<member, Void>() {

					private final Button btn = new Button("Select");

					{
						btn.setOnAction((ActionEvent event) -> {
							member data = getTableView().getItems().get(getIndex());
							if(!members.contains(data))
								members.add(data) ;
							ctrl.selectMemberForMission(stage, cityName, members, vehicles, weapons);
						});
					}
					@Override
					public void updateItem(Void item, boolean empty) {
						super.updateItem(item, empty);
						if (empty) {
							setGraphic(null);
						} else {
							setGraphic(btn);
						}
					}
				};
				return cell;
			}
		};

		colBtn.setCellFactory(cellFactory);

		table.getColumns().add(colBtn);
	}
	public static void main(String[] args) {
		launch(args);
	}
}




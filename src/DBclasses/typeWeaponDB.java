package DBclasses;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import MODELclasses.typeWeapon;

public class typeWeaponDB 
{
	/**
	 * <!-- begin-user-doc -->
	 * This ArrayList contains all the different type of weapons in the database 
	 * <!--  end-user-doc  --> 
	 */ 
	public static ArrayList<typeWeapon> typeWeaponList ;

	/**
	 * <!-- begin-user-doc -->
	 * Method for inserting the weapon's type into the database if not in
	 * @return true if added, else false  
	 * <!--  end-user-doc  -->
	 */
	public boolean addTypeWeapon(typeWeapon typeWeaponToAdd) throws ClassNotFoundException, SQLException
	{
		boolean typeWeaponAdded = false ;
		// test if the typeWeapon already exists in the database
		if(!typeWeaponExists(typeWeaponToAdd)) 
		{
			PreparedStatement statementInsert = database.connection().prepareStatement(
					"INSERT INTO typeWeapon (typeWeaponName,typeWeaponDescription,typeWeaponWeight,typeWeaponRange,typeWeaponPic,catWeaponId) VALUES (?,?,?,?,?,?) "); // prepared request to add a new typeWeapon
			statementInsert.setString(1, typeWeaponToAdd.getTypeWeaponName()); 
			statementInsert.setString(2, typeWeaponToAdd.getTypeWeaponDescription()); 
			statementInsert.setString(3, typeWeaponToAdd.getTypeWeaponRange());	
			statementInsert.setString(4, typeWeaponToAdd.getTypeWeaponWeight()); 	
			statementInsert.setString(5, typeWeaponToAdd.getTypeWeaponPic()); 	
			statementInsert.setInt(6, getCatWeaponId(typeWeaponToAdd));
			// request's execution, if it worked <=> typeWeaponAdded = true	
			typeWeaponAdded = (statementInsert.executeUpdate()!=0) ; 	
		}
		return typeWeaponAdded ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * This method is used to convert the catWeaponName into a catWeaponId before inserting the weapon's type in the database
	 * @param typeWeapon to get the catWeaponId with its catWeaponName
	 * @return catWeaponId
	 * <!--  end-user-doc  -->
	 */
	public static int getCatWeaponId(typeWeapon typeWeapon) throws ClassNotFoundException, SQLException
	{
		PreparedStatement statementGetId= 
				database.connection().prepareStatement("SELECT catWeaponId AS id FROM catWeapon WHERE catWeaponName LIKE ?") ;   
		statementGetId.setString(1, typeWeapon.getCatWeaponName()); 
		ResultSet result = statementGetId.executeQuery() ; 
		result.next() ;  
		// return the typeWeaponId associated to the weapon
		return result.getInt("id") ; 
	}

	/**
	 * <!-- begin-user-doc -->
	 * This method is used to test if a typeWeapon already exists (by seeking its name), in order to avoid duplications problems
	 * @param typeWeaponToTest
	 * @return true if the typeWeapon already exists, else false
	 * <!--  end-user-doc  -->
	 */
	public static boolean typeWeaponExists(typeWeapon typeWeaponToTest) throws ClassNotFoundException, SQLException
	{
		boolean typeWeaponExists = false ; // boolean for the test
			PreparedStatement statementTest= 
					database.connection().prepareStatement("SELECT COUNT(*) AS occurences FROM typeWeapon WHERE typeWeaponName LIKE ?") ;
			statementTest.setString(1, typeWeaponToTest.getTypeWeaponName());        
			ResultSet resultat = statementTest.executeQuery() ;  
			resultat.next() ;
			// this typeWeapon exists <=> typeWeaponExists = true
			typeWeaponExists = (resultat.getInt("occurences")!=0) ; 
		return typeWeaponExists; 
	}

	/**
	 * <!-- begin-user-doc -->
	 * This method fills the type weapon's list with the database's datas
	 * <!--  end-user-doc  --> 
	 */
	public static void loadTypeWeaponList() throws ClassNotFoundException, SQLException {
		typeWeaponList  = new ArrayList<typeWeapon> () ;
		Statement statementLoad = database.connection().createStatement() ;
		ResultSet results = statementLoad.executeQuery("SELECT T.*, C.catWeaponName FROM typeWeapon T "
				+ "INNER JOIN catweapon C ON T.catWeaponId = C.catWeaponId") ; 

		while(results.next()) 
		{
			// while there are some lignes in the table	
			typeWeapon typeWeapon= new typeWeapon (results.getString("T.typeWeaponName"), results.getString("T.typeWeaponDescription"), 
					results.getString("T.typeWeaponRange"), results.getString("T.typeWeaponWeight"), results.getString("T.typeWeaponPic")) ;
			typeWeapon.setTypeWeaponId(results.getInt("T.typeWeaponId"));
			typeWeapon.setRegistrationDate(results.getString("T.registrationDate"));
			typeWeapon.setCatWeaponName(results.getString("C.catWeaponName"));
			typeWeaponList.add(typeWeapon) ;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * Getter for the weapon's type list
	 * @return the ArrayList which contains all the weapon's type
	 * <!--  end-user-doc  -->
	 */
	public static ArrayList<typeWeapon> getTypeWeaponList() throws ClassNotFoundException, SQLException {
		// updating the list before returning it
		loadTypeWeaponList() ; 
		return typeWeaponList ;
	}
}

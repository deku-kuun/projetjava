package DBclasses;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import MODELclasses.weapon;

public class weaponDB 
{
	/**
	 * <!-- begin-user-doc -->
	 * This HashMap contains all the weapons in the database
	 * <!--  end-user-doc  -->
	 */
	public static ArrayList<weapon> weapons ;
	
	/**
	 * <!-- begin-user-doc -->
	 * @param weaponToAdd
	 * @return true if added, else false (a error could happen when inserting)
	 * <!--  end-user-doc  -->
	 */
	public boolean addWeaponInDatabase(weapon weaponToAdd) throws ClassNotFoundException, SQLException 
	{
		boolean weaponAdded = false ;
		if(!weaponExists(weaponToAdd))  
		{
			PreparedStatement statementInsert = database.connection().prepareStatement(
					"INSERT INTO weapon(weaponSN,typeWeaponId) VALUES (?,?)"); // prepared request to add a new weapon
			statementInsert.setString(1,weaponToAdd.getWeaponSN()); 	
			statementInsert.setInt(2,getTypeWeaponId(weaponToAdd));
			// updated ?  	
			weaponAdded = (statementInsert.executeUpdate()!=0) ; 
		}
		return weaponAdded ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * This method test if a weapon serial number already exists in the database, in order to avoid duplication problems
	 * @param 
	 * @return true if this weapon exists, else false
	 * <!--  end-user-doc  -->
	 */
	public static boolean weaponExists(weapon weaponToTest) throws ClassNotFoundException, SQLException
	{
		boolean weaponExists = false ; 
		PreparedStatement statementWeaponExists= 
				database.connection().prepareStatement("SELECT COUNT(*) AS occurences FROM weapon WHERE weaponSN LIKE ?") ; 
		statementWeaponExists.setString(1, weaponToTest.getWeaponSN());          
		ResultSet result = statementWeaponExists.executeQuery() ;  
		result.next() ;  
		// this weapon exists <=> weaponExists = true 
		weaponExists = (result.getInt("occurences")!=0) ; 
		return weaponExists; 
	}

	/**
	 * <!-- begin-user-doc -->
	 * This method is used to convert the typeWeaponName into a typeWeaponId before inserting the weapon in the database
	 * @param weapon to get the typeWeaponId with its typeWeaponName
	 * @return typeWeaponId
	 * <!--  end-user-doc  -->
	 */
	public static int getTypeWeaponId(weapon weapon) throws ClassNotFoundException, SQLException
	{
		PreparedStatement statementGetId= 
				database.connection().prepareStatement("SELECT typeWeaponId AS id FROM typeWeapon WHERE typeWeaponName LIKE ?") ;   
		statementGetId.setString(1, weapon.getTypeWeaponName()); 
		ResultSet result = statementGetId.executeQuery() ; 
		result.next() ;  
		// return the typeWeaponId associated to the weapon
		return result.getInt("id") ; 
	}

	/**
	 * <!-- begin-user-doc -->
	 * This method fill the weapon's list with the database's datas
	 * <!--  end-user-doc  -->
	 */
	public static void loadWeaponsList() throws ClassNotFoundException, SQLException 
	{
		weapons = new ArrayList<weapon> ();
		Statement statementLoad = database.connection().createStatement() ; 
		ResultSet results = statementLoad.executeQuery("SELECT W.*, T.typeWeaponName FROM weapon W "
				+ "INNER JOIN typeweapon T ON T.typeWeaponId = W.typeWeaponId ") ; 
		while(results.next()) 
		{
				// while there are some lignes in the table	
				weapon weapon = new weapon(results.getString("W.weaponSN")) ;
				weapon.setRegistrationDate(results.getString("W.registrationDate"));
				weapon.setTypeWeaponName(results.getString("T.typeWeaponName"));
				weapons.add(weapon) ;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * Getter for the weapons list
	 * @return the ArrayList which contains all the weapons
	 * <!--  end-user-doc  -->
	 */
	public static ArrayList<weapon> getWeaponList() throws ClassNotFoundException, SQLException {
		loadWeaponsList() ; // updating the list before returning it
		return weapons;
	}

}

package DBclasses;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import MODELclasses.squad;

public class squadDB 
{
	/**
	 * <!-- begin-user-doc -->
	 * This ArrayList contains all the squads in the database 
	 * <!--  end-user-doc  --> 
	 */ 
	public static ArrayList<squad> squadList ;

	/**
	 * <!-- begin-user-doc -->
	 * Method for inserting the squad into the database if not in
	 * @param squadToAdd
	 * @return true if added, else false  
	 * <!--  end-user-doc  -->
	 */
	public boolean addSquad(squad squadToAdd) throws ClassNotFoundException, SQLException
	{
		boolean squadAdded = false ;
		// test if the squad already exists in the database
		if(!squadExists(squadToAdd)) 
		{
			// prepared request to add a new squad
			PreparedStatement statementInsert = database.connection().prepareStatement(
					"INSERT INTO squad (squadName) VALUES (?) "); 
			statementInsert.setString(1, squadToAdd.getSquadName()); 
			// request's execution, if it worked <=> squadAdded = true	
			squadAdded = (statementInsert.executeUpdate()!=0) ; 	
		}
		return squadAdded ;
	}

	/**
	 * This method tests if a squad already exists in the database by seeking its name
	 * @param squadToTest
	 * @return true if the squad exists, else false
	 * <!--  end-user-doc  --> 
	 */
	public static boolean squadExists(squad squadToTest) throws ClassNotFoundException, SQLException
	{
		boolean squadExists = false ; // boolean for the test
		PreparedStatement statementTest= 
				database.connection().prepareStatement("SELECT COUNT(*) AS occurences FROM squad WHERE squadName LIKE ?") ; 
		statementTest.setString(1, squadToTest.getSquadName());         
		ResultSet result = statementTest.executeQuery() ;  
		result.next() ; 
		// this squad exists <=> squadExists = true
		squadExists = (result.getInt("occurences")!=0) ; 
		return squadExists;
	}

	/**
	 * <!-- begin-user-doc -->
	 * This method fills the squad's list with the database's datas
	 * <!--  end-user-doc  --> 
	 */
	public static void loadSquadList() throws ClassNotFoundException, SQLException {
		squadList  = new ArrayList<squad> () ;
		Statement statementLoad = database.connection().createStatement() ;
		ResultSet results = statementLoad.executeQuery("SELECT * FROM squad") ;
		while(results.next()) 
		{
			// while there are some lignes in the table	
			squad squad = new squad(results.getString("squadName")) ;
			squad.setSquadId(results.getInt("squadId"));
			squad.setCreationDate(results.getString("creationDate"));
			squadList.add(squad) ;  
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * Getter for the squad list
	 * @return the ArrayList which contains all the squads
	 * <!--  end-user-doc  -->
	 */
	public static ArrayList<squad> getSquadList() throws ClassNotFoundException, SQLException {
		// updating the list before returning it
		loadSquadList() ; 
		return squadList ;
	}
}


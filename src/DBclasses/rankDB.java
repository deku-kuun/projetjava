package DBclasses;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import MODELclasses.rank;

public class rankDB 
{
	/**
	 * <!-- begin-user-doc -->
	 * This ArrayList contains all the ranks in the database 
	 * <!--  end-user-doc  --> 
	 */ 
	public static ArrayList<rank> rankList ;

	/**
	 * <!-- begin-user-doc -->
	 * Method for inserting the rank into the database if not in
	 * @param rankToAdd
	 * @return true if added, else false  
	 * <!--  end-user-doc  -->
	 */
	public boolean addRank(rank rankToAdd) throws ClassNotFoundException, SQLException
	{
		boolean rankAdded = false ;
		// test if the rank already exists in the database
		if(!rankExists(rankToAdd)) 
		{
			// prepared request to add a new rank
			PreparedStatement statementInsert = database.connection().prepareStatement(
					"INSERT INTO rank (rankCode, rankName) VALUES (?,?)"); 
			statementInsert.setString(1, rankToAdd.getRankCode()); 
			statementInsert.setString(2, rankToAdd.getRankName());
			// request's execution, if it worked <=> rankAdded = true	
			rankAdded = (statementInsert.executeUpdate()!=0) ; 	
		}
		return rankAdded ;
	}

	/**
	 * This method tests if a rank exists in the database by seeking with its name
	 * @param rankToTest
	 * @return true if the rank exists, else false
	 * <!--  end-user-doc  --> 
	 */
	public static boolean rankExists(rank rankToTest) throws ClassNotFoundException, SQLException
	{
		boolean rankExists = false ; // boolean for the test
		PreparedStatement statementTest= 
				database.connection().prepareStatement("SELECT COUNT(*) AS occurences FROM rank WHERE rankName LIKE ?") ; 
		statementTest.setString(1, rankToTest.getRankName());         
		ResultSet result = statementTest.executeQuery() ;  
		result.next() ; 
		// this rank exists <=> rankExists = true
		rankExists = (result.getInt("occurences")!=0) ; 
		return rankExists;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * This method fills the rank's list with the database's datas
	 * <!--  end-user-doc  --> 
	 */
	public static void loadRankList() throws ClassNotFoundException, SQLException { 
		rankList  = new ArrayList<rank>() ;
		Statement statementLoad = database.connection().createStatement() ;
		ResultSet results = statementLoad.executeQuery("SELECT * FROM rank") ; 
		while(results.next()) 
		{
			// while there are some lignes in the table	
			rank rank = new rank(results.getString("rankName"), results.getString("rankCode")) ;
			rank.setRankId(results.getInt("rankId"));
			rank.setCreationDate(results.getString("creationDate"));
			rankList.add(rank) ;       
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * Getter for the rank list
	 * @return the ArrayList which contains the ranks
	 * <!--  end-user-doc  -->
	 */
	public static ArrayList<rank> getRankList() throws ClassNotFoundException, SQLException {
		// updating the list before returning it
		loadRankList() ; 
		return rankList ;
	}
}
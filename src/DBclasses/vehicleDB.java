package DBclasses;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import MODELclasses.vehicle;

public class vehicleDB 
{
	/**
	 * <!-- begin-user-doc -->
	 * This ArrayList contains all the squads in the database 
	 * <!--  end-user-doc  --> 
	 */ 
	public static ArrayList<vehicle> vehicles ;

	/**
	 * <!-- begin-user-doc -->
	 * This method add a vehicle into the database if not already in
	 * @param vehicleToAdd
	 * @return true if added, else false (an error could happen when inserting)
	 * <!--  end-user-doc  -->
	 */
	public boolean addVehicleInDatabase(vehicle vehicleToAdd) throws ClassNotFoundException, SQLException 
	{
		boolean vehicleAdded = false ;
		if(!vehicleExists(vehicleToAdd))  
		{
			PreparedStatement statementInsert = database.connection().prepareStatement(
					"INSERT INTO vehicle(vehicleLP,typeVehicleId) VALUES (?,?)"); // prepared request to add a new vehicle
			statementInsert.setString(1,vehicleToAdd.getVehicleLP()); 	
			statementInsert.setInt(2,getTypeVehicleId(vehicleToAdd));
			// updated ?  	
			vehicleAdded = (statementInsert.executeUpdate()!=0) ; 
		}
		return vehicleAdded ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * This method test if a vehicle licence plate already exists in the database, in order to avoid duplication problems
	 * @param vehicleToTest
	 * @return true if this vehicle exists, else false
	 * <!--  end-user-doc  -->
	 */
	public static boolean vehicleExists(vehicle vehicleToTest) throws ClassNotFoundException, SQLException
	{
		boolean vehicleExists = false ; 
		PreparedStatement statementVehicleExists= 
				database.connection().prepareStatement("SELECT COUNT(*) AS occurences FROM vehicle WHERE vehicleLP LIKE ?") ; 
		statementVehicleExists.setString(1, vehicleToTest.getVehicleLP());          
		ResultSet result = statementVehicleExists.executeQuery() ;  
		result.next() ;  
		// this vehicle exists <=> vehicleExists = true 
		vehicleExists = (result.getInt("occurences")!=0) ; 
		return vehicleExists; 
	}

	/**
	 * <!-- begin-user-doc -->
	 * This method is used to convert the typeVehicleName into a typeVehicleId before inserting the vehicle in the database
	 * @param vehicle to get the typeVehicleId with its typeVehicleName
	 * @return typeVehicleId
	 * <!--  end-user-doc  -->
	 */
	public static int getTypeVehicleId(vehicle vehicle) throws ClassNotFoundException, SQLException
	{
		PreparedStatement statementGetId= 
				database.connection().prepareStatement("SELECT typeVehicleId AS id FROM typeVehicle WHERE typeVehicleName LIKE ?") ;   
		statementGetId.setString(1, vehicle.getTypeVehicleName()); 
		ResultSet result = statementGetId.executeQuery() ; 
		result.next() ;  
		// return the typeVehicleId associated to the vehicle
		return result.getInt("id") ; 
	}

	/**
	 * <!-- begin-user-doc -->
	 * This method fill the vehicle's list with the database's datas
	 * <!--  end-user-doc  -->
	 */
	public static void loadVehiclesList() throws ClassNotFoundException, SQLException 
	{
		vehicles = new  ArrayList<vehicle> ();
		Statement statementLoad = database.connection().createStatement() ; 
		ResultSet results = statementLoad.executeQuery("SELECT V.*, T.typeVehicleName FROM vehicle V "
				+ "INNER JOIN typeVehicle T ON T.typeVehicleId = V.typeVehicleId") ; 
		while(results.next()) 
		{
			// while there are some lignes in the table	
			vehicle vehicle = new vehicle(results.getString("V.vehicleLP")) ;
			vehicle.setRegistrationDate(results.getString("V.registrationDate"));
			vehicle.setTypeVehicleName(results.getString("T.typeVehicleName"));
			vehicles.add(vehicle) ;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * Getter for the vehicles list
	 * @return the ArrayList which contains all the vehicles
	 * <!--  end-user-doc  -->
	 */
	public static ArrayList<vehicle> getVehiclesList() throws ClassNotFoundException, SQLException {
		loadVehiclesList() ; // updating the list before returning it
		return vehicles;
	}
}

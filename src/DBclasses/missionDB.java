package DBclasses;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import MODELclasses.mission;

public class missionDB 
{
	/**
	 * <!-- begin-user-doc -->
	 * This arrayList contains all the missions in the database :  <br>
	 * <!--  end-user-doc  --> 
	 */
	public static ArrayList<mission> missionList ;


	/**
	 * <!-- begin-user-doc -->
	 * This method add a mission into the database if not already in, 
	 * it also add the members, vehicles and weapons concerned by this weapon into their respectives tables
	 * @param missionToAdd
	 * @return true if added, else false (an error could happen when inserting)
	 *<!--  end-user-doc  --> 
	 */
	public boolean addMissionInDatabase(mission missionToAdd) throws ClassNotFoundException, SQLException
	{
		boolean missionAddedAll = false ;
		try
		{
			if(!missionExists(missionToAdd))
			{
				// start TRANSACTION by removing the default autocommit 
				database.connection().setAutoCommit(false);

				//adding the new mission
				boolean missionAdded = false ;
				PreparedStatement statementInsertMission= 
						database.connection().prepareStatement("INSERT INTO mission(missionName,missionDescription,"
								+ "missionPlaces,missionStatus,cityId, militaryId) VALUES (?,?,?,?,?,?)") ;
				statementInsertMission.setString(1, missionToAdd.getMissionName());
				statementInsertMission.setString(2, missionToAdd.getMissionDescription());
				statementInsertMission.setInt(3, missionToAdd.getMembersRequired_MilitaryIds().size());
				statementInsertMission.setString(4, missionToAdd.getMissionStatus());
				statementInsertMission.setInt(5, getCityId(missionToAdd));
				statementInsertMission.setString(6, missionToAdd.getCreatorMilitaryId());

				missionAdded = (statementInsertMission.executeUpdate()!=0) ;

				boolean membersRequiredAdded ;
				if(!missionToAdd.getMembersRequired_MilitaryIds().isEmpty())
				{
					//adding the required members
					membersRequiredAdded = false ;
					PreparedStatement statementInsertMission_has_member= 
							database.connection().prepareStatement("INSERT INTO mission_has_member(missionId, militaryId) VALUES (?,?)") ;
					for(int indexMemberList=0; indexMemberList < missionToAdd.getMembersRequired_MilitaryIds().size() ; indexMemberList++)
					{
						statementInsertMission_has_member.setInt(1, getAutoIncrementValue()-1);
						statementInsertMission_has_member.setString(2, missionToAdd.getMembersRequired_MilitaryIds().get(indexMemberList));
						membersRequiredAdded = (statementInsertMission_has_member.executeUpdate()!=0) ;
					}
				}
				else
					membersRequiredAdded = true ;

				boolean weaponsRequiredAdded ;
				if(!missionToAdd.getWeaponsRequired_SNs().isEmpty())
				{
					//adding the required weapons
					weaponsRequiredAdded = false ;
					PreparedStatement statementInsertMission_needs_weapon= 
							database.connection().prepareStatement("INSERT INTO mission_needs_weapon(missionId, weaponSN) VALUES (?,?)") ;
					for(int indexWeaponList=0; indexWeaponList < missionToAdd.getWeaponsRequired_SNs().size() ; indexWeaponList++)
					{
						statementInsertMission_needs_weapon.setInt(1, getAutoIncrementValue()-1);
						statementInsertMission_needs_weapon.setString(2, missionToAdd.getWeaponsRequired_SNs().get(indexWeaponList));
						weaponsRequiredAdded = (statementInsertMission_needs_weapon.executeUpdate()!=0) ;
					}
				}
				else
					weaponsRequiredAdded = true ;

				boolean vehiclesRequiredAdded;
				if(!missionToAdd.getVehiclesRequired_LPs().isEmpty())
				{
					//adding the required vehicles
					vehiclesRequiredAdded = false ;
					PreparedStatement statementInsertMission_needs_vehicle= 
							database.connection().prepareStatement("INSERT INTO mission_needs_vehicle(missionId, vehicleLP) VALUES (?,?)") ;
					for(int indexVehicleList=0; indexVehicleList < missionToAdd.getVehiclesRequired_LPs().size() ; indexVehicleList++)
					{
						statementInsertMission_needs_vehicle.setInt(1, getAutoIncrementValue()-1);
						statementInsertMission_needs_vehicle.setString(2, missionToAdd.getVehiclesRequired_LPs().get(indexVehicleList));
						vehiclesRequiredAdded = (statementInsertMission_needs_vehicle.executeUpdate()!=0) ;
					}
				}
				else
					vehiclesRequiredAdded = true ;

				// All the request have been executed <=> missionAddedAll = true
				missionAddedAll = missionAdded && membersRequiredAdded && weaponsRequiredAdded && vehiclesRequiredAdded ;
				// ending transaction block, commit updates AND setting the connection back to autocommit
				database.connection().commit();
				database.connection().setAutoCommit(true);
			}
		}
		catch(SQLException e)
		{
			if(!e.getMessage().contains("autocommit"))
			{
				throw e ;	
			}
		}
		return missionAddedAll ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * This method is used to test if a mission's name already exists, in order to avoid duplications problems
	 * @param missionToTest
	 * @return true if already exists, else false 
	 * <!--  end-user-doc  --> 
	 */
	public static boolean missionExists(mission missionToTest) throws ClassNotFoundException, SQLException
	{
		boolean missionExists = false ; 
		PreparedStatement statementTest= 
				database.connection().prepareStatement("SELECT COUNT(*) AS occurences FROM mission WHERE missionName LIKE ?") ; 

		statementTest.setString(1, missionToTest.getMissionName());         
		ResultSet resultat = statementTest.executeQuery() ;  // 
		resultat.next() ;
		// This mission exists <=> missionExists = true 
		missionExists = (resultat.getInt("occurences")!=0) ;
		return missionExists ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * This method is used to convert the missionName into the missionId
	 * @param mission to get the missionId with its name
	 * @return missionId
	 * <!--  end-user-doc  -->
	 */
	public static int getMissionId(mission mission) throws ClassNotFoundException, SQLException
	{
		PreparedStatement statementGetId= 
				database.connection().prepareStatement("SELECT missionId AS id FROM mission WHERE missionName LIKE ?") ;   
		statementGetId.setString(1, mission.getMissionName()); 
		ResultSet result = statementGetId.executeQuery() ; 
		result.next() ;  
		// return the missionId associated
		return result.getInt("id") ; 
	}

	/**
	 * <!-- begin-user-doc -->
	 * This method is used to convert the CityPostCode into the cityId
	 * @param mission to convert the cityId with its postCode
	 * @return cityId
	 * <!--  end-user-doc  -->
	 */
	public static int getCityId(mission mission) throws ClassNotFoundException, SQLException 
	{
		PreparedStatement statementGetId= 
				database.connection().prepareStatement("SELECT cityId AS id FROM city WHERE cityName LIKE ?") ;   
		statementGetId.setString(1, mission.getMissionCityLocation()); 
		ResultSet result = statementGetId.executeQuery() ; 
		result.next() ;  
		// return the missionId associated
		return result.getInt("id") ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * This method is used to get the next auto increment's value of missionId from the table mission
	 * @return the next auto increment value
	 * <!--  end-user-doc  -->
	 */
	public static int getAutoIncrementValue() throws ClassNotFoundException, SQLException
	{
		Statement statementLoad = database.connection().createStatement() ; 
		ResultSet resultAutoIncr = statementLoad.executeQuery("SELECT auto_increment AS value FROM information_schema.tables "
				+ "WHERE table_schema = 'h-mog_army' AND table_name='mission'") ;
		resultAutoIncr.next() ;  
		// return the value of the next id
		return resultAutoIncr.getInt("value") ;

	}

	/*
	 * <!-- begin-user-doc -->
	 * This method is used to update the status/beginning date of a mission when it begins
	 * @param missionToUpdate
	 * @param true if updated
	 * <!--  end-user-doc  -->
	 */
	public boolean beginMission(mission missionToUpdate) throws ClassNotFoundException, SQLException
	{
		PreparedStatement updateStatus= 
				database.connection().prepareStatement("UPDATE mission SET missionStatus = ?, missionBeginning = current_timestamp() WHERE missionId = ?") ;
		updateStatus.setString(1, missionToUpdate.getMissionStatus());
		updateStatus.setInt(2, missionToUpdate.getMissionId());
		return (updateStatus.executeUpdate()!=0) ;
	}

	/*
	 * <!-- begin-user-doc -->
	 * This method is used to update the status/ending date of a mission when it ends
	 * @param missionToUpdate
	 * @param true if updated
	 * <!--  end-user-doc  -->
	 */
	public boolean endMission(mission missionToUpdate) throws ClassNotFoundException, SQLException
	{
		PreparedStatement updateStatus= 
				database.connection().prepareStatement("UPDATE mission SET missionStatus = ?, missionEnding = current_timestamp() WHERE missionId = ?") ;
		updateStatus.setString(1, missionToUpdate.getMissionStatus());
		updateStatus.setInt(2, missionToUpdate.getMissionId());
		return (updateStatus.executeUpdate()!=0) ;
	}
	/**
	 * <!-- begin-user-doc -->
	 * This method fills the mission list by creating missions for each lines we can find in the mission table (mission needs... / mission has...)
	 * <!--  end-user-doc  -->
	 */
	public static void loadMissionList() throws ClassNotFoundException, SQLException {
		missionList = new ArrayList<mission> () ;
		Statement statementLoad = database.connection().createStatement() ; 
		ResultSet resultMission = statementLoad.executeQuery("SELECT M.missionId, M.missionName, M.missionDescription, M.missionPlaces, M.missionStatus,"
				+ "M.creationDate, M.missionBeginning, M.missionEnding, C.cityName, M.militaryId "
				+ "FROM mission M "
				+ "INNER JOIN city C ON C.cityId = M.cityId") ; 

		// ArrayList for the membersRequired
		ArrayList <String> membersRequired_militaryIds = new ArrayList <String>() ;
		PreparedStatement resultMembers= 
				database.connection().prepareStatement("SELECT militaryId FROM mission_has_member WHERE missionId LIKE ?") ;
		// ArrayList for the weaponsRequired
		ArrayList <String> weaponsRequired_SNs = new ArrayList <String>() ;
		PreparedStatement resultWeapons= 
				database.connection().prepareStatement("SELECT weaponSN FROM mission_needs_weapon WHERE missionId LIKE ?") ; 
		// ArrayList for the vehiclesRequired
		ArrayList <String> vehiclesRequired_LPs = new ArrayList <String>() ;
		PreparedStatement resultVehicles= 
				database.connection().prepareStatement("SELECT vehicleLP FROM mission_needs_vehicle WHERE missionId LIKE ?") ; 

		while(resultMission.next()) 
			// while there are some lignes in the mission table
		{
			int missionId = resultMission.getInt("missionId") ;
			// request execution for mission_has_member
			resultMembers.setInt(1,missionId);
			ResultSet members = resultMembers.executeQuery() ;
			// request execution for mission_needs_weapon
			resultWeapons.setInt(1,missionId);
			ResultSet weapons = resultWeapons.executeQuery() ;
			// request execution for mission_needs_vehicle
			resultVehicles.setInt(1,missionId);
			ResultSet vehicles = resultVehicles.executeQuery() ;

			// Adding members to the list
			while(members.next())
				membersRequired_militaryIds.add(members.getString("militaryId")) ;

			// Adding weapons to the list
			while(weapons.next())
				weaponsRequired_SNs.add(weapons.getString("weaponSN")) ;

			// Adding vehicles to the list
			while(vehicles.next())
				vehiclesRequired_LPs.add(vehicles.getString("vehicleLP")) ;

			//Creating a new mission add set all the attributes
			mission mission = new mission(resultMission.getString("M.missionName"), 
					resultMission.getString("M.missionDescription"),resultMission.getString("M.militaryId")) ;
			mission.setMissionId(missionId);
			mission.setMissionStatus(resultMission.getString("M.missionStatus"));
			mission.setMissionPlaces(resultMission.getString("M.missionPlaces"));
			mission.setCreationDate(resultMission.getString("M.creationDate"));
			mission.setMissionBeginning(resultMission.getString("M.missionBeginning")) ;
			mission.setMissionEnding(resultMission.getString("M.missionEnding")) ;
			mission.setMissionCityLocation(resultMission.getString("C.cityName")) ;
			mission.setMembersRequired_MilitaryIds(membersRequired_militaryIds) ;
			mission.setWeaponsRequired_SNs(weaponsRequired_SNs);
			mission.setVehiclesRequired_LPs(vehiclesRequired_LPs);
			// adding the new mission to the list
			missionList.add(mission); 
			// clear all the lists about this mission   
			membersRequired_militaryIds = new ArrayList <String>() ;
			weaponsRequired_SNs = new ArrayList <String>() ;
			vehiclesRequired_LPs = new ArrayList <String>() ;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * Getter for the mission list
	 * @return the HashMap which contains the missions
	 * <!--  end-user-doc  -->
	 */
	public static ArrayList<mission> getMissionList() throws ClassNotFoundException, SQLException {
		loadMissionList() ;
		return missionList;
	}
}

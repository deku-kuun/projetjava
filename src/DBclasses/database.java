package DBclasses;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class database 
{
	// project database URL link
	public static final String URL = "jdbc:mysql://localhost/h-mog_army"; 
	// user for MySQL_BD
	public static final String USER = "root" ; 
	// password for MySQL_BD
	public static final String PASSWORD = ""; 
	// creating a new connection
	public static Connection connect = null ;

	/**
	 * This method create the connection between the database and java_jdbc  
	 * @return the java connection to the database
	 */
	public static Connection connection() throws ClassNotFoundException, SQLException
	{
		Class.forName("com.mysql.jdbc.Driver");
		// Get connection
		connect = DriverManager.getConnection(URL, USER, PASSWORD);    
		return connect ;
	}
}

package DBclasses;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import MODELclasses.typeSoldier;

public class typeSoldierDB 
{
	/**
	 * <!-- begin-user-doc -->
	 * This ArrayList contains all the soldier's type in the database 
	 * <!--  end-user-doc  --> 
	 */ 
	public static ArrayList<typeSoldier> typeSoldierList ;

	/**
	 * <!-- begin-user-doc -->
	 * Method for inserting the typeSoldier into the database if not in
	 * @param typeSoldierToAdd
	 * @return true if added, else false  
	 * <!--  end-user-doc  -->
	 */
	public boolean addTypeSoldier(typeSoldier typeSoldierToAdd) throws ClassNotFoundException, SQLException
	{
		boolean typeSoldierAdded = false ;
		// test if the typeSoldier already exists in the database
		if(!typeSoldierExists(typeSoldierToAdd)) 
		{
			// prepared request to add a new typeSoldier
			PreparedStatement statementInsert = database.connection().prepareStatement(
					"INSERT INTO typeSoldier (typeSoldierName) VALUES (?) "); 
			statementInsert.setString(1, typeSoldierToAdd.getTypeSoldierName()); 
			// request's execution, if it worked <=> typeSoldierAdded = true	
			typeSoldierAdded = (statementInsert.executeUpdate()!=0) ; 	
		}
		return typeSoldierAdded ;
	}

	/**
	 * This method tests if a typeSoldier already exists in the database by seeking its name
	 * @param typeSoldierToTest
	 * @return true if the typeSoldier exists, else false
	 * <!--  end-user-doc  --> 
	 */
	public static boolean typeSoldierExists(typeSoldier typeSoldierToTest) throws ClassNotFoundException, SQLException
	{
		boolean typeSoldierExists = false ; // boolean for the test
		PreparedStatement statementTest= 
				database.connection().prepareStatement("SELECT COUNT(*) AS occurences FROM typeSoldier WHERE typeSoldierName LIKE ?") ; 
		statementTest.setString(1, typeSoldierToTest.getTypeSoldierName());         
		ResultSet result = statementTest.executeQuery() ;  
		result.next() ; 
		// this typeSoldier exists <=> typeSoldierExists = true
		typeSoldierExists = (result.getInt("occurences")!=0) ; 
		return typeSoldierExists;
	}

	/**
	 * <!-- begin-user-doc -->
	 * This method fills the soldier types list with the database's datas
	 * <!--  end-user-doc  --> 
	 */
	public static void loadTypeSoldierList() throws ClassNotFoundException, SQLException { 
		typeSoldierList  = new ArrayList<typeSoldier> () ;
		Statement statementLoad = database.connection().createStatement() ;
		ResultSet results = statementLoad.executeQuery("SELECT * FROM typeSoldier") ; 
		while(results.next()) 
		{
			typeSoldier typeSoldier = new typeSoldier(results.getString("typeSoldierName")) ;
			typeSoldier.setTypeSoldierId(results.getInt("typeSoldierId"));
			typeSoldier.setCreationDate(results.getString("creationDate"));
			typeSoldierList.add(typeSoldier) ;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * Getter for the typeSoldier list
	 * @return the ArrayList which contains all the soldier types
	 * <!--  end-user-doc  -->
	 */
	public static ArrayList<typeSoldier> getTypeSoldierList() throws ClassNotFoundException, SQLException {
		// updating the list before returning it
		loadTypeSoldierList() ; 
		return typeSoldierList ;
	}
}


package DBclasses;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import MODELclasses.catWeapon;

public class catWeaponDB 
{
	/**
	 * <!-- begin-user-doc -->
	 * This ArrayList contains all the ranks in the database 
	 * <!--  end-user-doc  --> 
	 */
	public static ArrayList<catWeapon> catWeaponList  ;

	/**
	 * <!-- begin-user-doc -->
	 * Method for inserting the weapon category into the database if not in
	 * @param catWeaponToAdd
	 * @return true if added, else false  
	 * <!--  end-user-doc  -->
	 */
	public boolean addCatWeapon(catWeapon catWeaponToAdd) throws ClassNotFoundException, SQLException
	{
		boolean catWeaponAdded = false ;
		// test if the catWeapon already exists in the database
		if(!catWeaponExists(catWeaponToAdd)) 
		{
			// prepared request to add a new weapon category
			PreparedStatement statementInsert = database.connection().prepareStatement(
					"INSERT INTO catWeapon (catWeaponName) VALUES (?) "); 
			statementInsert.setString(1, catWeaponToAdd.getCatWeaponName()); 
			// request's execution, if it worked <=> catWeaponAdded = true	
			catWeaponAdded = (statementInsert.executeUpdate()!=0) ; 	
		}
		return catWeaponAdded ;
	}

	/**
	 * This method tests if a category of weapon already exists in the database by seeking with name
	 * @param catWeaponToTest
	 * @return true if the category exists, else false
	 * <!--  end-user-doc  --> 
	 */
	public static boolean catWeaponExists(catWeapon catWeaponToTest) throws ClassNotFoundException, SQLException
	{
		boolean catWeaponExists = false ; // boolean for the test
		PreparedStatement statementTest= 
				database.connection().prepareStatement("SELECT COUNT(*) AS occurences FROM catWeapon WHERE catWeaponName LIKE ?") ; 
		statementTest.setString(1, catWeaponToTest.getCatWeaponName());         
		ResultSet result = statementTest.executeQuery() ;  
		result.next() ; 
		// this catWeapon exists <=> catWeaponExists = true
		catWeaponExists = (result.getInt("occurences")!=0) ; 
		return catWeaponExists;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * This method fills the catWeapon's list with the database's datas
	 * <!--  end-user-doc  --> 
	 */
	public static void loadCatWeaponList() throws ClassNotFoundException, SQLException {
		catWeaponList  = new ArrayList<catWeapon>() ;
		Statement statementLoad = database.connection().createStatement() ;
		ResultSet results = statementLoad.executeQuery("SELECT * FROM catWeapon") ;  
		while(results.next()) 
		{
			// while there are some lignes in the table	
			catWeapon catWeapon = new catWeapon(results.getString("catWeaponName")) ;
			catWeapon.setCatWeaponId(results.getInt("catWeaponId") );  
			catWeapon.setRegistrationDate(results.getString("registrationDate"));
			catWeaponList.add(catWeapon) ;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * Getter for the weapon category list
	 * @return the ArrayList which contains all weapon categories
	 * <!--  end-user-doc  -->
	 */
	public static ArrayList<catWeapon> getCatWeaponList() throws ClassNotFoundException, SQLException {
		// updating the list before returning it
		loadCatWeaponList() ; 
		return catWeaponList ;
	}
}

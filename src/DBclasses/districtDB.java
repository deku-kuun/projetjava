package DBclasses;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import MODELclasses.district;

public class districtDB 
{
	/**
	 * <!-- begin-user-doc -->
	 * This ArrayList contains all the district in the database 
	 * <!--  end-user-doc  -->
	 */
	public static ArrayList<district> districtList;

	/**
	 * <!-- begin-user-doc -->
	 * This method add a district into the database if not already in
	 * @param districtToAdd
	 * @return true if added, else false (an error could happen when inserting)
	 * <!--  end-user-doc  -->
	 */
	public boolean adddistrictInDatabase(district districtToAdd) throws ClassNotFoundException, SQLException 
	{
		boolean districtAdded = false ;
		if(!districtExists(districtToAdd))  
		{
			PreparedStatement statementInsert = database.connection().prepareStatement(
					"INSERT INTO district(districtCode, districtName, countryCode) VALUES (?,?,?)"); // prepared request to add a new district
			statementInsert.setString(1,districtToAdd.getDistrictCode()); 	
			statementInsert.setString(2,districtToAdd.getDistrictName());
			statementInsert.setString(3,getCountryCode(districtToAdd));
			// updated ?	
			districtAdded = (statementInsert.executeUpdate()!=0) ; 
		}
		return districtAdded ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * This method test if a district already exists in the database (districtCode), in order to avoid duplication problems
	 * @param districtToTest
	 * @return true if this district exists, else false
	 * <!--  end-user-doc  -->
	 */
	public static boolean districtExists(district districtToTest) throws ClassNotFoundException, SQLException
	{
		boolean districtExists = false ; 
		PreparedStatement statementDistrictExists= 
				database.connection().prepareStatement("SELECT COUNT(*) AS occurences FROM district WHERE districtCode LIKE ?") ; 
		statementDistrictExists.setString(1, districtToTest.getDistrictCode()); 
		ResultSet result = statementDistrictExists.executeQuery() ;  
		result.next() ;  
		// this district exists <=> districtExists = true 
		districtExists = (result.getInt("occurences")!=0) ; 
		return districtExists; 
	}

	/**
	 * <!-- begin-user-doc -->
	 * This method is used to convert the countryName into a countryCode before inserting the district in the database
	 * @param discrict to get the countryCode with its name
	 * @return countryCode
	 * <!--  end-user-doc  -->
	 */
	public static String getCountryCode(district district) throws ClassNotFoundException, SQLException
	{
		PreparedStatement statementGetCode= 
				database.connection().prepareStatement("SELECT countryCode AS code FROM country WHERE countryName LIKE ?") ;   
		statementGetCode.setString(1, district.getCountryName()); 
		ResultSet result = statementGetCode.executeQuery() ; 
		result.next() ;  
		// return the countryCode associated to the country
		return result.getString("code") ; 
	}

	/**
	 * <!-- begin-user-doc --> 
	 * This method fill the districts list with the database's datas
	 * <!--  end-user-doc  --> 
	 */
	public static void loadDistrictList() throws ClassNotFoundException, SQLException { 
		districtList = new ArrayList<district> ();
		Statement statementLoad = database.connection().createStatement() ;
		ResultSet resultat = statementLoad.executeQuery("SELECT D.districtId, D.DistrictCode, D.districtName, "
				+ "D.registrationDate, C.countryName FROM district D "
				+ "INNER JOIN country C ON D.countryCode= C.countryCode") ;
		while(resultat.next())	
		{
			district district = new district(resultat.getString("D.DistrictCode"), resultat.getString("D.districtName")) ;
			district.setDistrictId(resultat.getInt("D.districtId"));
			district.setRegistrationDate(resultat.getString("D.registrationDate"));
			district.setCountryName(resultat.getString("C.countryName"));
			districtList.add(district) ;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * Getter for the districtList
	 * @return the ArrayList which contains all the districts
	 * <!--  end-user-doc  -->
	 */
	public static ArrayList<district> getDistrictList() throws ClassNotFoundException, SQLException {
		loadDistrictList() ; // updating the list before returning it
		return districtList;
	}
}

package DBclasses;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import MODELclasses.info;

public class infosDB 
{
	private String militaryId ;
	private ArrayList<info> infos ;
	public infosDB(String militaryId) 
	{
		this.militaryId = militaryId ;
	}

	public String getNames() throws ClassNotFoundException, SQLException
	{
		PreparedStatement infosStatement = database.connection().
				prepareStatement("SELECT firstName, surName " +
						"FROM member " + 
						"WHERE militaryId = ?") ;
		infosStatement.setString(1, this.militaryId);
		ResultSet resultInfos = infosStatement.executeQuery() ;
		resultInfos.next() ;
		return (resultInfos.getString("firstName").toUpperCase() 
				+ " " + 
				resultInfos.getString("surName")) ;
	}

	public void loadInfos() throws ClassNotFoundException, SQLException
	{
		infos = new ArrayList<info> () ;
		PreparedStatement infosStatement = database.connection().
				prepareStatement("SELECT me.firstName, me.surName, c.cityName, m.* FROM mission m " + 
						"INNER JOIN city c ON c.cityId = m.cityId " + 
						"INNER JOIN mission_has_member mhm ON mhm.missionId = m.missionId " + 
						"INNER JOIN member me ON me.militaryId = mhm.militaryId " + 
						"WHERE me.militaryId = ?") ;
		infosStatement.setString(1, this.militaryId);
		ResultSet resultInfos = infosStatement.executeQuery() ;
		while(resultInfos.next())
		{
			infos.add(new info(resultInfos.getString("firstName"), resultInfos.getString("surName"), 
					resultInfos.getString("cityName"),	resultInfos.getInt("missionId"), 
					resultInfos.getString("missionName"), resultInfos.getString("missionDescription"), 
					resultInfos.getString("missionPlaces"), resultInfos.getString("missionStatus"),
					resultInfos.getString("creationDate"))) ;
		}	
	}

	public ArrayList<info> getInfos() throws ClassNotFoundException, SQLException 
	{
		this.loadInfos() ;
		return infos;
	}
}
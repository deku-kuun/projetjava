package DBclasses;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import MODELclasses.country;

public class countryDB 
{
	/**
	 * <!-- begin-user-doc -->
	 * This ArrayList contains all the ranks in the database 
	 * <!--  end-user-doc  --> 
	 */ 
	public static ArrayList<country> countryList ;

	/**
	 * <!-- begin-user-doc -->
	 * Method for inserting the country into the database if not in
	 * @param countryToAdd
	 * @return true if added, else false  
	 * <!--  end-user-doc  -->
	 */
	public boolean addcountry(country countryToAdd) throws ClassNotFoundException, SQLException
	{
		boolean countryAdded = false ;
		// test if the country already exists in the database
		if(!countryExists(countryToAdd)) 
		{
			// prepared request to add a new country
			PreparedStatement statementInsert = database.connection().prepareStatement(
					"INSERT INTO country (countryCode, countryName) VALUES (?,?)"); 
			statementInsert.setString(1, countryToAdd.getCountryCode()); 
			statementInsert.setString(2, countryToAdd.getCountryName());
			// request's execution, if it worked <=> countryAdded = true	
			countryAdded = (statementInsert.executeUpdate()!=0) ; 	
		}
		return countryAdded ;
	}

	/**
	 * This method tests if a country exists in the database by seeking with its name
	 * @param countryToTest
	 * @return true if the country exists, else false
	 * <!--  end-user-doc  --> 
	 */
	public static boolean countryExists(country countryToTest) throws ClassNotFoundException, SQLException
	{
		boolean countryExists = false ; // boolean for the test
		PreparedStatement statementTest= 
				database.connection().prepareStatement("SELECT COUNT(*) AS occurences FROM country WHERE countryName LIKE ?") ; 
		statementTest.setString(1, countryToTest.getCountryName());         
		ResultSet result = statementTest.executeQuery() ;  
		result.next() ; 
		// this country exists <=> countryExists = true
		countryExists = (result.getInt("occurences")!=0) ; 
		return countryExists;
	}

	/**
	 * <!-- begin-user-doc -->
	 * This method fills the country's list with the database's datas
	 * <!--  end-user-doc  --> 
	 */
	public static void loadcountryList() throws ClassNotFoundException, SQLException {
		countryList  = new ArrayList<country> () ;
		Statement statementLoad = database.connection().createStatement() ;
		ResultSet results = statementLoad.executeQuery("SELECT * FROM country") ; 
		while(results.next()) 
		{
			// while there are some lignes in the table	
			country country = new country(results.getString("countryCode"), results.getString("countryName")) ;
			country.setCountryId(results.getInt("countryId"));
			country.setRegistrationDate(results.getString("registrationDate")) ;
			countryList.add(country) ;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * Getter for the country list
	 * @return the ArrayList which contains all the countries
	 * <!--  end-user-doc  -->
	 */
	public static ArrayList<country> getCountryList() throws ClassNotFoundException, SQLException {
		// updating the list before returning it
		loadcountryList() ; 
		return countryList ;
	}

}
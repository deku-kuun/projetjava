package DBclasses;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import MODELclasses.catVehicle;

public class catVehicleDB 
{
	/**
	 * <!-- begin-user-doc -->
	 * This ArrayList contains all the catVehicle in the database : 
	 * <!--  end-user-doc  --> 
	 */ 
	public static ArrayList<catVehicle> catVehicleList  ;

	/**
	 * <!-- begin-user-doc -->
	 * Method for inserting the vehicle's category into the database if not in
	 * @param catVehicleToAdd
	 * @return true if added, else false  
	 * <!--  end-user-doc  -->
	 */
	public boolean addCatVehicle(catVehicle catVehicleToAdd) throws ClassNotFoundException, SQLException
	{
		boolean catVehicleAdded = false ;
		// test if the catVehicle already exists in the database
		if(!catVehicleExists(catVehicleToAdd)) 
		{
			// prepared request to add a new vehicle category
			PreparedStatement statementInsert = database.connection().prepareStatement(
					"INSERT INTO catVehicle (catVehicleName) VALUES (?) "); 
			statementInsert.setString(1, catVehicleToAdd.getCatVehicleName()); 
			// request's execution, if it worked <=> catVehicleAdded = true	
			catVehicleAdded = (statementInsert.executeUpdate()!=0) ; 	
		}
		return catVehicleAdded ;
	}

	/**
	 * This method tests if a category of vehicle already exists in the database by seeking its name
	 * @param catVehicleToTest
	 * @return true if the category exists, else false
	 * <!--  end-user-doc  --> 
	 */
	public static boolean catVehicleExists(catVehicle catVehicleToTest) throws ClassNotFoundException, SQLException
	{
		boolean catVehicleExists = false ; // boolean for the test
		PreparedStatement statementTest= 
				database.connection().prepareStatement("SELECT COUNT(*) AS occurences FROM catVehicle WHERE catVehicleName LIKE ?") ; 
		statementTest.setString(1, catVehicleToTest.getCatVehicleName());         
		ResultSet result = statementTest.executeQuery() ;  
		result.next() ; 
		// this catVehicle exists <=> catVehicleExists = true
		catVehicleExists = (result.getInt("occurences")!=0) ; 
		return catVehicleExists;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * This method fills the catVehicle's list with the database's datas
	 * <!--  end-user-doc  --> 
	 */
	public static void loadCatVehicleList() throws ClassNotFoundException, SQLException {
		catVehicleList  = new ArrayList<catVehicle> () ;
		Statement statementLoad = database.connection().createStatement() ;
		ResultSet results = statementLoad.executeQuery("SELECT * FROM catVehicle") ;  
		while(results.next()) 
		{
			// while there are some lignes in the table	
			catVehicle catVehicle = new catVehicle(results.getString("catVehicleName")) ;
			catVehicle.setCatVehicleId(results.getInt("catVehicleId")) ; 
			catVehicle.setRegistrationDate(results.getString("registrationDate"));
			catVehicleList.add(catVehicle) ;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * Getter for the vehicle's category list
	 * @return the ArrayList which contains all the vehicle categories
	 * <!--  end-user-doc  -->
	 */
	public static ArrayList<catVehicle> getCatVehicleList() throws ClassNotFoundException, SQLException {
		// updating the list before returning it
		loadCatVehicleList() ; 
		return catVehicleList ;
	}
}


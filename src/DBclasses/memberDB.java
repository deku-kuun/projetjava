package DBclasses;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import MODELclasses.member;
import MODELclasses.soldier;

public class memberDB
{
	/**
	 * <!-- begin-user-doc -->
	 * This ArrayList contains all the members in the database 
	 * <!--  end-user-doc  -->
	 */
	public static ArrayList<member> memberList ;

	/**
	 * <!-- begin-user-doc -->
	 * This method add a member into the database if not already in, if the member is also a soldier he/she is added to the tables for soldiers 
	 * @param memberToAdd
	 * @return true if added, else false (an error could happen when inserting)
	 * <!--  end-user-doc  -->
	 */
	public boolean addMemberInDatabase(member memberToAdd) throws ClassNotFoundException, SQLException, NoSuchAlgorithmException 
	{
		boolean memberAdded = false  ;
		try
		{
			if(!militaryIdExists(memberToAdd) && !emailExists(memberToAdd))
			{
				// start TRANSACTION by removing the default autocommit 
				database.connection().setAutoCommit(false);
				PreparedStatement statementInsertMember= 
						database.connection().prepareStatement("INSERT INTO member(militaryId,password,email,surname,firstname,picName,rankCode) VALUES (?,?,?,?,?,?,?)") ;
				statementInsertMember.setString(1,memberToAdd.getMilitaryId());
				statementInsertMember.setString(2,hash(memberToAdd.getPassword()));
				statementInsertMember.setString(3,memberToAdd.getEmail());
				statementInsertMember.setString(4,memberToAdd.getSurname());
				statementInsertMember.setString(5,memberToAdd.getFirstname());
				statementInsertMember.setString(6,memberToAdd.getPicName());
				statementInsertMember.setString(7,getRankCode(memberToAdd));

				//If the member is a soldier, we insert him/her into the two following tables
				if(memberToAdd instanceof soldier)
				{
					boolean soldierAdded = false ;
					PreparedStatement statementInsertSoldier = database.connection().prepareStatement(
							"INSERT INTO member_has_typeSoldier(militaryId,typeSoldierId) VALUES (?,?) ");
					statementInsertSoldier.setString(1,((soldier) memberToAdd).getMilitaryId());
					statementInsertSoldier.setInt(2,getTypeSoldierId((soldier) memberToAdd));

					PreparedStatement statementInsertSoldier2 = database.connection().prepareStatement(
							"INSERT INTO squad_has_member(squadId,militaryId) VALUES (?,?) ");
					statementInsertSoldier2.setInt(1,getSquadId(((soldier) memberToAdd)));
					statementInsertSoldier2.setString(2,((soldier) memberToAdd).getMilitaryId());

					// soldierAdded = requests have been executed ?	
					soldierAdded = (statementInsertSoldier.executeUpdate()!=0 && (statementInsertSoldier2.executeUpdate()!=0)) ; 
					memberAdded = (statementInsertMember.executeUpdate()!=0) && soldierAdded ;
				}
				else
					// member has been added ?
					memberAdded =  (statementInsertMember.executeUpdate()!=0) ;
				// ending transaction block, commit changes AND setting the connection back to autocommit
				database.connection().commit();
				database.connection().setAutoCommit(true);
			}

		}
		catch(SQLException e)
		{
			if(!e.getMessage().contains("autocommit"))
				throw e ;	
		}
		return memberAdded ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * This method is used to test if a member already exists, in order to avoid duplications problems
	 * @param member to test military existence
	 * @return true if already exists, else false 
	 * <!--  end-user-doc  -->
	 */
	public static boolean militaryIdExists(member member) throws ClassNotFoundException, SQLException
	{
		boolean militaryIdExists = false ; 
		PreparedStatement statementTest= 
				database.connection().prepareStatement("SELECT COUNT(*) AS nb FROM member WHERE militaryId LIKE ?") ;     
		statementTest.setString(1, member.getMilitaryId());         
		ResultSet resultat = statementTest.executeQuery() ;  
		resultat.next() ;  
		// this member exists <=> militaryIdExists = true
		militaryIdExists = (resultat.getInt("nb")!=0) ; 			
		return militaryIdExists ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * This method is used to test if a email already exists, in order to avoid duplications problems
	 * @param member to test email existence
	 * @return true if already exists, else false 
	 * <!--  end-user-doc  -->
	 */
	public static boolean emailExists(member member) throws ClassNotFoundException, SQLException
	{
		boolean emailExists = false ;
		PreparedStatement statementTest= 
				database.connection().prepareStatement("SELECT COUNT(*) AS nb FROM member WHERE email LIKE ?") ;
		statementTest.setString(1, member.getEmail());         
		ResultSet resultat = statementTest.executeQuery() ;   
		resultat.next() ;
		// This email exists <=> emailExists = true
		emailExists = (resultat.getInt("nb")!=0) ; 
		return emailExists ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * This method is used to find the code of a rank with its name
	 * @param member of the one we want to find the rankName
	 * @return rankCode associated to the member
	 * <!--  end-user-doc  -->
	 */
	public static String getRankCode(member member) throws ClassNotFoundException, SQLException
	{
		PreparedStatement statementTest= 
				database.connection().prepareStatement("SELECT rankCode AS code FROM rank WHERE rankName LIKE ?") ; 
		statementTest.setString(1, member.getRankName()); 
		ResultSet resultat = statementTest.executeQuery() ;  
		resultat.next() ;
		return resultat.getString("code") ;       
	}

	/**
	 * <!-- begin-user-doc -->
	 * This method is used to find the id of a squad with its name
	 * @param soldier of the one we want to find the rankName
	 * @return squadId associated to the soldier
	 * <!--  end-user-doc  -->
	 */
	public static int getSquadId(soldier soldier) throws ClassNotFoundException, SQLException
	{
		PreparedStatement statementTest= 
				database.connection().prepareStatement("SELECT squadId AS id FROM squad WHERE squadName LIKE ?") ; 
		statementTest.setString(1, soldier.getSquadName()); 
		ResultSet resultat = statementTest.executeQuery() ;  
		resultat.next() ;
		return resultat.getInt("id") ; 
	}

	/**
	 * <!-- begin-user-doc -->
	 * This method is used to find the id of a typeSoldier with its name
	 * @param soldier of the one we want to find the typeSoldierId
	 * @return typeSoldierId associated to the soldier
	 * <!--  end-user-doc  -->
	 */
	public static int getTypeSoldierId(soldier soldier) throws ClassNotFoundException, SQLException
	{
		PreparedStatement statementTest= 
				database.connection().prepareStatement("SELECT typeSoldierId AS id FROM typeSoldier WHERE typeSoldierName LIKE ?") ; 
		statementTest.setString(1, soldier.getTypeSoldierName()); 
		ResultSet resultat = statementTest.executeQuery() ;  
		resultat.next() ;
		return resultat.getInt("id") ; 
	}

	/**
	 * <!-- begin-user-doc -->
	 * This method updates the member's list with the database datas
	 * <!--  end-user-doc  -->
	 */
	public static void loadMemberList() throws ClassNotFoundException, SQLException 
	{
		memberList = new ArrayList <member> ();
		Statement statementLoad = database.connection().createStatement() ; 
		ResultSet results = statementLoad.executeQuery("SELECT M.*, R.rankName FROM member M INNER JOIN rank R on R.rankCode = M.rankCode") ; 
		while(results.next())	
		{

			member member = new member(results.getString("M.militaryId"),null, results.getString("M.email"), 
					results.getString("M.firstName"), results.getString("M.surname"), results.getString("M.picName")) ;
			member.setRankName(results.getString("R.rankName"));
			member.setRegistrationDate(results.getString("M.registrationDate"));
			memberList.add(member) ;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * This method is used to crypt a password before adding it into the database
	 * @param text to hash (sha-256)
	 * @return crypted text in byte array format (utf-8)
	 * <!-- end-user-doc -->
	 */
	public static String hash(String text) throws NoSuchAlgorithmException 
	{ 
		// Set the hashage process to SHA-256
		MessageDigest digest = MessageDigest.getInstance("SHA-256"); 
		// Return the coded text (byte array format) crypted in SHA-256
		return toHexString(digest.digest(text.getBytes(StandardCharsets.UTF_8)));  
	}

	/**
	 * <!-- begin-user-doc -->
	 * This method converts a byte array format into a java string format (it works with the hash method)
	 * @param byte array to convert
	 * @return text converted
	 * <!-- end-user-doc -->
	 */
	public static String toHexString(byte[] hash) 
	{ 
		// Convert byte array into signum representation
		BigInteger number = new BigInteger(1, hash);  
		// Convert text into hex value 
		StringBuilder hexString = new StringBuilder(number.toString(16));   
		while (hexString.length() < 32)   
		{  
			// Pad with leading zeros
			hexString.insert(0, '0');  
		}  
		return hexString.toString();  
	} 

	/**
	 * <!-- begin-user-doc -->
	 * Getter for the member list
	 * @return the ArrayList which contains all the members
	 * <!--  end-user-doc  -->
	 */
	public static ArrayList<member> getMemberList() throws ClassNotFoundException, SQLException {
		// updating the list before returning it
		loadMemberList() ; 
		return memberList;
	}
}

package DBclasses;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import MODELclasses.city;

public class cityDB 
{
	/**
	 * <!-- begin-user-doc -->
	 * This ArrayList contains all the cities in the database 
	 * <!--  end-user-doc  -->
	 */
	public static ArrayList<city> cityList ;

	/**
	 * <!-- begin-user-doc -->
	 * This method add a city into the database if not already in
	 * @param cityToAdd
	 * @return true if added, else false (an error could happen when inserting)
	 * <!--  end-user-doc  -->
	 */
	public boolean addCityInDatabase(city cityToAdd) throws ClassNotFoundException, SQLException 
	{
		boolean cityAdded = false ;
		if(!cityExists(cityToAdd))  
		{
			PreparedStatement statementInsert = database.connection().prepareStatement(
					"INSERT INTO city(postCodeCity,cityName,districtCode) VALUES (?,?,?)"); // prepared request to add a new city
			statementInsert.setString(1,cityToAdd.getCityPostCode()); 	
			statementInsert.setString(2,cityToAdd.getCityName());
			statementInsert.setString(3,getDistrictCode(cityToAdd));
			// updated ?	
			cityAdded = (statementInsert.executeUpdate()!=0) ; 
		}
		return cityAdded ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * This method test if a city already exists in the database (postCode+districtCode), in order to avoid duplication problems
	 * @param cityToTest
	 * @return true if this city exists, else false
	 * <!--  end-user-doc  -->
	 */
	public static boolean cityExists(city cityToTest) throws ClassNotFoundException, SQLException
	{
		boolean cityExists = false ; 
		PreparedStatement statementCityExists= 
				database.connection().prepareStatement("SELECT COUNT(*) AS occurences FROM city WHERE postCodeCity LIKE ? AND districtCode LIKE ?") ; 
		statementCityExists.setString(1, cityToTest.getCityPostCode());
		statementCityExists.setString(2, getDistrictCode(cityToTest));   
		ResultSet result = statementCityExists.executeQuery() ;  
		result.next() ;  
		// this city exists <=> cityExists = true 
		cityExists = (result.getInt("occurences")!=0) ; 
		return cityExists; 
	}

	/**
	 * <!-- begin-user-doc -->
	 * This method is used to convert the districtName into a districtCode before inserting the city in the database
	 * @param discrict to get the districtCode with its name
	 * @return districtCode
	 * <!--  end-user-doc  -->
	 */
	public static String getDistrictCode(city city) throws ClassNotFoundException, SQLException
	{
		PreparedStatement statementGetCode= 
				database.connection().prepareStatement("SELECT districtCode AS code FROM district WHERE districtName LIKE ?") ;   
		statementGetCode.setString(1, city.getDistrictName()); 
		ResultSet result = statementGetCode.executeQuery() ; 
		result.next() ;  
		// return the districtCode associated to the district
		return result.getString("code") ; 
	}

	/**
	 * <!-- begin-user-doc --> 
	 * This method fill the cities list with the database's datas
	 * <!--  end-user-doc  --> 
	 */
	public static void loadCityList() throws ClassNotFoundException, SQLException { 
		cityList = new ArrayList<city> ();
		Statement statementLoad = database.connection().createStatement() ;
		ResultSet resultat = statementLoad.executeQuery("SELECT C.cityId, C.postCodeCity, C.cityName, "
				+ "C.registrationDate, D.districtName FROM city C "
				+ "INNER JOIN district D ON D.districtCode= C.districtCode") ;
		while(resultat.next())	
		{
			city city = new city(resultat.getString("C.postCodeCity"), resultat.getString("C.cityName")) ;
			city.setCityId(resultat.getInt("C.cityId"));
			city.setDistrictName(resultat.getString("D.districtName"));
			city.setRegistrationDate(resultat.getString("C.registrationDate"));
			cityList.add(city) ;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * Getter for the cityList
	 * @return the ArrayList which contains the cities
	 * <!--  end-user-doc  -->
	 */
	public static ArrayList<city> getCityList() throws ClassNotFoundException, SQLException {
		loadCityList() ; // updating the list before returning it
		return cityList;
	}
}

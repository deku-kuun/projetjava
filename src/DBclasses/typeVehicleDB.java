package DBclasses;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import MODELclasses.typeVehicle;

public class typeVehicleDB 
{
	/**
	 * <!-- begin-user-doc -->
	 * This ArrayList contains all the different types of vehicle in the database 
	 * 	 * <!--  end-user-doc  --> 
	 */
	public static  ArrayList<typeVehicle> typeVehicleList ;

	/**
	 * <!-- begin-user-doc -->
	 * Method for inserting the vehicle type into the database if not in
	 * @return true if added, else false  
	 * <!--  end-user-doc  -->
	 */
	public boolean addTypeVehicle(typeVehicle typeVehicleToAdd) throws ClassNotFoundException, SQLException
	{
		boolean typeVehicleAdded = false ;
		// test if the typeVehicle already exists in the database
		if(!typeVehicleExists(typeVehicleToAdd)) 
		{ 
			// prepared request to add a new typeVehicle
			PreparedStatement statementInsert = database.connection().prepareStatement(
					"INSERT INTO typeVehicle(typeVehicleName,typeVehicleDescription,typeVehicleMaxSpeed,typeVehiclePlaces,typeVehiclePic,catVehicleId) VALUES (?,?,?,?,?,?) "); 
			statementInsert.setString(1, typeVehicleToAdd.getTypeVehicleName()); 
			statementInsert.setString(2, typeVehicleToAdd.getTypeVehicleDescription()); 
			statementInsert.setString(3, typeVehicleToAdd.getTypeVehicleMaxSpeed());	
			statementInsert.setInt(4, typeVehicleToAdd.getTypeVehiclePlaces()); 	
			statementInsert.setString(5, typeVehicleToAdd.getTypeVehiclePic()); 	
			statementInsert.setInt(6, getCatVehicleId(typeVehicleToAdd));
			// request's execution, if it worked <=> typeVehicleAdded = true	
			typeVehicleAdded = (statementInsert.executeUpdate()!=0) ; 	
		}
		return typeVehicleAdded ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * This method is used to convert the catVehicleName into a catVehicleId before inserting the vehicle type in the database
	 * @param typeVehicle to get the catVehicleId with its catVehicleName
	 * @return catVehicleId
	 */
	public static int getCatVehicleId(typeVehicle typeVehicle) throws ClassNotFoundException, SQLException
	{
		PreparedStatement statementGetId= 
				database.connection().prepareStatement("SELECT catVehicleId AS id FROM catVehicle WHERE catVehicleName LIKE ?") ;   
		statementGetId.setString(1, typeVehicle.getCatVehicleName()); 
		ResultSet result = statementGetId.executeQuery() ; 
		result.next() ;  
		// return the catVehicleId associated to the typeVehicle
		return result.getInt("id") ; 
	}

	/**
	 * <!-- begin-user-doc -->
	 * This method is used to test if a typeVehicle already exists (by seeking its name), in order to avoid duplications problems
	 * @param typeVehicleToTest
	 * @return true if the typeVehicle already exists, else false
	 * <!--  end-user-doc  -->
	 */
	public static boolean typeVehicleExists(typeVehicle typeVehicleToTest) throws ClassNotFoundException, SQLException
	{
		boolean typeVehicleExists = false ; // boolean for the test
		PreparedStatement statementTest= 
				database.connection().prepareStatement("SELECT COUNT(*) AS occurences FROM typeVehicle WHERE typeVehicleName LIKE ?") ;
		statementTest.setString(1, typeVehicleToTest.getTypeVehicleName());        
		ResultSet resultat = statementTest.executeQuery() ;  
		resultat.next() ;
		// this typeVehicle exists <=> typeVehicleExists = true
		typeVehicleExists = (resultat.getInt("occurences")!=0) ; 
		return typeVehicleExists; 
	}

	/**
	 * <!-- begin-user-doc -->
	 * This method fills the vehicle type list with the database's datas
	 * <!--  end-user-doc  --> 
	 */
	public static void loadTypeVehicleList() throws ClassNotFoundException, SQLException {
		typeVehicleList  = new ArrayList<typeVehicle> () ;
		Statement statementLoad = database.connection().createStatement() ;
		ResultSet results = statementLoad.executeQuery("SELECT T.*, C.catVehicleName FROM typeVehicle T "
				+ "INNER JOIN catVehicle C ON T.catVehicleId = C.catVehicleId") ; 
		while(results.next()) 
		{
			// while there are some lignes in the table
			typeVehicle typeVehicle= new typeVehicle (results.getString("T.typeVehicleName"), results.getString("T.typeVehicleDescription"),
					results.getString("T.typeVehicleMaxSpeed"), results.getInt("T.typeVehiclePlaces"), results.getString("T.typeVehiclePic")) ;
			typeVehicle.setTypeVehicleId(results.getInt("T.typeVehicleId"));
			typeVehicle.setRegistrationDate(results.getString("T.registrationDate"));
			typeVehicle.setCatVehicleName(results.getString("C.catVehicleName"));
			typeVehicleList.add(typeVehicle) ;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * Getter for the vehicle type list
	 * @return the ArrayList which contains all the vehicle types
	 * <!--  end-user-doc  -->
	 */
	public static ArrayList<typeVehicle> getTypeVehicleList() throws ClassNotFoundException, SQLException {
		// updating the list before returning it
		loadTypeVehicleList() ; 
		return typeVehicleList ;
	}	
}

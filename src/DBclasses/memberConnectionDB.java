package DBclasses;

import java.security.NoSuchAlgorithmException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class memberConnectionDB 
{
	private String militaryId ;
	private String password ;

	/**
	 * <!-- begin-user-doc -->
	 * Constructor for the objet memberConnectionDB, the objet is created when a user wants to have access to the application
	 * @param militaryId 
	 * @param password
	 * <!-- end-user-doc -->
	 */
	public memberConnectionDB(String militaryId, String password) 
	{
		this.militaryId = militaryId ;
		this.password = password ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * This method is going to look in the database if the password and id given are associated to the same member
	 * @return true is the password and the militaryId are the good ones
	 * <!-- end-user-doc -->
	 */
	public boolean testInformations() throws SQLException, NoSuchAlgorithmException, ClassNotFoundException
	{
		PreparedStatement testInformations = database.connection().
				prepareStatement("SELECT COUNT(*) as nb, password FROM member WHERE militaryId LIKE ?") ;
		testInformations.setString(1, this.militaryId);
		ResultSet resultTest = testInformations.executeQuery() ;
		resultTest.next() ;
		if(resultTest.getInt("nb")==0) 
			return false ;
		else
		{
			if(resultTest.getString("password").compareTo(memberDB.hash(password))==0)
				return true ;
			else
				return false ;
		}
	}
}
package Controller;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Random;

import INTERFACEclasses.* ;
import MODELclasses.* ;
import DBclasses.*;
import javafx.application.Application;
import javafx.stage.Stage; 

public class controller extends Application
{
	private String militaryId = "" ;
	public static void addMission(String missionName, String missionDescription,
			String cityName, ArrayList<member> members, ArrayList<weapon> weapons,
			ArrayList<vehicle> vehicles, controller ctrl, Stage primaryStage)
	{
		try
		{
			mission missionToAdd = new mission(missionName, missionDescription, ctrl.getMilitaryId()) ;
			missionToAdd.setMissionCityLocation(cityName);
			for(member m : members)
				missionToAdd.addMember(m.getMilitaryId()) ;
			for(weapon w : weapons)
				missionToAdd.addWeapon(w.getWeaponSN()) ;
			for(vehicle v : vehicles)
				missionToAdd.addVehicle(v.getVehicleLP()) ;	

			missionDB database = new missionDB() ;
			if(database.addMissionInDatabase(missionToAdd))
			{
				new AlertInterface("sucess adding !") ;
				addMissionInterface fen = new addMissionInterface(ctrl);
				fen.start(primaryStage);
			}
			else
				throw new Exception("Error when adding mission..This mission might already exists"
						+ "\n Please check its informations") ;
		}
		catch(Exception e)
		{
			new AlertInterface(e) ;
		}
	}

	public static void addMember(String militaryId, String password, String email ,
			String firstname, String surname ,String picName ,String rankName) 
	{
		try
		{
			member memberToAdd = new member(militaryId,password, email, firstname, surname, picName) ;
			memberToAdd.setRankName(rankName);
			memberDB database = new memberDB() ;
			if(database.addMemberInDatabase(memberToAdd)) 
				new AlertInterface("sucess adding !") ;
			else
				throw new Exception("Error when adding member.. This member might be already in the army"
						+ "\n Please check his/her informations") ;
		}
		catch(Exception e)
		{
			new AlertInterface(e) ;
		}
	}

	public static void addSoldier(String militaryId, String password, String email, String firstname, 
			String surname, String picName, String squadName, String typeSoldierName) 
	{
		try
		{
			soldier soldier = new soldier(militaryId, password, email, email, surname, picName) ;
			soldier.setTypeSoldierName(typeSoldierName); soldier.setSquadName(squadName);
			memberDB database = new memberDB() ;
			if(database.addMemberInDatabase(soldier)) 
				new AlertInterface("sucess adding !") ;
			else
				throw new Exception("Error when adding soldier.. This soldier might be already in the army"
						+ "\n Please check his/her informations") ;
		}
		catch(Exception e)
		{
			new AlertInterface(e) ;
		}
	}

	public static void addCatVehicle(String catVehicleName) 
	{
		try 
		{
			catVehicle newCatVehicle = new catVehicle(catVehicleName);
			catVehicleDB database = new catVehicleDB() ;
			if(database.addCatVehicle(newCatVehicle))
				new AlertInterface("sucess adding") ;
			else
				throw new Exception("Error when adding vehicule category.. This category might already exists"
						+ "\nPlease check its name") ;
		} 
		catch (Exception e) 
		{
			new AlertInterface(e) ;
		}
	}

	public static void addCatWeapon(String catWeaponName) 
	{
		try 
		{
			catWeapon newCatWeapon = new catWeapon(catWeaponName);
			catWeaponDB database = new catWeaponDB() ;
			if(database.addCatWeapon(newCatWeapon))
				new AlertInterface("sucess adding") ;
			else
				throw new Exception("Error when adding weapon category.. This category might already exists"
						+ "\nPlease check its name") ;
		} 
		catch (Exception e) 
		{
			new AlertInterface(e) ;
		}
	}

	public static void addCity(String cityPostCode, String cityName, String districtName) 
	{
		try
		{
			if(districtName.trim().compareTo("")==0)
				throw new Exception("You did not select a district") ;
			else
			{
				city cityToAdd = new city(cityPostCode, cityName) ;
				cityToAdd.setDistrictName(districtName);
				cityDB database = new cityDB() ;
				if(database.addCityInDatabase(cityToAdd))
					new AlertInterface("sucess adding") ;
				else
					throw new Exception("Error when adding city.. It might already exists"
							+ "\n Please check its name") ;	
			}
		}
		catch(Exception e)
		{
			new AlertInterface(e) ;
		}	
	}

	public static void addCountry(String countryCode, String countryName) 
	{
		try 
		{
			country newCountry = new country(countryCode,countryName);
			countryDB database = new countryDB() ;
			if(database.addcountry(newCountry))
				new AlertInterface("sucess adding !") ;
			else
				throw new Exception("Error when adding country.. It might already exists"
						+ "\n Please check its name") ;
		} 
		catch (Exception e) 
		{
			new AlertInterface(e) ;
		}
	}

	public static void addDistrict( String districtCode,  String districtName, String countryName) 
	{
		try
		{
			if(countryName.compareTo("")==0)
				throw new Exception("You did not select a country") ;
			else
			{
				district districtToAdd = new district(districtCode, districtName) ;
				districtToAdd.setCountryName(countryName);
				districtDB database = new districtDB() ;
				if(database.adddistrictInDatabase(districtToAdd))
					new AlertInterface("sucess adding !") ;
				else
					throw new Exception("Error when adding district.. It might already exists"
							+ "\n Please check its name") ;
			} 
		}
		catch (Exception e) 
		{
			new AlertInterface(e) ;
		}
	}

	public static void addRank(String rankCode, String rankName)
	{
		try
		{
			rank rankToAdd = new rank(rankName, rankCode) ;
			rankDB database = new rankDB() ;
			if(database.addRank(rankToAdd)) 
				new AlertInterface("sucess adding !") ;
			else
				throw new Exception("Error when adding rank.. This rank might already exists"
						+ "\n Please check its informations") ;	
		}
		catch(Exception e)
		{
			new AlertInterface(e) ;
		}
	}

	public static void addTypeWeapon( String typeWeaponName, String typeWeaponDescription, 
			String typeWeaponRange, String typeWeaponWeight, 
			String typeWeaponPic, String catWeaponName)
	{
		try
		{
			typeWeapon typeWeaponToAdd = new typeWeapon( typeWeaponName,  typeWeaponDescription,  typeWeaponRange,  typeWeaponWeight,  typeWeaponPic) ;
			typeWeaponToAdd.setCatWeaponName(catWeaponName);
			typeWeaponDB database = new typeWeaponDB() ;
			if(database.addTypeWeapon(typeWeaponToAdd)) 
				new AlertInterface("sucess adding !") ;
			else
				throw new Exception("Error when adding this weapon model.. This model might already exists"
						+ "\n Please check its informations") ;	
		}
		catch(Exception e)
		{
			new AlertInterface(e) ;
		}
	}	

	public static void addWeapon(String weaponSN, String typeWeaponName)
	{
		try
		{
			weapon weaponToAdd = new weapon(weaponSN) ;
			weaponToAdd.setTypeWeaponName(typeWeaponName);
			weaponDB database = new weaponDB() ;
			if(database.addWeaponInDatabase(weaponToAdd)) 
				new AlertInterface("sucess adding !") ;
			else
				throw new Exception("Error when adding this weapon.. This weapon might already exists"
						+ "\n Please check its informations (maybe the serial number)") ;
		}
		catch(Exception e)
		{
			new AlertInterface(e) ;
		}
	}

	public static void addTypeVehicle(String typeVehicleName,  String typeVehicleDescription,
			String typeVehicleMaxSpeed, int typeVehiclePlaces, 
			String typeVehiclePic, String catVehicleName)
	{
		try
		{
			typeVehicle typeVehicleToAdd = new typeVehicle(typeVehicleName,typeVehicleDescription,
					typeVehicleMaxSpeed,typeVehiclePlaces,typeVehiclePic) ;
			typeVehicleToAdd.setCatVehicleName(catVehicleName);
			typeVehicleDB database = new typeVehicleDB() ;
			if(database.addTypeVehicle(typeVehicleToAdd)) 
				new AlertInterface("sucess adding !") ;
			else
				throw new Exception("Error when adding this vehicle model.. This model might might already exists"
						+ "\n Please check its informations") ;	
		}
		catch(Exception e)
		{
			new AlertInterface(e) ;
		}

	}

	public static void addVehicle(String vehicleLP, String typeVehicleName)
	{
		try
		{
			vehicle vehicleToAdd = new vehicle(vehicleLP) ;
			vehicleToAdd.setTypeVehicleName(typeVehicleName);
			vehicleDB database = new vehicleDB() ;
			if(database.addVehicleInDatabase(vehicleToAdd))
				new AlertInterface("sucess adding !") ;
			else
				throw new Exception("Error when adding this vehicle.. This vehicle might already exists"
						+ "\n Please check its informations (maybe the licence plate)") ;
		}
		catch(Exception e)
		{
			new AlertInterface(e) ;
		}
	}

	public static void startMission(mission missionToStart)
	{
		try
		{ 
			if(missionToStart.getMissionStatus().compareTo("Mission started")==0)
				throw new Exception("this mission has already begun, impossible to start it again") ;
			else if(missionToStart.getMissionStatus().compareTo("Mission ended")==0)
				throw new Exception("this mission is over, impossible to start it") ;
			else
			{
				missionToStart.beginMission() ;
				missionDB database = new missionDB() ;
				if(database.beginMission(missionToStart))
					new AlertInterface("this mission has started !") ;
			}
		}
		catch(Exception e)
		{
			new AlertInterface(e) ;
		}
	}

	public static void endMission(mission missionToStart)
	{
		try
		{ 
			if(missionToStart.getMissionStatus().compareTo("Mission ended")==0)
				throw new Exception("this mission is over, impossible to end it again") ;
			else if(missionToStart.getMissionStatus().compareTo("Mission not begin yet")==0)
				throw new Exception("this mission has not been started yet, impossible to end it") ;
			else
			{
				missionToStart.endMission() ;
				missionDB database = new missionDB() ;
				if(database.endMission(missionToStart))
					new AlertInterface("this mission is over !") ;
			}
		}
		catch(Exception e)
		{
			new AlertInterface(e) ;
		}
	}

	public ArrayList <info> getInfos()
	{
		infosDB database = new infosDB(this.getMilitaryId()) ;
		try
		{
			return database.getInfos() ;
		}
		catch (Exception e)
		{
			new AlertInterface(e);
			return null;
		}
	}
	
	public String getNames()
	{
		infosDB database = new infosDB(this.getMilitaryId()) ;
		try
		{
			return database.getNames() ;
		}
		catch (Exception e)
		{
			new AlertInterface(e);
			return null;
		}
	}

	public void lancerConnection(Stage primaryStage) {
		connectionInterface fen = new connectionInterface(this);
		try
		{
			fen.start(primaryStage);
		} 
		catch (Exception e) 
		{
			new AlertInterface(e);
		}
	}

	public void selectCityForMission(Stage primaryStage, String cityName,
			ArrayList <member> members, ArrayList <vehicle> vehicles,
			ArrayList <weapon> weapons)
	{
		addMissionInterface fen = new addMissionInterface(this, cityName, members, vehicles, weapons);
		try
		{
			fen.start(primaryStage);
		} 
		catch (Exception e) 
		{
			new AlertInterface(e);
		}
	}

	public void selectWeaponForMission(Stage primaryStage, String cityName,
			ArrayList <member> members, ArrayList <vehicle> vehicles,
			ArrayList <weapon> weapons)
	{
		addMissionInterface fen = new addMissionInterface(this, cityName, members, vehicles, weapons);
		try
		{
			fen.start(primaryStage);
		} 
		catch (Exception e) 
		{
			new AlertInterface(e);
		}
	}

	public void selectVehicleForMission(Stage primaryStage, String cityName,
			ArrayList <member> members, ArrayList <vehicle> vehicles,
			ArrayList <weapon> weapons)
	{
		addMissionInterface fen = new addMissionInterface(this, cityName, members, vehicles, weapons);
		try
		{
			fen.start(primaryStage);
		} 
		catch (Exception e) 
		{
			new AlertInterface(e);
		}
	}

	public void selectMemberForMission(Stage primaryStage, String cityName,
			ArrayList <member> members, ArrayList <vehicle> vehicles,
			ArrayList <weapon> weapons)
	{
		addMissionInterface fen = new addMissionInterface(this, cityName, members, vehicles, weapons);
		try
		{
			fen.start(primaryStage);
		} 
		catch (Exception e) 
		{
			new AlertInterface(e);
		}
	}

	public void selectDistrictForCity(Stage primaryStage, String districtName)
	{
		addCityInterface fen = new addCityInterface(this, districtName) ;
		try
		{
			fen.start(primaryStage);
		} 
		catch (Exception e) 
		{
			new AlertInterface(e);
		}
	}

	public void selectTypeWeaponForWeapon(Stage primaryStage, String typeWeaponName)
	{
		addWeaponInterface fen = new addWeaponInterface(this, typeWeaponName) ;
		try
		{
			fen.start(primaryStage);
		} 
		catch (Exception e) 
		{
			new AlertInterface(e);
		}
	}
	
	public void selectCatWeaponForTypeWeapon(Stage primaryStage, String catWeaponName)
	{
		addTypeWeaponInterface fen = new addTypeWeaponInterface(this, catWeaponName) ;
		try
		{
			fen.start(primaryStage);
		} 
		catch (Exception e) 
		{
			new AlertInterface(e);
		}
	}
	
	public void selectCatVehicleForTypeVehicle(Stage primaryStage, String catVehicleName)
	{
		addTypeVehicleInterface fen = new addTypeVehicleInterface(this, catVehicleName) ;
		try
		{
			fen.start(primaryStage);
		} 
		catch (Exception e) 
		{
			new AlertInterface(e);
		}
	}

	public void lancerShowMission(Stage primaryStage) 
	{
		showMissionInterface fen = new showMissionInterface(this);
		try
		{
			fen.start(primaryStage);
		} 
		catch (Exception e) 
		{
			new AlertInterface(e);
		}
	}

	public void lancerAddMission(Stage primaryStage) {
		addMissionInterface fen = new addMissionInterface(this);
		try
		{
			fen.start(primaryStage);
		} 
		catch (Exception e) 
		{
			new AlertInterface(e);
		}
	}

	public void selectTypeVehicleForVehicle(Stage primaryStage, String typeVehicleName) {
		addVehicleInterface fen = new addVehicleInterface(this, typeVehicleName);
		try
		{
			fen.start(primaryStage);
		} 
		catch (Exception e) 
		{
			new AlertInterface(e);
		}
	}

	public void launchSelectTypeVehicleForVehicle(Stage primaryStage) {
		selectTypeVehicleForVehicleInterface fen = new selectTypeVehicleForVehicleInterface(this);
		try
		{
			fen.start(primaryStage);
		} 
		catch (Exception e) 
		{
			new AlertInterface(e);
		}
	}

	public void lancerAddVehicle(Stage primaryStage) {
		addVehicleInterface fen = new addVehicleInterface(this);
		try
		{
			fen.start(primaryStage);
		} 
		catch (Exception e) 
		{
			new AlertInterface(e);
		}
	}

	public void lancerAddDistrict(Stage primaryStage) {
		addDistrictInterface fen = new addDistrictInterface(this);
		try
		{
			fen.start(primaryStage);
		} 
		catch (Exception e) 
		{
			new AlertInterface(e);
		}
	}

	public void lancerShowCatVehicle(Stage primaryStage) {
		showCatVehicleInterface fen = new showCatVehicleInterface(this);
		try
		{
			fen.start(primaryStage);
		} 
		catch (Exception e) 
		{
			new AlertInterface(e);
		}
	}

	public void lancerShowTypeVehicle(Stage primaryStage) {
		showTypeVehicleInterface fen = new showTypeVehicleInterface(this);
		try
		{
			fen.start(primaryStage);
		} 
		catch (Exception e) 
		{
			new AlertInterface(e);
		}
	}

	public void lancerShowVehicle(Stage primaryStage) {
		showVehicleInterface fen = new showVehicleInterface(this);
		try
		{
			fen.start(primaryStage);
		} 
		catch (Exception e) 
		{
			new AlertInterface(e);
		}
	}

	public void lancerSelectCountryForDistrict(Stage primaryStage) {
		selectCountryForDistrictInterface fen = new selectCountryForDistrictInterface(this);
		try 
		{
			fen.start(primaryStage);
		} 
		catch (Exception e) 
		{
			new AlertInterface(e) ;
		}
	}

	public void lancerAddCountry(Stage primaryStage) 
	{
		addCountryInterface fen = new addCountryInterface(this);
		try
		{
			fen.start(primaryStage);
		} 
		catch (Exception e) 
		{
			new AlertInterface(e);
		}
	}

	public void selectCountryForDistrict(Stage primaryStage, String countryName)
	{
		addDistrictInterface fen = new addDistrictInterface(this, countryName);
		try 
		{
			fen.start(primaryStage);
		} 
		catch (Exception e) 
		{
			new AlertInterface(e) ;
		}
	}

	public void lancerShowMember(Stage primaryStage)
	{
		showMemberInterface fen = new showMemberInterface(this);
		try
		{
			fen.start(primaryStage);
		} 
		catch (Exception e) 
		{
			new AlertInterface(e);
		}
	}

	public void lancerAddMember(Stage primaryStage) {
		addMemberInterface fen = new addMemberInterface(this);
		try
		{
			fen.start(primaryStage);
		} 
		catch (Exception e) 
		{
			new AlertInterface(e);
		}
	}

	public void lancerMemberForMission(Stage primaryStage, String cityName,
			ArrayList <member> members, ArrayList <vehicle> vehicles,
			ArrayList <weapon> weapons) {
		selectMemberForMissionInterface fen = new selectMemberForMissionInterface(this,cityName, members, vehicles, weapons);
		try
		{
			fen.start(primaryStage);
		} 
		catch (Exception e) 
		{
			new AlertInterface(e);
		}
	}

	public void lancerSelectCityForMission(Stage primaryStage, String cityName,
			ArrayList <member> members, ArrayList <vehicle> vehicles,
			ArrayList <weapon> weapons) {
		selectCityForMissionInterface fen = new selectCityForMissionInterface(this,cityName, members, vehicles, weapons);
		try
		{
			fen.start(primaryStage);
		} 
		catch (Exception e) 
		{
			new AlertInterface(e);
		}
	}

	public void lancerVehicleForMission(Stage primaryStage, String cityName,
			ArrayList <member> members, ArrayList <vehicle> vehicles,
			ArrayList <weapon> weapons) {
		selectVehicleForMissionInterface fen = new selectVehicleForMissionInterface(this,cityName, members, vehicles, weapons);
		try
		{
			fen.start(primaryStage);
		} 
		catch (Exception e) 
		{
			new AlertInterface(e);
		}
	}

	public void lancerWeaponForMission(Stage primaryStage, String cityName,
			ArrayList <member> members, ArrayList <vehicle> vehicles,
			ArrayList <weapon> weapons) {
		selectWeaponForMissionInterface fen = new selectWeaponForMissionInterface(this,cityName, members, vehicles, weapons);
		try
		{
			fen.start(primaryStage);
		} 
		catch (Exception e) 
		{
			new AlertInterface(e);
		}
	}

	public void lancerSelectDistrictForCity(Stage primaryStage) {
		selectDistrictForCityInterface fen = new selectDistrictForCityInterface(this);
		try 
		{
			fen.start(primaryStage);
		} 
		catch (Exception e) 
		{
			new AlertInterface(e);
		}
	}	

	public void lancerAddCity(Stage primaryStage) {
		addCityInterface fen = new addCityInterface(this);
		try
		{
			fen.start(primaryStage);
		} 
		catch (Exception e) 
		{
			new AlertInterface(e);
		}
	}

	public void lancerShowTypeWeapon(Stage primaryStage) {
		showTypeWeapon fen = new showTypeWeapon(this);
		try
		{
			fen.start(primaryStage);
		} 
		catch (Exception e) 
		{
			new AlertInterface(e);
		}
	}

	public void lancerShowWeapon(Stage primaryStage) {
		showWeaponInterface fen = new showWeaponInterface(this);
		try
		{
			fen.start(primaryStage);
		} 
		catch (Exception e) 
		{
			new AlertInterface(e);
		}
	}

	public void lancerAddCatWeapon(Stage primaryStage) {
		addCatWeaponInterface fen = new addCatWeaponInterface(this);
		try
		{
			fen.start(primaryStage);
		} 
		catch (Exception e) 
		{
			new AlertInterface(e);
		}
	}

	public void lancerShowCatWeapon(Stage primaryStage) {
		showCatWeaponInterface fen = new showCatWeaponInterface(this);
		try
		{
			fen.start(primaryStage);
		} 
		catch (Exception e) 
		{
			new AlertInterface(e);
		}
	}
	
	public void lancerSelectCatWeaponForTypeWeapon(Stage primaryStage) 
	{
        selectCatWeaponForTypeWeapon fen = new selectCatWeaponForTypeWeapon(this);
        try
        {
            fen.start(primaryStage);
        } 
        catch (Exception e) 
        {
            new AlertInterface(e);
        }
    }
	
	public void lancerSelectCatVehicleForTypeVehicle(Stage primaryStage) 
	{
        selectCatVehicleForTypeVehicle fen = new selectCatVehicleForTypeVehicle(this);
        try
        {
            fen.start(primaryStage);
        } 
        catch (Exception e) 
        {
        	e.printStackTrace();
            new AlertInterface(e);
        }
    }

	public void lancerAccueil(Stage primaryStage) 
	{
		accueilInterface fen = new accueilInterface(this);
		try
		{
			fen.start(primaryStage);
		} 
		catch (Exception e) 
		{
			new AlertInterface(e);
		}
	}

	public void lancerShowCity(Stage primaryStage) 
	{
		showCityInterface fen = new showCityInterface(this);
		try
		{
			fen.start(primaryStage);
		} 
		catch (Exception e) 
		{
			new AlertInterface(e);
		}
	}

	public void lancerShowDistrict(Stage primaryStage) 
	{
		showDistrictInterface fen = new showDistrictInterface(this);
		try
		{
			fen.start(primaryStage);
		} 
		catch (Exception e) 
		{
			new AlertInterface(e);
		}
	}

	public void lancerShowCountry(Stage primaryStage) 
	{
		showCountryInterface fen = new showCountryInterface(this);
		try
		{
			fen.start(primaryStage);
		} 
		catch (Exception e) 
		{
			new AlertInterface(e);
		}
	}

	public void lancerSelectTypeSoldier(Stage primaryStage, String squadName) {
		selectTypeSoldierForSoldierInterface fen = new selectTypeSoldierForSoldierInterface(this, squadName);
		try
		{
			fen.start(primaryStage);
		} 
		catch (Exception e) 
		{
			new AlertInterface(e);
		}
	}

	public void lancerSelectSoldierSquad(Stage primaryStage, String typeSoldierName) {
		selectSoldierSquadInterface fen = new selectSoldierSquadInterface(this, typeSoldierName);
		try
		{
			fen.start(primaryStage);
		} 
		catch (Exception e) 
		{
			new AlertInterface(e);
		}
	}

	public void selectTypeSoldierOrSquad(Stage primaryStage, String rankName, String squadName, String typeSoldierName) {
		addMemberInterface fen = new addMemberInterface(this,rankName,squadName,typeSoldierName);
		try
		{
			fen.start(primaryStage);
		} 
		catch (Exception e) 
		{
			new AlertInterface(e);
		}
	}

	public void lancerAddCatVehicle(Stage primaryStage) {
		addCatVehiclesInterface fen = new addCatVehiclesInterface(this);
		try
		{
			fen.start(primaryStage);
		} 
		catch (Exception e) 
		{
			new AlertInterface(e);
		}
	}

	public void lancerAddWeapon(Stage primaryStage) {
		addWeaponInterface fen = new addWeaponInterface(this);
		try
		{
			fen.start(primaryStage);
		} 
		catch (Exception e) 
		{
			new AlertInterface(e);
		}
	}

	public void lancerTypeWeapon(Stage primaryStage) {
		addTypeWeaponInterface fen = new addTypeWeaponInterface(this);
		try
		{
			fen.start(primaryStage);
		} 
		catch (Exception e) 
		{
			new AlertInterface(e);
		}
	}

	public void lancerTypeVehicle(Stage primaryStage) {
		addTypeVehicleInterface fen = new addTypeVehicleInterface(this);
		try
		{
			fen.start(primaryStage);
		} 
		catch (Exception e) 
		{
			new AlertInterface(e);
		}
	}

	public void lancerSelectTypeWeaponForWeapon(Stage primaryStage) {
		selectTypeWeaponForWeapon fen = new selectTypeWeaponForWeapon(this);
		try
		{
			fen.start(primaryStage);
		} 
		catch (Exception e) 
		{
			new AlertInterface(e);
		}
	}

	public void lancerSelectMemberRank(Stage primaryStage) 
	{
		selectMemberRankInterface fen = new selectMemberRankInterface(this);
		try
		{
			fen.start(primaryStage);
		} 
		catch (Exception e) 
		{
			new AlertInterface(e);
		}
	}

	public void selectMemberRank(Stage primaryStage, String rankName) 
	{
		addMemberInterface fen = new addMemberInterface(this, rankName); 
		try
		{
			fen.start(primaryStage);
		} 
		catch (Exception e) 
		{
			new AlertInterface(e);
		}
	}

	public static String generatePassword() 
	{
		String pass = "" ;
		Random randomChar = new Random();
		String alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234556789&#{([-_!^�@)]=}*";
		int pos ;
		for(int i = 0; i <= 10; i++) 
		{
			pos = randomChar.nextInt(alphabet.length());
			pass += alphabet.charAt(pos) ;
		}
		return pass;
	}

	public void testConnection(Stage primaryStage, String militaryId, String password) 
	{
		try
		{
			memberConnectionDB database = new memberConnectionDB(militaryId,password) ;
			if(database.testInformations()) 
			{
				new AlertInterface("You are connected !") ;
				this.setMilitaryId(militaryId);
				accueilInterface fen = new accueilInterface(this);
				fen.start(primaryStage);	
			}
			else
				throw new Exception("Wrong id ou password") ;
		}
		catch(Exception e)
		{
			new AlertInterface(e) ;
		}
	}

	@Override
	public void start(Stage primaryStage) {
		connectionInterface fen = new connectionInterface(this);
		try
		{
			fen.start(primaryStage);
		} 
		catch (Exception e) 
		{
			new AlertInterface(e);
		}
	}

	public static void main(String[] args) throws ClassNotFoundException, SQLException
	{
		launch(args);
	}

	public String getMilitaryId()
	{
		return this.militaryId ;
	}

	public void setMilitaryId(String militaryId) 
	{
		this.militaryId = militaryId;
	}	
}

package DBclassesTest;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.SQLException;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import DBclasses.squadDB;
import MODELclasses.squad;

class squadDBTest {

	@Test
	void testAddSquad() throws ClassNotFoundException, SQLException {
		squad s = new squad("42 eme squad") ;
		squadDB sDB = new squadDB () ; 
		assertFalse(sDB.addSquad(s)) ;
	}

	@Test
	void testSquadExists() throws ClassNotFoundException, SQLException {
		squad s = new squad("42 eme squad") ;
		assertTrue(squadDB.squadExists(s)) ;
	}

	@Test
	void testGetSquadList() throws ClassNotFoundException, SQLException {

		ArrayList <squad> list = new ArrayList <squad> () ;
		squad s = new squad("42 eme squad") ;
		s.setSquadId(1); s.setCreationDate("2019-11-23 16:15:18.0");
		list.add(s) ;
		ArrayList <squad> test = squadDB.getSquadList() ;
		for(squad sq :  test)
			assertEquals(sq,list.get(test.indexOf(sq))) ;
	}

}

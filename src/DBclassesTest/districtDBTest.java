package DBclassesTest;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.SQLException;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import DBclasses.districtDB;
import MODELclasses.district;

class districtDBTest {

	@Test
	void testAdddistrictInDatabase() throws ClassNotFoundException, SQLException {
		district ds = new district("74", "Limousin") ;
		ds.setCountryName("France");
		districtDB dsDB = new districtDB() ;
		assertFalse(dsDB.adddistrictInDatabase(ds)) ;
	}

	@Test
	void testDistrictExists() throws ClassNotFoundException, SQLException 
	{
		district ds = new district("74", "Limousin") ;
		ds.setCountryName("France");
		assertTrue(districtDB.districtExists(ds)) ;
	}

	@Test
	void testGetCountryCode() throws ClassNotFoundException, SQLException 
	{
		district ds = new district("74", "Limousin") ;
		ds.setCountryName("France");
		assertEquals("FR",districtDB.getCountryCode(ds)) ;
	}

	@Test
	void testGetDistrictList() throws ClassNotFoundException, SQLException {
		ArrayList <district> list = new ArrayList <district> () ;

		district ds1 = new district("24", "Centre") ;
		ds1.setDistrictId(1); ds1.setCountryName("France"); ds1.setRegistrationDate("2019-11-12 16:15:50.0");

		district ds2 = new district("74", "Limousin") ;
		ds2.setDistrictId(2); ds2.setCountryName("France"); ds2.setRegistrationDate("2019-12-04 18:29:21.0");

		list.add(ds1) ; list.add(ds2) ; 
		ArrayList <district> test = districtDB.getDistrictList() ;
		for(district ds : test)
		{
			assertEquals(ds,list.get(test.indexOf(ds))) ;
		}
	}

}

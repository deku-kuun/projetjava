package DBclassesTest;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.SQLException;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import DBclasses.typeWeaponDB;
import MODELclasses.typeWeapon;

class typeWeaponDBTest {

	@Test
	void testAddTypeWeapon() throws ClassNotFoundException, SQLException {
		typeWeapon tw = new typeWeapon("M4", "tres puissont pour attaquer", "0.5 miles", "2", "pasdephpto.png") ;
		typeWeaponDB twDB = new typeWeaponDB() ;
		assertFalse(twDB.addTypeWeapon(tw)) ;
	}

	@Test
	void testGetCatWeaponId() throws ClassNotFoundException, SQLException {
		typeWeapon tw = new typeWeapon("M4", "tres puissont pour attaquer", "0.5 miles", "2", "pasdephpto.png") ;
		tw.setCatWeaponName("machine gun");
		assertEquals(2,typeWeaponDB.getCatWeaponId(tw)) ;
	}

	@Test
	void testTypeWeaponExists() throws ClassNotFoundException, SQLException {
		typeWeapon tw = new typeWeapon("M4", "tres puissont pour attaquer", "0.5 miles", "2", "pasdephpto.png") ;
		assertTrue(typeWeaponDB.typeWeaponExists(tw)) ;
	}

	@Test
	void testGetTypeWeaponList() throws ClassNotFoundException, SQLException {
		typeWeapon tw = new typeWeapon("M4", "tres puissont pour attaquer", "0.5 miles", "2", "pasdephpto.png") ;
		tw.setCatWeaponName("machine gun"); tw.setTypeWeaponId(5); tw.setRegistrationDate("2019-11-21 19:54:41.0");
		ArrayList <typeWeapon> list = new ArrayList <typeWeapon> () ;
		list.add(tw) ;
		ArrayList <typeWeapon> test = typeWeaponDB.getTypeWeaponList() ;
		for(typeWeapon t : test)
			assertEquals(t,list.get(test.indexOf(t))) ;			
	}

}

package DBclassesTest;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.SQLException;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import DBclasses.typeVehicleDB;
import MODELclasses.typeVehicle;

class typeVehicleDBTest {

	@Test
	void testAddTypeVehicle() throws ClassNotFoundException, SQLException {
		typeVehicle tv = new typeVehicle("test", "test123", "110 km/h", 5, "default.png") ;
		typeVehicleDB tvDB = new typeVehicleDB() ;
		assertFalse(tvDB.addTypeVehicle(tv)) ;
	}

	@Test
	void testGetCatVehicleId() throws ClassNotFoundException, SQLException {
		typeVehicle tv = new typeVehicle("test", "test123", "110 km/h", 5, "default.png") ;
		tv.setCatVehicleName("utility truck");
		assertEquals(2,typeVehicleDB.getCatVehicleId(tv));
	}

	@Test
	void testTypeVehicleExists() throws ClassNotFoundException, SQLException {
		typeVehicle tv = new typeVehicle("test", "test123", "110 km/h", 5, "default.png") ;
		assertTrue(typeVehicleDB.typeVehicleExists(tv));
	}

	@Test
	void testGetTypeVehicleList() throws ClassNotFoundException, SQLException {
		typeVehicle tv = new typeVehicle("test", "testtttt", "110 km/h", 5, "default.png") ;
		tv.setTypeVehicleId(1); tv.setRegistrationDate("2019-11-12 21:20:31.0");
		tv.setCatVehicleName("utility truck");
		
		ArrayList <typeVehicle> list = new ArrayList <typeVehicle> () ;
		list.add(tv) ;
		
		ArrayList <typeVehicle> test = typeVehicleDB.getTypeVehicleList() ;
		for(typeVehicle t : test)	
			assertEquals(t,list.get(test.indexOf(t))) ;
			
		
	}

}

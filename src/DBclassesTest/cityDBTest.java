package DBclassesTest;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.SQLException;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import DBclasses.cityDB;
import MODELclasses.city;

class cityDBTest {

	@Test
	void testAddCityInDatabase() throws ClassNotFoundException, SQLException {
		city ct = new city("37233", "Saint-Pierre-Des-Corps") ;
		ct.setDistrictName("Centre");
		cityDB ctDB = new cityDB() ;
		assertFalse(ctDB.addCityInDatabase(ct)) ;
	}
	
	

	@Test
	void testCityExists() throws ClassNotFoundException, SQLException {
		city ct = new city("37233", "Saint-Pierre-Des-Corps") ;
		ct.setDistrictName("Centre");
		assertTrue(cityDB.cityExists(ct)) ;
	}

	@Test
	void testGetDistrictCode() throws ClassNotFoundException, SQLException {
		city ct = new city("37233", "Saint-Pierre-Des-Corps") ;
		ct.setDistrictName("Centre");
		assertEquals("24",cityDB.getDistrictCode(ct)) ;
	}

	@Test
	void testGetCityList() throws ClassNotFoundException, SQLException {
		ArrayList <city> list = new ArrayList <city> () ;

		city ct1 = new city("37261", "tours") ;
		ct1.setCityId(1); ct1.setDistrictName("Centre"); ct1.setRegistrationDate("2019-11-27 13:23:59.0");

		city ct2 = new city("37233", "Saint-Pierre-Des-Corps") ;
		ct2.setCityId(2); ct2.setDistrictName("Centre"); ct2.setRegistrationDate("2019-12-04 17:41:38.0");

		list.add(ct1) ; list.add(ct2) ; 
		ArrayList <city> test = cityDB.getCityList() ;
		for(city ct : test)
		{
			assertEquals(ct,list.get(test.indexOf(ct))) ;
		}
	}

}

package DBclassesTest;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.SQLException;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import DBclasses.countryDB;
import MODELclasses.country;

class countryDBTest {

	@Test
	void testAddcountry() throws ClassNotFoundException, SQLException {
		country cnt = new country("DZ", "Alg�rie") ;
		countryDB cntDB = new countryDB() ;
		assertFalse(cntDB.addcountry(cnt)) ;
	}

	@Test
	void testCountryExists() throws ClassNotFoundException, SQLException {
		country cnt = new country("DZ", "Alg�rie") ;
		assertTrue(countryDB.countryExists(cnt)) ;
	}

	@Test
	void testGetCountryList() throws ClassNotFoundException, SQLException {
		ArrayList <country> list = new ArrayList <country> () ;
		country cnt1 = new country("FR","France") ;
		cnt1.setCountryId(1); cnt1.setRegistrationDate("2019-11-12 16:15:50.0");
		country cnt2 = new country("DZ","Alg�rie") ;
		cnt2.setCountryId(2); cnt2.setRegistrationDate("2019-12-04 18:17:06.0");
		list.add(cnt1) ; list.add(cnt2) ;
		
		ArrayList<country> test = countryDB.getCountryList() ;
		for(country cnt : test )
		{
			assertEquals(cnt,list.get(test.indexOf(cnt))) ;
		}
		
	}

}

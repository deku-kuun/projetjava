package DBclassesTest;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.SQLException;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import DBclasses.typeSoldierDB;
import MODELclasses.typeSoldier;

class typeSoldierDBTest {

	@Test
	void testAddTypeSoldier() throws ClassNotFoundException, SQLException {
		typeSoldier ts = new typeSoldier("Sniper") ;
		typeSoldierDB tsDB = new typeSoldierDB() ;
		assertFalse(tsDB.addTypeSoldier(ts)) ;
	}

	@Test
	void testTypeSoldierExists() throws ClassNotFoundException, SQLException {
		typeSoldier ts = new typeSoldier("Sniper") ;
		assertTrue(typeSoldierDB.typeSoldierExists(ts)) ;
	}

	@Test
	void testGetTypeSoldierList() throws ClassNotFoundException, SQLException {
		typeSoldier ts = new typeSoldier("Sniper") ;
		ts.setTypeSoldierId(1); ts.setCreationDate("2019-11-14 15:34:19.0");
		ArrayList <typeSoldier> list = new ArrayList <typeSoldier> () ;
		list.add(ts) ;
		ArrayList <typeSoldier> test = typeSoldierDB.getTypeSoldierList() ;
		for(typeSoldier t : test)
		{
			assertEquals(t,list.get(test.indexOf(t))) ;
		}
		
	}

}

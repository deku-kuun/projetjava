package DBclassesTest;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.SQLException;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import DBclasses.vehicleDB;
import MODELclasses.vehicle;

class vehicleDBTest {

	@Test
	void testAddVehicleInDatabase() throws ClassNotFoundException, SQLException {
		vehicle v = new vehicle ("LP-T3ST-93") ;
		vehicleDB vDB = new vehicleDB() ;
		assertFalse(vDB.addVehicleInDatabase(v)) ;
	}

	@Test
	void testVehicleExists() throws ClassNotFoundException, SQLException {
		vehicle v = new vehicle ("LP-T3ST-93") ;
		assertTrue(vehicleDB.vehicleExists(v)) ;
	}

	@Test
	void testGetTypeVehicleId() throws ClassNotFoundException, SQLException {
		vehicle v = new vehicle ("LP-T3ST-93") ;
		v.setTypeVehicleName("test");
		assertEquals(1,vehicleDB.getTypeVehicleId(v)) ;
	}

	@Test
	void testGetVehiclesList() throws ClassNotFoundException, SQLException {
		vehicle v = new vehicle("LP-T3ST-93") ;
		v.setTypeVehicleName("test"); v.setRegistrationDate("2019-11-22 22:17:17.0");
		ArrayList <vehicle> list = new ArrayList <vehicle> () ;
		list.add(v) ;
		ArrayList <vehicle> test = vehicleDB.getVehiclesList() ;
		for(vehicle ve : test)
			assertEquals(ve,list.get(test.indexOf(ve))) ;
	}

}

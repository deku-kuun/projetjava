package DBclassesTest;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.SQLException;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import DBclasses.catVehicleDB;
import MODELclasses.catVehicle;

class catVehicleDBTest {

	@Test
	void testAddCatVehicle() throws ClassNotFoundException, SQLException {
		catVehicle cV = new catVehicle("PVP") ;
		catVehicleDB cVDB = new catVehicleDB() ;
		assertFalse(cVDB.addCatVehicle(cV)) ;	
	}

	@Test
	void testCatVehicleExists() throws ClassNotFoundException, SQLException {
		catVehicle cV = new catVehicle("PVP") ;
		assertTrue(catVehicleDB.catVehicleExists(cV)) ;
	}

	@Test
	void testGetCatVehicleList() throws ClassNotFoundException, SQLException 
	{
		ArrayList<catVehicle> list = new ArrayList <catVehicle> () ;
		catVehicle cV1 = new catVehicle("utility truck") ;
		cV1.setCatVehicleId(2); cV1.setRegistrationDate("2019-11-22 22:17:43.0");
		catVehicle cV2 = new catVehicle("PVP") ;
		cV2.setCatVehicleId(3); cV2.setRegistrationDate("2019-12-04 16:24:13.0");
		list.add(cV1) ; list.add(cV2) ;
		ArrayList<catVehicle> test = catVehicleDB.getCatVehicleList() ;
		for(catVehicle cv : test )
		{
			assertEquals(cv,list.get(test.indexOf(cv))) ;
		}
	}

}

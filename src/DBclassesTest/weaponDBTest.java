package DBclassesTest;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.SQLException;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import DBclasses.weaponDB;
import MODELclasses.weapon;

class weaponDBTest {

	@Test
	void testAddWeaponInDatabase() throws ClassNotFoundException, SQLException {
		weapon w = new weapon ("SN12-414") ;
		weaponDB wDB = new weaponDB() ;
		assertFalse(wDB.addWeaponInDatabase(w)) ;
	}

	@Test
	void testWeaponExists() throws ClassNotFoundException, SQLException {
		weapon w = new weapon ("SN12-414") ;
		assertTrue(weaponDB.weaponExists(w)) ;
	}

	@Test
	void testGetTypeWeaponId() throws ClassNotFoundException, SQLException {
		weapon w = new weapon ("SN12-414") ;
		w.setTypeWeaponName("M4");
		assertEquals(5,weaponDB.getTypeWeaponId(w)) ;
	}

	@Test
	void testGetWeaponList() throws ClassNotFoundException, SQLException {
		weapon w = new weapon ("SN12-414") ;
		w.setTypeWeaponName("M4"); w.setRegistrationDate("2019-11-21 19:28:22.0");
		ArrayList <weapon> list = new ArrayList <weapon> () ;
		list.add(w) ;
		ArrayList <weapon> test = weaponDB.getWeaponList() ;
		for(weapon ve : test)
			assertEquals(ve,list.get(test.indexOf(ve))) ;
	}

}

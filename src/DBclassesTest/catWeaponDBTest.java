package DBclassesTest;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.SQLException;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import DBclasses.catWeaponDB;
import MODELclasses.catWeapon;


class catWeaponDBTest {

	@Test
	void testAddCatWeapon() throws ClassNotFoundException, SQLException {
		catWeapon cW = new catWeapon("pistol") ;
		catWeaponDB cWDB = new catWeaponDB() ;
		assertFalse(cWDB.addCatWeapon(cW)) ;
	}

	@Test
	void testCatWeaponExists() throws ClassNotFoundException, SQLException {
		catWeapon cW = new catWeapon("pistol") ;
		assertTrue(catWeaponDB.catWeaponExists(cW)) ;
	}

	@Test
	void testGetCatWeaponList() throws ClassNotFoundException, SQLException {
		ArrayList <catWeapon> list = new ArrayList <catWeapon> () ;
		catWeapon cW1 = new catWeapon("machine gun") ;
		cW1.setCatWeaponId(2); cW1.setRegistrationDate("2019-11-21 19:53:59.0");
		catWeapon cW2 = new catWeapon("pistol") ;
		cW2.setCatWeaponId(3); cW2.setRegistrationDate("2019-12-04 16:58:27.0");
		list.add(cW1); list.add(cW2);
		ArrayList <catWeapon> test = catWeaponDB.getCatWeaponList() ;
		for(catWeapon cw : test )
		{
			assertEquals(cw.getCatWeaponId(),list.get(test.indexOf(cw)).getCatWeaponId()) ;
			assertEquals(cw.getCatWeaponName(),list.get(test.indexOf(cw)).getCatWeaponName()) ;
			assertEquals(cw.getRegistrationDate(),list.get(test.indexOf(cw)).getRegistrationDate()) ;
		}

	}

}

package DBclassesTest;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.SQLException;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import DBclasses.rankDB;
import MODELclasses.rank;

class rankDBTest {

	@Test
	void testAddRank() throws ClassNotFoundException, SQLException {
		rank r = new rank("Soldier", "S") ;
		rankDB rDB = new rankDB() ;
		assertFalse(rDB.addRank(r)) ;
	}

	@Test
	void testRankExists() throws ClassNotFoundException, SQLException {
		rank r = new rank("Soldier", "S") ;
		assertTrue(rankDB.rankExists(r)) ;
	}

	@Test
	void testGetRankList() throws ClassNotFoundException, SQLException {
		
		ArrayList <rank> list = new ArrayList <rank> () ;
 		rank r1 = new rank("G�n�ral des arm�es","GA") ; r1.setRankId(1); rank r2 = new rank("G�n�ral","G") ; r2.setRankId(2);
		rank r3 = new rank("G�n�ral de division","GD") ; r3.setRankId(3); rank r4 = new rank("G�n�ral de brigade","GB") ; r4.setRankId(4);
		rank r5 = new rank("Colonel","Co") ; r5.setRankId(5); rank r6 = new rank("Lieutenant-Colonel","LC") ; r6.setRankId(6);
		rank r7 = new rank("Capitaine","Ca") ; r7.setRankId(7);  rank r8 = new rank("Lieutenant","L") ; r8.setRankId(8);
		rank r9 = new rank("Soldier","S") ; r9.setRankId(9); rank r10 = new rank("Admin","AD") ; r10.setRankId(10);
		rank r11 = new rank("G�n�ral de corps d'arm�e","GCA") ;  r11.setRankId(11);
		
		list.add(r1) ; list.add(r2) ; list.add(r3) ; list.add(r4) ; list.add(r5) ;
		list.add(r6) ; list.add(r7) ; list.add(r8) ; list.add(r9) ; list.add(r10) ;
		list.add(r11) ;
		
		ArrayList <rank> test = rankDB.getRankList() ;
		for(rank r :  test)
			assertEquals(r,list.get(test.indexOf(r))) ;
	}
	

}

package DBclassesTest;

import static org.junit.jupiter.api.Assertions.*;

import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import DBclasses.memberDB;
import MODELclasses.member;
import MODELclasses.soldier;

class memberDBTest {

	@Test
	void testAddMemberInDatabase() throws ClassNotFoundException, NoSuchAlgorithmException, SQLException {
		member mb = new member("HKLPMD37", "test123", "test@hotmail.fr", "aleppo", "abdulKader", "default.png") ;
		mb.setRankName("Capitaine");
		memberDB mbDB = new memberDB() ;
		assertFalse(mbDB.addMemberInDatabase(mb)) ;
	}

	@Test
	void testMilitaryIdExists() throws ClassNotFoundException, SQLException {
		member mb = new member("HKLPMD37", "test123", "test@hotmail.fr", "aleppo", "abdulKader", "default.png") ;
		assertTrue(memberDB.militaryIdExists(mb)) ;
	}

	@Test
	void testEmailExists() throws ClassNotFoundException, SQLException {
		member mb = new member("HKLPMD37", "test123", "test@hotmail.fr", "aleppo", "abdulKader", "default.png") ;
		assertTrue(memberDB.emailExists(mb)) ;
	}

	@Test
	void testGetRankCode() throws ClassNotFoundException, SQLException {
		member mb = new member("HKLPMD37", "test123", "test@hotmail.fr", "aleppo", "abdulKader", "default.png") ;
		mb.setRankName("Capitaine");
		assertEquals("Ca",memberDB.getRankCode(mb)) ;
	}

	@Test
	void testGetSquadId() throws ClassNotFoundException, SQLException {
		soldier sl = new soldier("KFP45DD", "test123", "test2@hotmail.fr", "aleppoDeux", "abdulKader", "default2.png") ;
		sl.setSquadName("42 eme squad"); 
		assertEquals(1,memberDB.getSquadId(sl)) ;
	}

	@Test
	void testGetTypeSoldierId() throws ClassNotFoundException, SQLException {
		soldier sl = new soldier("KFP45DD", "test123", "test2@hotmail.fr", "aleppoDeux", "abdulKader", "default2.png") ;
		sl.setTypeSoldierName("Sniper");
		assertEquals(1,memberDB.getTypeSoldierId(sl)) ;
	}

	@Test
	void testGetMemberList() throws ClassNotFoundException, SQLException {
		ArrayList <member> list = new ArrayList <member> () ;

		member m1 = new member("HKLPMD37", null, "test@hotmail.fr", "aleppo","abdulKader" , "default.png") ;
		m1.setRankName("Capitaine"); m1.setRegistrationDate("2019-12-05 13:00:09.0");	

		member m2 = new member("HK3791DZ", null, 
				"hakosTqt", "tqt", "hakos", "default.png") ;
		m2.setRankName("G�n�ral"); m2.setRegistrationDate("2019-11-30 15:46:37.0");

		list.add(m2) ; list.add(m1) ; 
		ArrayList <member> test = memberDB.getMemberList() ;
		for(member m : test)
			assertEquals(m,list.get(test.indexOf(m))) ;

	}

}

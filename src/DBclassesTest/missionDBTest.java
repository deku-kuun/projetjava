package DBclassesTest;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.SQLException;

import org.junit.jupiter.api.Test;

import DBclasses.missionDB;
import MODELclasses.mission;

class missionDBTest {

	@Test
	void testAddMissionInDatabase() throws ClassNotFoundException, SQLException {
		mission m = new mission("missionTest", "test ajout missionTest", "HK3791DZ") ;
		m.addMember("HKLPMD37") ; m.addVehicle("LP-T3ST-93") ;
		m.addWeapon("SN12-414") ; m.setMissionCityLocation("Tours");
		missionDB mDB = new missionDB() ;
		assertFalse(mDB.addMissionInDatabase(m)) ;
	}

	@Test
	void testMissionExists() throws ClassNotFoundException, SQLException {
		mission m = new mission("missionTest", "test ajout missionTest", "HK3791DZ") ;
		assertTrue(missionDB.missionExists(m)) ;
	}

	@Test
	void testGetMissionId() throws ClassNotFoundException, SQLException {
		mission m = new mission("missionTest", "test ajout missionTest", "HK3791DZ") ;
		assertEquals(8,missionDB.getMissionId(m)) ;
	}

	@Test
	void testGetCityId() throws ClassNotFoundException, SQLException {
		mission m = new mission("missionTest", "test ajout missionTest", "HK3791DZ") ;
		m.setMissionCityLocation("Tours");
		assertEquals(1,missionDB.getCityId(m)) ;
	}
	
	@Test
	void testEndMission() throws ClassNotFoundException, SQLException, InterruptedException {
		Thread.sleep(1);
		mission m = new mission("missionTest", "test ajout missionTest", "HK3791DZ") ;
		m.setMissionId(8);
		m.endMission() ;
		missionDB database = new missionDB() ;
		assertTrue(database.endMission(m)) ;
	}
	
	@Test
	void testBeginMission() throws ClassNotFoundException, SQLException {
		mission m = new mission("missionTest", "test ajout missionTest", "HK3791DZ") ;
		m.setMissionId(8);
		m.beginMission() ;
		missionDB database = new missionDB() ;
		assertTrue(database.beginMission(m)) ;
	}

}

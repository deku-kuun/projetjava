package MODELclasses;

public class rank 
{
	private int rankId ;
	
	private String rankName ;
	
	private String rankCode ;
	
	private String creationDate ;
	
	/**
	 * This constructor initializes a rank datas with the ones in parameter
	 * @param rankName
	 * @param rankCode example : 'soldier' = 'S', general='G', when there are many word, you must write the initials of each word 
	 */
	public rank(String rankName, String rankCode)
	{
		this.rankName = rankName ;
		this.rankCode = rankCode ;
	}

	// Getters
	public int getRankId() {
		return rankId;
	}
	
	public String getRankName() {
		return rankName;
	}

	public String getRankCode() {
		return rankCode;
	}

	
	public String getCreationDate() {
		return creationDate;
	}

	//Setters
	public void setRankId(int rankId) {
		this.rankId = rankId;
	}
	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		rank other = (rank) obj;
		if (rankCode == null) {
			if (other.rankCode != null)
				return false;
		} else if (!rankCode.equals(other.rankCode))
			return false;
		if (rankId != other.rankId)
			return false;
		if (rankName == null) {
			if (other.rankName != null)
				return false;
		} else if (!rankName.equals(other.rankName))
			return false;
		return true;
	}
}

package MODELclasses;

public class city 
{
	private int cityId ;
	private String cityPostCode;
	private String cityName;
	private String districtName;
	private String registrationDate ;

	/**
	 * The constructor initializes the city's datas in the ones in parameter
	 * @param cityPostCode it can be the Insee code or the code used in the country
	 * @param cityName 
	 */
	public city(String cityPostCode, String cityName) 
	{
		this.cityPostCode = cityPostCode;
		this.cityName = cityName;
	}

	//Getters
	public String getCityPostCode() {
		return cityPostCode;
	}

	public String getCityName() {
		return cityName;
	}

	public String getDistrictName() {
		return districtName;
	}

	public int getCityId() {
		return cityId;
	}

	public String getRegistrationDate() {
		return registrationDate;
	}

	//Setters
	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}

	public void setCityId(int cityId) {
		this.cityId = cityId;
	}
	
	public void setRegistrationDate(String registrationDate) {
		this.registrationDate = registrationDate;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		city other = (city) obj;
		if (cityId != other.cityId)
			return false;
		if (cityName == null) {
			if (other.cityName != null)
				return false;
		} else if (!cityName.equals(other.cityName))
			return false;
		if (cityPostCode == null) {
			if (other.cityPostCode != null)
				return false;
		} else if (!cityPostCode.equals(other.cityPostCode))
			return false;
		if (districtName == null) {
			if (other.districtName != null)
				return false;
		} else if (!districtName.equals(other.districtName))
			return false;
		if (registrationDate == null) {
			if (other.registrationDate != null)
				return false;
		} else if (!registrationDate.equals(other.registrationDate))
			return false;
		return true;
	}
}

package MODELclasses;

public class catWeapon 
{
	private int catWeaponId ;

	private String catWeaponName ;
	private String registrationDate ;

	/**
	 * <!-- begin-user-doc -->
	 * The constructor of the class 'catWeapon' initializes the weapon category with the name in parameter
	 * @param catWeaponName for example : sniper, machine gun, pistol...
	 * <!--  end-user-doc  -->
	 */
	public catWeapon(String catWeaponName)
	{
		this.catWeaponName = catWeaponName ;
	}

	// Getters
	public String getCatWeaponName() {
		return catWeaponName;
	}

	public int getCatWeaponId() {
		return catWeaponId;
	}
	public String getRegistrationDate() {
		return registrationDate;
	}
	//Setters
	public void setCatWeaponId(int catWeaponId) {
		this.catWeaponId = catWeaponId;
	}
	
	public void setRegistrationDate(String registrationDate) {
		this.registrationDate = registrationDate;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		catWeapon other = (catWeapon) obj;
		if (catWeaponId != other.catWeaponId)
			return false;
		if (catWeaponName == null) {
			if (other.catWeaponName != null)
				return false;
		} else if (!catWeaponName.equals(other.catWeaponName))
			return false;
		if (registrationDate == null) {
			if (other.registrationDate != null)
				return false;
		} else if (!registrationDate.equals(other.registrationDate))
			return false;
		return true;
	}
}

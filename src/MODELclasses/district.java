package MODELclasses;

public class district 
{
	private int districtId ;
	private String districtCode;
	private String districtName;
	private String countryName;
	private String registrationDate ;

	/**
	 * The constructor initializes the district's datas in the ones in parameter
	 * @param districtCode it can be the ISO code(france) or any other code of a country
	 * @param districtName
	 */
	public district(String districtCode, String districtName) 
	{
		this.districtCode = districtCode;
		this.districtName = districtName;
	}

	//Getters 
	public String getDistrictCode() {
		return districtCode;
	}
	public String getDistrictName() {
		return districtName;
	}
	public String getCountryName() {
		return countryName;
	}	
	
	public int getDistrictId() {
		return districtId;
	}
	
	public String getRegistrationDate() {
		return registrationDate;
	}

	//Setters
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	
	public void setDistrictId(int districtId) {
		this.districtId = districtId;
	}

	public void setRegistrationDate(String registrationDate) {
		this.registrationDate = registrationDate;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		district other = (district) obj;
		if (countryName == null) {
			if (other.countryName != null)
				return false;
		} else if (!countryName.equals(other.countryName))
			return false;
		if (districtCode == null) {
			if (other.districtCode != null)
				return false;
		} else if (!districtCode.equals(other.districtCode))
			return false;
		if (districtId != other.districtId)
			return false;
		if (districtName == null) {
			if (other.districtName != null)
				return false;
		} else if (!districtName.equals(other.districtName))
			return false;
		if (registrationDate == null) {
			if (other.registrationDate != null)
				return false;
		} else if (!registrationDate.equals(other.registrationDate))
			return false;
		return true;
	}

	

}

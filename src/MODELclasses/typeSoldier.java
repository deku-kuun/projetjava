package MODELclasses;

public class typeSoldier 
{
	private int typeSoldierId ;
	private String typeSoldierName;
	private String creationDate ;

	/**
	 * <!-- begin-user-doc -->
	 * The constructor of the class 'typeSoldier' initializes the type soldier's name with the one in parameter
	 * @param typeSoldierName
	 * <!--  end-user-doc  -->
	 */
	public typeSoldier(String typeSoldierName)
	{
		this.typeSoldierName = typeSoldierName ;
	}

	// Getter 
	public String getTypeSoldierName() {
		return typeSoldierName;
	}

	public int getTypeSoldierId() {
		return typeSoldierId;
	}

	public String getCreationDate() {
		return creationDate;
	}

	// Setters
	public void setTypeSoldierId(int typeSoldierId) {
		this.typeSoldierId = typeSoldierId;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		typeSoldier other = (typeSoldier) obj;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (typeSoldierId != other.typeSoldierId)
			return false;
		if (typeSoldierName == null) {
			if (other.typeSoldierName != null)
				return false;
		} else if (!typeSoldierName.equals(other.typeSoldierName))
			return false;
		return true;
	}	
}

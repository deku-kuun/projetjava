package MODELclasses;

public class typeVehicle 
{

	private int typeVehicleId ;
	
	private String typeVehicleName;

	private String typeVehicleDescription;

	private String typeVehicleMaxSpeed;

	private int typeVehiclePlaces;

	private String typeVehiclePic;
	
	private String registrationDate;

	private String catVehicleName;
	
	/**
	 * <!-- begin-user-doc -->
	 * The constructor of the class 'typeVehicle initializes the datas with the ones in parameter
	 * @param typeVehicleName
	 * @param typeVehicleDescription
	 * @param typeVehicleMaxSpeed
	 * @param typeVehicleMaxSpeed
	 * @param typeVehicleMaxSpeed
	 * <!--  end-user-doc  -->
	 */
	public typeVehicle(String typeVehicleName, String typeVehicleDescription, String typeVehicleMaxSpeed,
			int typeVehiclePlaces, String typeVehiclePic) 
	{
		this.typeVehicleName = typeVehicleName;
		this.typeVehicleDescription = typeVehicleDescription;
		this.typeVehicleMaxSpeed = typeVehicleMaxSpeed;
		this.typeVehiclePlaces = typeVehiclePlaces;
		this.typeVehiclePic = typeVehiclePic;
	}

	// Getters 
	public String getTypeVehicleName() {
		return typeVehicleName;
	}

	public String getTypeVehicleDescription() {
		return typeVehicleDescription;
	}

	public String getTypeVehicleMaxSpeed() {
		return typeVehicleMaxSpeed;
	}

	public int getTypeVehiclePlaces() {
		return typeVehiclePlaces;
	}

	public String getTypeVehiclePic() {
		return typeVehiclePic;
	}

	public String getCatVehicleName() {
		return catVehicleName;
	}	
	
	public int getTypeVehicleId() {
		return typeVehicleId;
	}
	
	public String getRegistrationDate() {
		return registrationDate;
	}

	//Setters
	public void setCatVehicleName(String catVehicleName) {
		this.catVehicleName = catVehicleName;
	}

	public void setTypeVehicleId(int typeVehicleId) {
		this.typeVehicleId = typeVehicleId;
	}

	public void setRegistrationDate(String registrationDate) {
		this.registrationDate = registrationDate;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		typeVehicle other = (typeVehicle) obj;
		if (catVehicleName == null) {
			if (other.catVehicleName != null)
				return false;
		} else if (!catVehicleName.equals(other.catVehicleName))
			return false;
		if (registrationDate == null) {
			if (other.registrationDate != null)
				return false;
		} else if (!registrationDate.equals(other.registrationDate))
			return false;
		if (typeVehicleDescription == null) {
			if (other.typeVehicleDescription != null)
				return false;
		} else if (!typeVehicleDescription.equals(other.typeVehicleDescription))
			return false;
		if (typeVehicleId != other.typeVehicleId)
			return false;
		if (typeVehicleMaxSpeed == null) {
			if (other.typeVehicleMaxSpeed != null)
				return false;
		} else if (!typeVehicleMaxSpeed.equals(other.typeVehicleMaxSpeed))
			return false;
		if (typeVehicleName == null) {
			if (other.typeVehicleName != null)
				return false;
		} else if (!typeVehicleName.equals(other.typeVehicleName))
			return false;
		if (typeVehiclePic == null) {
			if (other.typeVehiclePic != null)
				return false;
		} else if (!typeVehiclePic.equals(other.typeVehiclePic))
			return false;
		if (typeVehiclePlaces != other.typeVehiclePlaces)
			return false;
		return true;
	}
	
}

package MODELclasses;

public class member 
{
	private String militaryId ;

	private String password ;

	private String email ;

	private String firstname ;

	private String surname ;

	private String picName ;

	private String rankName ;

	private String registrationDate ;
	
	/**
	 * <!-- begin-user-doc -->
	 * The constructor of the class 'user' sets the user's datas with the ones in parameter 
	 * @param militaryId 
	 * @param password 
	 * @param email 
	 * @param firstname 
	 * @param surname 
	 * @param picName path to the member's picture
	 * @param rankName name of his/her rank
	 * <!--  end-user-doc  --> 
	 */
	public member(String militaryId, String password,String email,String firstname,String surname,String picName)
	{ 
		this.militaryId =  militaryId ; 
		this.password =  password ;
		this.email =  email ; 
		this.firstname =  firstname ; 
		this.surname =  surname ; 
		this.picName =  picName ; 
	}
	
	// Getters 
	public String getMilitaryId() {
		return militaryId;
	}

	public String getPassword() {
		return password;
	}

	public String getEmail() {
		return email;
	}

	public String getFirstname() {
		return firstname;
	}

	public String getSurname() {
		return surname;
	}

	public String getPicName() {
		return picName;
	}

	public String getRankName() {
		return rankName;
	}
	
	public String getRegistrationDate() {
		return registrationDate;
	}
	
	//Setters
	public void setRankName(String rankName) {
		this.rankName = rankName;
	}
	
	public void setRegistrationDate(String registrationDate) {
		this.registrationDate = registrationDate;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		member other = (member) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (firstname == null) {
			if (other.firstname != null)
				return false;
		} else if (!firstname.equals(other.firstname))
			return false;
		if (militaryId == null) {
			if (other.militaryId != null)
				return false;
		} else if (!militaryId.equals(other.militaryId))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (picName == null) {
			if (other.picName != null)
				return false;
		} else if (!picName.equals(other.picName))
			return false;
		if (rankName == null) {
			if (other.rankName != null)
				return false;
		} else if (!rankName.equals(other.rankName))
			return false;
		if (registrationDate == null) {
			if (other.registrationDate != null)
				return false;
		} else if (!registrationDate.equals(other.registrationDate))
			return false;
		if (surname == null) {
			if (other.surname != null)
				return false;
		} else if (!surname.equals(other.surname))
			return false;
		return true;
	}
	
}

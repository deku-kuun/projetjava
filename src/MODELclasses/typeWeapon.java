package MODELclasses;

public class typeWeapon 
{
	
	@Override
	public String toString() {
		return "typeWeapon [typeWeaponId=" + typeWeaponId + ", typeWeaponName=" + typeWeaponName
				+ ", typeWeaponDescription=" + typeWeaponDescription + ", typeWeaponRange=" + typeWeaponRange
				+ ", typeWeaponWeight=" + typeWeaponWeight + ", typeWeaponPic=" + typeWeaponPic + ", registrationDate="
				+ registrationDate + ", catWeaponName=" + catWeaponName + "]";
	}

	private int typeWeaponId ;
	
	private String typeWeaponName;

	private String typeWeaponDescription;

	private String typeWeaponRange;

	private String typeWeaponWeight;

	private String typeWeaponPic;
	
	private String registrationDate;
	
	private String catWeaponName ;

	/**
	 * This constuctor initializes the typeWeapon's datas with the ones in parameter
	 * @param typeWeaponName for example : PAMAS G1, FA-MAS F1 etc...
	 * @param typeWeaponDescription main objectives of the weapon, and the different situations to use it
	 * @param typeWeaponRange effective shooting distance 
	 * @param typeWeaponWeight
	 * @param typeWeaponPic path to locate the weapon picture
	 */
	public typeWeapon(String typeWeaponName, String typeWeaponDescription, String typeWeaponRange,
			String typeWeaponWeight, String typeWeaponPic) 
	{
		this.typeWeaponName = typeWeaponName;
		this.typeWeaponDescription = typeWeaponDescription;
		this.typeWeaponRange = typeWeaponRange;
		this.typeWeaponWeight = typeWeaponWeight;
		this.typeWeaponPic = typeWeaponPic;
	}

	//Getters
	public int getTypeWeaponId() {
		return typeWeaponId;
	}
	
	public String getTypeWeaponName() {
		return typeWeaponName;
	}

	public String getTypeWeaponDescription() {
		return typeWeaponDescription;
	}

	public String getTypeWeaponRange() {
		return typeWeaponRange;
	}

	public String getTypeWeaponWeight() {
		return typeWeaponWeight;
	}

	public String getTypeWeaponPic() {
		return typeWeaponPic;
	}
	
	public String getRegistrationDate() {
		return registrationDate;
	}

	public String getCatWeaponName() {
		return catWeaponName;
	}
	
	//Setter
	public void setCatWeaponName(String catWeaponName) {
		this.catWeaponName = catWeaponName;
	}
	
	public void setTypeWeaponId(int typeWeaponId) {
		this.typeWeaponId = typeWeaponId;
	}

	public void setRegistrationDate(String registrationDate) {
		this.registrationDate = registrationDate;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		typeWeapon other = (typeWeapon) obj;
		if (catWeaponName == null) {
			if (other.catWeaponName != null)
				return false;
		} else if (!catWeaponName.equals(other.catWeaponName))
			return false;
		if (registrationDate == null) {
			if (other.registrationDate != null)
				return false;
		} else if (!registrationDate.equals(other.registrationDate))
			return false;
		if (typeWeaponDescription == null) {
			if (other.typeWeaponDescription != null)
				return false;
		} else if (!typeWeaponDescription.equals(other.typeWeaponDescription))
			return false;
		if (typeWeaponId != other.typeWeaponId)
			return false;
		if (typeWeaponName == null) {
			if (other.typeWeaponName != null)
				return false;
		} else if (!typeWeaponName.equals(other.typeWeaponName))
			return false;
		if (typeWeaponPic == null) {
			if (other.typeWeaponPic != null)
				return false;
		} else if (!typeWeaponPic.equals(other.typeWeaponPic))
			return false;
		if (typeWeaponRange == null) {
			if (other.typeWeaponRange != null)
				return false;
		} else if (!typeWeaponRange.equals(other.typeWeaponRange))
			return false;
		if (typeWeaponWeight == null) {
			if (other.typeWeaponWeight != null)
				return false;
		} else if (!typeWeaponWeight.equals(other.typeWeaponWeight))
			return false;
		return true;
	}
}

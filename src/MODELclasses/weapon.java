package MODELclasses;

public class weapon 
{

	private String weaponSN;

	private String typeWeaponName;

	private String registrationDate ;

	/**
	 * <!-- begin-user-doc -->
	 * The constructor of the class 'weapon' initializes the data with the one in parameter
	 * @param weaponSN the serial number of the vehicle (ex : 'RS-879-05')
	 * <!--  end-user-doc  -->
	 */
	public weapon(String weaponSN) 
	{
		this.weaponSN = weaponSN;
	}

	// Getters 
	public String getWeaponSN() {
		return weaponSN;
	}

	public String getTypeWeaponName() {
		return typeWeaponName;
	}
	
	public String getRegistrationDate() {
		return registrationDate;
	}
	
	//Setters
	public void setTypeWeaponName(String typeWeaponName) {
		this.typeWeaponName = typeWeaponName;
	}

	public void setRegistrationDate(String registrationDate) {
		this.registrationDate = registrationDate;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		weapon other = (weapon) obj;
		if (registrationDate == null) {
			if (other.registrationDate != null)
				return false;
		} else if (!registrationDate.equals(other.registrationDate))
			return false;
		if (typeWeaponName == null) {
			if (other.typeWeaponName != null)
				return false;
		} else if (!typeWeaponName.equals(other.typeWeaponName))
			return false;
		if (weaponSN == null) {
			if (other.weaponSN != null)
				return false;
		} else if (!weaponSN.equals(other.weaponSN))
			return false;
		return true;
	}

}

package MODELclasses;

public class soldier extends member {

	private String typeSoldierName ;

	private String squadName ;
	
	/**
	 * <!-- begin-user-doc -->
	 * The constructor of the class 'soldier' initialize the soldier's datas with the ones in parameter
	 * @param militaryId 
	 * @param password
	 * @param email
	 * @param firstname
	 * @param surname
	 * @param picName path to the location of the member's picture
	 * @param rankName 
	 * @param typeSoldierName 
	 * @param squadName name of her/his squad
	 * <!--  end-user-doc  -->
	 */
	public soldier(String militaryId, String password, String email, String firstname, 
			String surname, String picName) 
	{
		super(militaryId, password, email, firstname, surname, picName);
		super.setRankName("Soldier");
	}
	
	// Getters 
	public String getTypeSoldierName() {
		return typeSoldierName;
	}
	public String getSquadName() {
		return squadName;
	}
	
	// Setters
	public void setTypeSoldierName(String typeSoldierName) {
		this.typeSoldierName = typeSoldierName;
	}
	
	public void setSquadName(String squadName) {
		this.squadName = squadName;
	}
}

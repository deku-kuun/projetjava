package MODELclasses;

import java.util.ArrayList;

public class mission 
{
	private int missionId ;
	
	private String missionName;
	
	private String missionDescription;
	
	private String missionStatus;
	
	private String missionPlaces ;
	
	private String creationDate ;

	private String missionBeginning ;
	
	private String missionEnding ;

	private String missionCityLocation;

	private String creatorMilitaryId ;

	private ArrayList<String> membersRequired_MilitaryIds ;

	private ArrayList<String> weaponsRequired_SNs ;

	private ArrayList<String> vehiclesRequired_LPs ;

	/**
	 * <!-- begin-user-doc -->
	 * The constructor of the class 'mission' initializes the mission's datas with the ones in parameter
	 * @param missionName
	 * @param missionDescription
	 * @param creatorMilitaryId the militaryId of the mission creator
	 * <!--  end-user-doc  -->
	 */
	public mission(String missionName,String missionDescription, String creatorMilitaryId) 
	{
		this.missionName = missionName;
		this.missionDescription = missionDescription;;
		this.creatorMilitaryId = creatorMilitaryId;
		this.missionStatus = "Mission not begin yet" ;
		this.membersRequired_MilitaryIds = new ArrayList<String>() ;
		this.weaponsRequired_SNs =  new ArrayList<String>();
		this.vehiclesRequired_LPs = new ArrayList<String>();
	}

	/**
	 * <!-- begin-user-doc -->
	 * This method add a member to a mission if not already in
	 * <!--  end-user-doc  -->
	 */
	public boolean addMember(String militaryId)
	{
		boolean addM = false ;
		if(!this.membersRequired_MilitaryIds.contains(militaryId))
			addM = this.membersRequired_MilitaryIds.add(militaryId) ;
		return addM ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * This method add a weapon to a mission if not already in
	 * <!--  end-user-doc  -->
	 */
	public boolean addWeapon(String weaponSN)
	{
		boolean addW = false ;
		if(!this.weaponsRequired_SNs.contains(weaponSN))
			addW = this.weaponsRequired_SNs.add(weaponSN) ;
		return addW ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * This method add a vehicle to a mission if not already in
	 * <!--  end-user-doc  -->
	 */
	public boolean addVehicle(String vehicleLP)
	{
		boolean addV = false ;
		if(!this.vehiclesRequired_LPs.contains(vehicleLP))
			addV = this.vehiclesRequired_LPs.add(vehicleLP) ;
		return addV ;
	}

	/**
	 * This method sets the status of the mission to 'mission started' if a rankedMember starts it
	 */
	public boolean beginMission()
	{
		this.setMissionStatus("Mission started") ;	
		return true ;
	}
	/**
	 * This method sets the status of the mission to 'mission ended' if a rankedMember ends it
	 */
	public boolean endMission()
	{
		this.setMissionStatus("Mission ended") ;	
		return true ;
	}
	
	//Getters

	public int getMissionId() {
		return missionId;
	}

	public String getMissionName() {
		return missionName;
	}

	public String getMissionDescription() {
		return missionDescription;
	}

	public String getMissionStatus() {
		return missionStatus;
	}
	
	public String getMissionPlaces() {
		return missionPlaces ;
	}
	
	public String getCreationDate() {
		return creationDate;
	}

	public String getMissionBeginning() {
		return missionBeginning;
	}

	public String getMissionEnding() {
		return missionEnding;
	}

	public String getMissionCityLocation() {
		return missionCityLocation;
	}

	public String getCreatorMilitaryId() {
		return creatorMilitaryId;
	}

	public ArrayList<String> getMembersRequired_MilitaryIds() {
		return membersRequired_MilitaryIds;
	}

	public ArrayList<String> getWeaponsRequired_SNs() {
		return weaponsRequired_SNs;
	}

	public ArrayList<String> getVehiclesRequired_LPs() {
		return vehiclesRequired_LPs;
	}
	
	//Setters

	public void setMissionId(int missionId) {
		this.missionId = missionId;
	}

	public void setMissionName(String missionName) {
		this.missionName = missionName;
	}

	public void setMissionDescription(String missionDescription) {
		this.missionDescription = missionDescription;
	}

	public void setMissionStatus(String missionStatus) {
		this.missionStatus = missionStatus;
	}
	
	public void setMissionPlaces(String missionPlaces) {
		this.missionPlaces = missionPlaces;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public void setMissionBeginning(String missionBeginning) {
		this.missionBeginning = missionBeginning;
	}

	public void setMissionEnding(String missionEnding) {
		this.missionEnding = missionEnding;
	}

	public void setMissionCityLocation(String missionCityLocation) {
		this.missionCityLocation = missionCityLocation;
	}


	public void setMembersRequired_MilitaryIds(ArrayList<String> membersRequired_MilitaryIds) {
		this.membersRequired_MilitaryIds = membersRequired_MilitaryIds;
	}

	public void setWeaponsRequired_SNs(ArrayList<String> weaponsRequired_SNs) {
		this.weaponsRequired_SNs = weaponsRequired_SNs;
	}

	public void setVehiclesRequired_LPs(ArrayList<String> vehiclesRequired_LPs) {
		this.vehiclesRequired_LPs = vehiclesRequired_LPs;
	}
	
}

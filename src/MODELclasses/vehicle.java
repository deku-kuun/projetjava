package MODELclasses;

public class vehicle 
{
	
	private String vehicleLP;

	private String typeVehicleName;
	
	private String registrationDate ;

	/**
	 * <!-- begin-user-doc --> 
	 * The constructor of the class 'vehicle' initializes the data with the one in parameter
	 * @param vehicleLP contains the licence plate of the vehicle (ex : 'RS-879-05')
	 * <!--  end-user-doc  -->
	 */
	public vehicle(String vehicleLP) 
	{
		this.vehicleLP = vehicleLP;
	}


	// Getters 
	public String getVehicleLP() {
		return vehicleLP;
	}

	public String getTypeVehicleName() {
		return typeVehicleName;
	}
	
	public String getRegistrationDate() {
		return registrationDate;
	}

	// Setters
	public void setTypeVehicleName(String typeVehicleName) {
		this.typeVehicleName = typeVehicleName;
	}

	public void setRegistrationDate(String registrationDate) {
		this.registrationDate = registrationDate;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		vehicle other = (vehicle) obj;
		if (registrationDate == null) {
			if (other.registrationDate != null)
				return false;
		} else if (!registrationDate.equals(other.registrationDate))
			return false;
		if (typeVehicleName == null) {
			if (other.typeVehicleName != null)
				return false;
		} else if (!typeVehicleName.equals(other.typeVehicleName))
			return false;
		if (vehicleLP == null) {
			if (other.vehicleLP != null)
				return false;
		} else if (!vehicleLP.equals(other.vehicleLP))
			return false;
		return true;
	}

}

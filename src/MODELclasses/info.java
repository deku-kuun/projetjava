package MODELclasses;

public class info 
{
	private int missionId ;
	private String firstName, surName, cityName, missionName, missionDescription, missionPlaces, missionStatus, creationDate ;

	public info(String firstName, String surName, String cityName, 
			int missionId, String missionName, String missionDescription, 
			String missionPlaces, String missionStatus, String creationDate) 
	{
		this.missionId = missionId;
		this.firstName = firstName;
		this.surName = surName;
		this.cityName = cityName;
		this.missionName = missionName;
		this.missionDescription = missionDescription;
		this.missionStatus = missionStatus;
		this.missionPlaces = missionPlaces;
		this.creationDate = creationDate;
	}

	//Getters
	public int getMissionId() {
		return missionId;
	}
	public String getFirstName() {
		return firstName;
	}
	public String getSurName() {
		return surName;
	}
	public String getCityName() {
		return cityName;
	}
	public String getMissionName() {
		return missionName;
	}
	public String getMissionDescription() {
		return missionDescription;
	}
	public String getMissionPlaces() {
		return missionPlaces;
	}
	public String getMissionStatus() {
		return missionStatus;
	}
	public String getCreationDate() {
		return creationDate;
	}

	//Setters
	public void setMissionId(int missionId) {
		this.missionId = missionId;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public void setSurName(String surName) {
		this.surName = surName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public void setMissionName(String missionName) {
		this.missionName = missionName;
	}
	public void setMissionDescription(String missionDescription) {
		this.missionDescription = missionDescription;
	}
	public void setMissionPlaces(String missionPlaces) {
		this.missionPlaces = missionPlaces;
	}
	public void setMissionStatus(String missionStatus) {
		this.missionStatus = missionStatus;
	}
	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}
}
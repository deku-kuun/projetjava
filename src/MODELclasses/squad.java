package MODELclasses;

public class squad 
{
	private int squadId ;
	private String squadName ;
	private String creationDate ;

	/**
	 * The constructor initializes the squad with the name in parameter
	 * @param squadName
	 */
	public squad(String squadName)
	{
		this.squadName = squadName ;
	}

	//Getters
	public String getSquadName() {
		return squadName;
	}

	public int getSquadId() {
		return squadId;
	}

	public String getCreationDate() {
		return creationDate;
	}

	//Setters
	public void setSquadId(int squadId) {
		this.squadId = squadId;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		squad other = (squad) obj;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (squadId != other.squadId)
			return false;
		if (squadName == null) {
			if (other.squadName != null)
				return false;
		} else if (!squadName.equals(other.squadName))
			return false;
		return true;
	}

}

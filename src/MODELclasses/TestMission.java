package MODELclasses;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

public class TestMission {

	
	@Test
	public void testMissionStatus() {
		mission mission = new mission("Mission_1", "Mission d'une dur�e de 3 mois ", "12224324");
		assertEquals(mission.getMissionStatus(), "Mission not begin yet");
	}
	
	@Test
	public void testMissionMember() {
		mission mission = new mission("Mission_1", "Mission d'une dur�e de 3 mois ", "12224324");
		mission.addMember("222");
		assertNotNull(mission.getMembersRequired_MilitaryIds());
		assertEquals(mission.getMembersRequired_MilitaryIds().size(),1);
		assertEquals(mission.getMembersRequired_MilitaryIds().get(0),"222");
	}
	
	@Test
	public void testMissionVehicule() {
		mission mission = new mission("Mission_1", "Mission d'une dur�e de 3 mois ", "12224324");
		mission.addVehicle("565656");
		assertNotNull(mission.getVehiclesRequired_LPs());
		assertEquals(mission.getVehiclesRequired_LPs().size(),1);
		assertEquals(mission.getVehiclesRequired_LPs().get(0),"565656");
	}
	
	@Test
	public void testMissionWeapon() {
		mission mission = new mission("Mission_1", "Mission d'une dur?e de 3mois ", "12224324");
		mission.addWeapon("FK324");
		assertNotNull(mission.getWeaponsRequired_SNs());
		assertEquals(mission.getWeaponsRequired_SNs().size(),1);
		assertEquals(mission.getWeaponsRequired_SNs().get(0),"FK324");
	}
	
	@Test
	public void testBeginMission() {
		mission mission = new mission("Mission_1", "Mission d'une dur�e de 3 mois ", "12224324");
		assertEquals(mission.getMissionStatus(), "Mission not begin yet");
		mission.beginMission();
		assertEquals(mission.getMissionStatus(), "Mission started");
	}
	
	@Test
	public void testEndMission() {
		mission mission = new mission("Mission_1", "Mission d'une dur�e de 3 mois ", "12224324");
		assertEquals(mission.getMissionStatus(), "Mission not begin yet");
		mission.beginMission();
		assertEquals(mission.getMissionStatus(), "Mission started");
		mission.endMission();
		assertEquals(mission.getMissionStatus(), "Mission ended");
	}
}

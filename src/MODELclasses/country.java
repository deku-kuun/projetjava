package MODELclasses;

public class country 
{
	private int countryId ;
	private String countryCode ;
	private String countryName ;
	private String registrationDate ;

	/**
	 * The constructor initializes the country's data with the ones in parameter
	 * @param countryCode international code for countries, for example : DE->Germany <br> GB->United Kingdom <br> US->United States of America 
	 * @param countryName
	 */
	public country(String countryCode, String countryName) 
	{
		this.countryCode = countryCode;
		this.countryName = countryName;
	}

	//Getters
	public String getCountryCode() {
		return countryCode;
	}

	public String getCountryName() {
		return countryName;
	}

	public int getCountryId() {
		return countryId;
	}

	public String getRegistrationDate() {
		return registrationDate;
	}

	//Setters
	public void setRegistrationDate(String registrationDate) {
		this.registrationDate = registrationDate;
	}

	public void setCountryId(int countryId) {
		this.countryId = countryId;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		country other = (country) obj;
		if (countryCode == null) {
			if (other.countryCode != null)
				return false;
		} else if (!countryCode.equals(other.countryCode))
			return false;
		if (countryId != other.countryId)
			return false;
		if (countryName == null) {
			if (other.countryName != null)
				return false;
		} else if (!countryName.equals(other.countryName))
			return false;
		if (registrationDate == null) {
			if (other.registrationDate != null)
				return false;
		} else if (!registrationDate.equals(other.registrationDate))
			return false;
		return true;
	}	
}
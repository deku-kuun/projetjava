package MODELclasses;

public class catVehicle 
{

	private int catVehicleId ;
	private String catVehicleName ;
	private String registrationDate ;

	/**
	 * <!-- begin-user-doc -->
	 * The constructor of the class 'catVehicle' initializes the vehicle category with the name in parameter
	 * @param catVehicleName the name of the vehicle's type, for example : armoured, utility, special, amphibious etc..
	 * <!--  end-user-doc  -->
	 */
	public catVehicle(String catVehicleName)
	{
		this.catVehicleName = catVehicleName ;
	}

	// Getters
	public String getCatVehicleName() {
		return catVehicleName;
	}

	public String getRegistrationDate() {
		return registrationDate;
	}

	public int getCatVehicleId() {
		return catVehicleId;
	}

	//Setters
	public void setCatVehicleId(int catVehicleId) {
		this.catVehicleId = catVehicleId;
	}

	public void setRegistrationDate(String registrationDate) {
		this.registrationDate = registrationDate;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		catVehicle other = (catVehicle) obj;
		if (catVehicleId != other.catVehicleId)
			return false;
		if (catVehicleName == null) {
			if (other.catVehicleName != null)
				return false;
		} else if (!catVehicleName.equals(other.catVehicleName))
			return false;
		if (registrationDate == null) {
			if (other.registrationDate != null)
				return false;
		} else if (!registrationDate.equals(other.registrationDate))
			return false;
		return true;
	}
}

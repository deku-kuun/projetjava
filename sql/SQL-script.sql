-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mar. 03 déc. 2019 à 20:54
-- Version du serveur :  5.7.24
-- Version de PHP :  7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `h-mog_army`
--

-- --------------------------------------------------------

--
-- Structure de la table `catvehicle`
--

CREATE TABLE `catvehicle` (
  `catVehicleId` int(11) NOT NULL,
  `catVehicleName` varchar(255) NOT NULL,
  `registrationDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `catweapon`
--

CREATE TABLE `catweapon` (
  `catWeaponId` int(11) NOT NULL,
  `catWeaponName` varchar(255) NOT NULL,
  `registrationDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `city`
--

CREATE TABLE `city` (
  `cityId` int(11) NOT NULL,
  `postCodeCity` varchar(50) NOT NULL,
  `cityName` varchar(255) NOT NULL,
  `registrationDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `districtCode` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `country`
--

CREATE TABLE `country` (
  `countryId` int(11) NOT NULL,
  `countryCode` varchar(3) NOT NULL,
  `countryName` varchar(255) NOT NULL,
  `registrationDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `district`
--

CREATE TABLE `district` (
  `districtId` int(11) NOT NULL,
  `districtCode` varchar(50) NOT NULL,
  `districtName` varchar(255) NOT NULL,
  `registrationDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `countryCode` varchar(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `member`
--

CREATE TABLE `member` (
  `memberId` int(11) NOT NULL,
  `militaryId` varchar(255) NOT NULL,
  `password` text NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `surName` varchar(100) DEFAULT NULL,
  `firstName` varchar(100) DEFAULT NULL,
  `picName` varchar(255) DEFAULT 'default.png',
  `registrationDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `rankCode` varchar(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `member_has_typesoldier`
--

CREATE TABLE `member_has_typesoldier` (
  `militaryId` varchar(255) NOT NULL,
  `typeSoldierId` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `mission`
--

CREATE TABLE `mission` (
  `missionId` int(11) NOT NULL,
  `missionName` varchar(255) NOT NULL,
  `missionDescription` text NOT NULL,
  `missionPlaces` int(11) NOT NULL,
  `missionStatus` varchar(30) NOT NULL,
  `creationDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `missionBeginning` datetime DEFAULT NULL,
  `missionEnding` datetime DEFAULT NULL,
  `cityId` int(11) NOT NULL,
  `militaryId` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `mission_has_member`
--

CREATE TABLE `mission_has_member` (
  `missionId` int(11) NOT NULL,
  `militaryId` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `mission_needs_vehicle`
--

CREATE TABLE `mission_needs_vehicle` (
  `missionId` varchar(255) NOT NULL,
  `vehicleLP` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `mission_needs_weapon`
--

CREATE TABLE `mission_needs_weapon` (
  `missionId` int(11) NOT NULL,
  `weaponSN` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `rank`
--

CREATE TABLE `rank` (
  `rankId` int(11) NOT NULL,
  `rankCode` varchar(3) NOT NULL,
  `rankName` varchar(255) NOT NULL,
  `creationDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `squad`
--

CREATE TABLE `squad` (
  `squadId` int(11) NOT NULL,
  `squadName` varchar(255) NOT NULL,
  `creationDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `squad_has_member`
--

CREATE TABLE `squad_has_member` (
  `squadId` int(11) NOT NULL,
  `militaryId` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `typesoldier`
--

CREATE TABLE `typesoldier` (
  `typeSoldierId` int(11) NOT NULL,
  `typeSoldierName` varchar(255) NOT NULL,
  `creationDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `typevehicle`
--

CREATE TABLE `typevehicle` (
  `typeVehicleId` int(11) NOT NULL,
  `typeVehicleName` varchar(255) NOT NULL,
  `typeVehicleDescription` text NOT NULL,
  `typeVehicleMaxSpeed` varchar(100) NOT NULL,
  `typeVehiclePlaces` smallint(6) NOT NULL,
  `typeVehiclePic` varchar(255) NOT NULL,
  `registrationDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `catVehicleId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `typeweapon`
--

CREATE TABLE `typeweapon` (
  `typeWeaponId` int(11) NOT NULL,
  `typeWeaponName` varchar(255) NOT NULL,
  `typeWeaponDescription` text NOT NULL,
  `typeWeaponRange` varchar(50) NOT NULL,
  `typeWeaponWeight` varchar(255) NOT NULL,
  `typeWeaponPic` varchar(255) NOT NULL,
  `registrationDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `catWeaponId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `vehicle`
--

CREATE TABLE `vehicle` (
  `vehicleId` int(11) NOT NULL,
  `vehicleLP` varchar(255) NOT NULL,
  `registrationDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `typeVehicleId` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `weapon`
--

CREATE TABLE `weapon` (
  `weaponId` int(11) NOT NULL,
  `weaponSN` varchar(255) NOT NULL,
  `registrationDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `typeWeaponId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `catvehicle`
--
ALTER TABLE `catvehicle`
  ADD PRIMARY KEY (`catVehicleId`);

--
-- Index pour la table `catweapon`
--
ALTER TABLE `catweapon`
  ADD PRIMARY KEY (`catWeaponId`);

--
-- Index pour la table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`cityId`),
  ADD KEY `city_district_FK` (`districtCode`);

--
-- Index pour la table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`countryId`,`countryCode`);

--
-- Index pour la table `district`
--
ALTER TABLE `district`
  ADD PRIMARY KEY (`districtId`,`districtCode`) USING BTREE,
  ADD KEY `district_country_FK` (`countryCode`);

--
-- Index pour la table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`memberId`,`militaryId`),
  ADD KEY `member_rank_FK` (`rankCode`);

--
-- Index pour la table `member_has_typesoldier`
--
ALTER TABLE `member_has_typesoldier`
  ADD KEY `member_has_typeSoldier_member_FK` (`militaryId`) USING BTREE,
  ADD KEY `member_has_typeSoldier_typeSoldier_FK` (`typeSoldierId`) USING BTREE;

--
-- Index pour la table `mission`
--
ALTER TABLE `mission`
  ADD PRIMARY KEY (`missionId`),
  ADD KEY `mission_city_FK` (`cityId`),
  ADD KEY `member_militaryId_FK` (`militaryId`) USING BTREE;

--
-- Index pour la table `mission_has_member`
--
ALTER TABLE `mission_has_member`
  ADD KEY `mission_has_member_mission_FK` (`missionId`),
  ADD KEY `mission_has_member_member0_FK` (`militaryId`);

--
-- Index pour la table `mission_needs_vehicle`
--
ALTER TABLE `mission_needs_vehicle`
  ADD KEY `mission_needs_vehicle_mission_FK` (`missionId`);

--
-- Index pour la table `mission_needs_weapon`
--
ALTER TABLE `mission_needs_weapon`
  ADD KEY `FK_missionId` (`missionId`),
  ADD KEY `FK_weaponSN` (`weaponSN`);

--
-- Index pour la table `rank`
--
ALTER TABLE `rank`
  ADD PRIMARY KEY (`rankId`,`rankCode`);

--
-- Index pour la table `squad`
--
ALTER TABLE `squad`
  ADD PRIMARY KEY (`squadId`);

--
-- Index pour la table `squad_has_member`
--
ALTER TABLE `squad_has_member`
  ADD KEY `FK_squadId` (`squadId`),
  ADD KEY `militaryId` (`militaryId`);

--
-- Index pour la table `typesoldier`
--
ALTER TABLE `typesoldier`
  ADD PRIMARY KEY (`typeSoldierId`);

--
-- Index pour la table `typevehicle`
--
ALTER TABLE `typevehicle`
  ADD PRIMARY KEY (`typeVehicleId`),
  ADD KEY `vehicle_catVehicle_FK` (`catVehicleId`) USING BTREE;

--
-- Index pour la table `typeweapon`
--
ALTER TABLE `typeweapon`
  ADD PRIMARY KEY (`typeWeaponId`),
  ADD UNIQUE KEY `FK_catWeaponId` (`catWeaponId`) USING BTREE;

--
-- Index pour la table `vehicle`
--
ALTER TABLE `vehicle`
  ADD PRIMARY KEY (`vehicleId`,`vehicleLP`) USING BTREE,
  ADD KEY `FK_typeVehicleId` (`typeVehicleId`);

--
-- Index pour la table `weapon`
--
ALTER TABLE `weapon`
  ADD PRIMARY KEY (`weaponId`,`weaponSN`),
  ADD KEY `Weapon_typeWeapon_FK` (`typeWeaponId`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `catvehicle`
--
ALTER TABLE `catvehicle`
  MODIFY `catVehicleId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `catweapon`
--
ALTER TABLE `catweapon`
  MODIFY `catWeaponId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `city`
--
ALTER TABLE `city`
  MODIFY `cityId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `country`
--
ALTER TABLE `country`
  MODIFY `countryId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `district`
--
ALTER TABLE `district`
  MODIFY `districtId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `member`
--
ALTER TABLE `member`
  MODIFY `memberId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `mission`
--
ALTER TABLE `mission`
  MODIFY `missionId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `rank`
--
ALTER TABLE `rank`
  MODIFY `rankId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `squad`
--
ALTER TABLE `squad`
  MODIFY `squadId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `typesoldier`
--
ALTER TABLE `typesoldier`
  MODIFY `typeSoldierId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `typevehicle`
--
ALTER TABLE `typevehicle`
  MODIFY `typeVehicleId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `typeweapon`
--
ALTER TABLE `typeweapon`
  MODIFY `typeWeaponId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `vehicle`
--
ALTER TABLE `vehicle`
  MODIFY `vehicleId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `weapon`
--
ALTER TABLE `weapon`
  MODIFY `weaponId` int(11) NOT NULL AUTO_INCREMENT;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `squad_has_member`
--
ALTER TABLE `squad_has_member`
  ADD CONSTRAINT `FK_squadId` FOREIGN KEY (`squadId`) REFERENCES `squad` (`squadId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `weapon`
--
ALTER TABLE `weapon`
  ADD CONSTRAINT `weapon_ibfk_1` FOREIGN KEY (`typeWeaponId`) REFERENCES `typeweapon` (`typeWeaponId`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
